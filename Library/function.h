/*
These codes have been developed by J. Olmedo, V. Sreenath and I. Agullo. You are 
free to use them for any scientific or pedagogical purpose. However, the authors 
are not responsible of any mistake or typo in these codes. Nevertheless, we will 
acknowledge any feedback at this respect. Besides, if you plan to use these codes 
(or a modified version based on them) for scientific purposes (i.e. not pedagogical 
purposes), please, we will very much appreciate if you cite any of the following 
references: 
arXiv:1702.06036; arXiv:1601.01716.
Date: 05/11/19.
Place: Louisiana State University
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_math.h>

/**
 *  Some global parameters.
 */
struct param_const;

/****************  Declaration of functions in FRW in GR  *************************/

double Vphi2 (const double y[], void *params);

double dVphi2 (const double y[], void *params);

double ddVphi2 (const double y[], void *params);

double Kphi (const double y[], void *params);

double rho (const double y[], void *params);

double rho_KV (double K, double V);

double P_KV (double K, double V);

double hubble (const double y[], void *params);

double dhubble (const double y[], void *params);

double c_gr (const double y[], void *params);

double constraint (const double y[], void *params);

double rel_c (double C_gr, double Rho);

double dphi_sol (double C_gr, double V, double Dphi);

double ricci (const double y[], void *params);

double ricci_H (double H, double dH);

double slPS (const double y[], void *params); 

double slns (const double y[], void *params);

double sleps (const double y[], void *params);

double slepsV (const double y[], void *params);

double sletaV (const double y[], void *params);



/****************  Declaration of functions in FRW in LQC  *************************/

double Vpf_lqc (const double y[], void *params);

double dVpf_lqc (const double y[], void *params);

double ddVpf_lqc (const double y[], void *params);

double rhoQ_H (double H, double Rho, void *params);

double PQ_H (double H, double dH, double Rho, double P, void *params);

double hubble_lqc (const double y[], void *params);

double dhubble_lqc (const double y[], void *params);

double c_gr_lqc (const double y[], void *params);

double constraint_lqc (const double y[], void *params);

double slPS_lqc (const double y[], void *params); 

double sleps_lqc (const double y[], void *params);

void ss_hyb (const double y[], const double Vfs[], double ss[], void *params);

void ss_drsd (const double y[], const double Vfs[], double ss[], void *params);

void ss_ppu (const double y[], const double KVfs[], double ss[], void *params);



/****************  Declaration of functions in FRW in RLQG  *************************/

struct param_int;

double intnd0 (double x, void * params);

double intnd1 (double x, void * params);

double intnd2 (double x, void * params);

double intnd3 (double x, void * params);

double intnd4 (double x, void * params);

double intgr (const gsl_function * f);

double hubble_rlqg (const double y[], void *params);

double dhubble_rlqg (const double y[], void *params);

double c_gr_rlqg (const double y[], void *params);

double constraint_rlqg (const double y[], void *params);

double ricci_rlqg (const double y[], void *params);

double slPS_rlqg (const double y[], void *params);

void ss_rlqg_hyb (const double y[], const double KVfs[], double ss[], void *params);

void ss_rlqg_hyb_mink (const double y[], const double KVfs[], double ss[], void *params);

void ss_rlqg_drsd (const double y[], const double KVfs[], double ss[], void *params);

void ss_rlqg_drsd_mink (const double y[], const double KVfs[], double ss[], void *params);

void ss_rlqg_mink (const double y[], const double KVfs[], double ss[], void *params);

void ss_rlqg (const double y[], const double KVfs[], double ss[], void *params);

/****************  Declaration of functions in FRW in RLQG with suddle point approx *************************/

double hubble_rlqg_sp (const double y[], void *params);

double dhubble_rlqg_sp (const double y[], void *params);

double c_gr_rlqg_sp (const double y[], void *params);

double constraint_rlqg_sp (const double y[], void *params);

/****************  Declaration of functions in Bianchi I in GR  *************************/

struct param_const_b1;

double Vphi2_b1 (const double y[], void *params);

double dVphi2_b1 (const double y[], void *params);

double ddVphi2_b1 (const double y[], void *params);

double Kphi_b1 (const double y[], void *params);

double rho_b1 (const double y[], void *params);

double hubble_b1 (const double y[], void *params);

double dhubble_b1 (const double y[], void *params);

double c_gr_b1 (const double y[], void *params);

double constraint_b1 (const double y[], void *params);

double ricci_b1 (const double y[], void *params);

double ddaoa_b1 (const double y[], void *params);

double h1 (const double y[], void *params);

double h2 (const double y[], void *params);

double h3 (const double y[], void *params);

double sig2 (const double y[], void *params);

double slPS_b1 (const double y[], void *params);

double slns_b1 (const double y[], void *params);

void dy_b1_gr (const double y[], void *params, const double KVfs[], const double gii[], const double hi[], double f[]);

void ddy_b1_gr (const double y[], void *params, const double f[], double df[]);

void shear (const double y[], const double f[], const double df[], double sij[], double dsij[]);

void hatkt (const double y[], void *params, const double KVfs[], const double gii[], const double hi[], double Kt[]);

void fshear (const double y[], void *params, const double KVfs[], const double gii[], const double hi[], const double Kt[], const double f[], const double sij[], const double dsij[], double fsij[], double fdsij[]);

void couplns (const double y[], void *params, double const KVfs[], double const f[], double const df[], double const fsij[], double const fdsij[], double couplns[]);

double bet_t (const double y[], void *params);

double gamm_t (const double y[], void *params);


/****************  Declaration of functions in Bianchi I in LQC in AW  *************************/

double Vpf_b1_lqc (const double y[], void *params);

double dVpf_b1_lqc (const double y[], void *params);

double ddVpf_b1_lqc (const double y[], void *params);

double hubble_b1_lqc (const double y[], void *params);

double dhubble_b1_lqc (const double y[], void *params);

double c_gr_b1_lqc (const double y[], void *params);

double constraint_b1_lqc (const double y[], void *params);

double ddaoa_b1_lqc (const double y[], void *params);

double h1_lqc (const double y[], void *params);

double h2_lqc (const double y[], void *params);

double h3_lqc (const double y[], void *params);

double sig2_lqc (const double y[], void *params);

double slPS_b1_lqc (const double y[], void *params);

void dy_b1_lqc (const double y[], void *params, const double KVfs[], const double gii[], const double hi[], double f[]);

void ddy_b1_lqc (const double y[], void *params, const double f[], double df[]);

//void shear_lqc (const double y[], void *params, double sij[], double dsij[]);

//void fshear_lqc (const double y[], void *params, const double sij[], const double dsij[], double fsij[], double fdsij[]);

void couplns_lqc (const double y[], void *params, double const KVfs[], double const f[], double const df[], double const fsij[], double const fdsij[], double couplns[]);


/****************  Declaration of functions in Bianchi I in LQC in AW  *************************/

double hubble_b1_lqc_vac (const double y[], void *params);

double constraint_b1_lqc_vac (const double y[], void *params);

double ddaoa_b1_lqc_vac (const double y[], void *params);

double h1_lqc_vac (const double y[], void *params);

double h2_lqc_vac (const double y[], void *params);

double h3_lqc_vac (const double y[], void *params);

double sig2_lqc_vac (const double y[], void *params);

void shear_lqc_vac (const double y[], void *params, double sij[], double dsij[]);

void fshear_lqc_vac (const double y[], void *params, const double sij[], const double dsij[], double fsij[], double fdsij[]);

void couplns_lqc_vac (const double y[], void *params, double const fsij[], double const fdsij[], double couplns[]); 

