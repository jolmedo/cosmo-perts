/*
These codes have been developed by J. Olmedo, V. Sreenath and I. Agullo. You are 
free to use them for any scientific or pedagogical purpose. However, the authors 
are not responsible of any mistake or typo in these codes. Nevertheless, we will 
acknowledge any feedback at this respect. Besides, if you plan to use these codes 
(or a modified version based on them) for scientific purposes (i.e. not pedagogical 
purposes), please, we will very much appreciate if you cite any of the following 
references: 
arXiv:1702.06036; arXiv:1601.01716.
Date: 05/11/19.
Place: Louisiana State University
*/


#include "functions.h"

/****************  Declaration of initial conditions for perturbations in FRW in GR  *************************/

void id_Mink (double y[], double z[], void *params);

void id_BD (double y[], double z[], void *params);


/****************  Declaration of initial conditions for perturbations in FRW in GR  *************************/

void id_Mink_lqc (double y[], double z[], void *params);

void id_BD_lqc (double y[], double z[], void *params);


/****************  Declaration of initial conditions for perturbations in FRW in GR  *************************/

void id_Mink_rlqg (double y[], double z[], void *params);


/****************  Declaration of initial conditions for perturbations in Bianchi I in GR  *************************/

void id_b1_Mink (double y[], double z[], void *params);

void id_b1_Mink2 (double y[], double z[], void *params);

/****************  Declaration of initial conditions for perturbations in Bianchi I in AW LQC  *************************/

void id_b1_Mink_lqc (double y[], double z[], void *params);

void id_b1_Mink2_lqc (double y[], double z[], void *params);

void id_b1_BD_lqc (double y[], double z[], void *params);


/****************  Declaration of some observables for perturbations in Bianchi I  *************************/

void PS_b1 (const double y[], double ps[], int n, void *params);

void norms_b1 (const double y[], double nsol[], int l); 

void alpha_b1 (const double y0[], const double y1[], double alpha[][3][3], int l);

void beta_b1 (const double y0[], const double y1[], double beta[][3][3], int l);

/****************  Declaration of initial conditions for perturbations in Bianchi I in AW LQC in vacuum *************************/

void id_b1_Mink_lqc_vac (double y[], double z[], void *params);

void norms_b1_vac (const double y[], double nsol[], int l);