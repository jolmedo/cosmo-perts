/*
These codes have been developed by J. Olmedo, V. Sreenath and I. Agullo. You are 
free to use them for any scientific or pedagogical purpose. However, the authors 
are not responsible of any mistake or typo in these codes. Nevertheless, we will 
acknowledge any feedback at this respect. Besides, if you plan to use these codes 
(or a modified version based on them) for scientific purposes (i.e. not pedagogical 
purposes), please, we will very much appreciate if you cite any of the following 
references: 
arXiv:1702.06036; arXiv:1601.01716.
Date: 05/11/19.
Place: Louisiana State University
*/


#include "functions.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

struct param_const;

/****************  Declaration of odes in FRW in GR  *************************/

int func (double t, const double y[], double f[],
      void *params);

int jac (double t, const double y[], double *dfdy, 
     double dfdt[], void *params);


/****************  Declaration of odes in FRW in LQC  *************************/

int func_lqc (double t, const double y[], double f[],
      void *params);

int jac_lqc (double t, const double y[], double *dfdy, 
     double dfdt[], void *params);

/****************  Declaration of odes in FRW in RLQG  *************************/

struct param_int;

int func_rlqg (double t, const double y[], double f[], void *params);

int func_rlqg_sp (double t, const double y[], double f[], void *params);

int jac_rlqg (double t, const double y[], double *dfdy, double dfdt[], void *params);


/****************  Declaration of odes in Bianchi I in GR  *************************/

int func_b1 (double t, const double y[], double f[], void *params);

int jac_b1 (double t, const double y[], double *dfdy, double dfdt[], void *params);

//int func_FRW_B1 (double t, const double y[], double f[], void *params);

int func_b1_alpha (double t, const double y[], double f[], void *params);

int jac_b1_alpha (double t, const double y[], double *dfdy, double dfdt[], void *params);


/****************  Declaration of odes in Bianchi I in AW LQC  *************************/

int func_b1_lqc (double t, const double y[], double f[], void *params);

int jac_b1_lqc (double t, const double y[], double *dfdy, double dfdt[], void *params);

int func_b1_lqc_alpha (double t, const double y[], double f[], void *params);

int jac_b1_lqc_alpha (double t, const double y[], double *dfdy, double dfdt[], void *params);

int func_b1_lqc_alp_vac (double t, const double y[], double f[], void *params);

int jac_b1_lqc_alp_vac (double t, const double y[], double *dfdy, double dfdt[], void *params);


/****************  Declaration of odes for perturbations in FRW in GR  *************************/

int func_perts (double t, const double y[], double f[], void *params);

int jac_perts (double t, const double y[], double *dfdy, double dfdt[], void *params);


/****************  Declaration of odes for perturbations in FRW in LQC  *************************/

int func_perts_lqc (double t, const double y[], double f[], void *params);

int jac_perts_lqc (double t, const double y[], double *dfdy, double dfdt[], void *params);


/****************  Declaration of odes for perturbations in FRW in RLQG a la hybrid  *************************/

int func_perts_rlqg (double t, const double y[], double f[], void *params);

int func_perts_rlqg_sp (double t, const double y[], double f[], void *params);

int jac_perts_rlqg (double t, const double y[], double *dfdy, double dfdt[], void *params);


/****************  Declaration of odes for perturbations in Bianchi I in GR  *************************/

int func_b1_perts (double t, const double y[], double f[], void *params);

int jac_b1_perts (double t, const double y[], double *dfdy, double dfdt[], void *params);


/****************  Declaration of odes for perturbations in Bianchi I in LQC  *************************/

int func_b1_perts_lqc (double t, const double y[], double f[], void *params);

int jac_b1_perts_lqc (double t, const double y[], double *dfdy, double dfdt[], void *params);

/****************  Declaration of odes for perturbations in Bianchi I in LQC in vacuum  *************************/

int func_b1_perts_lqc_vac (double t, const double y[], double f[], void *params);

int jac_b1_perts_lqc_vac (double t, const double y[], double *dfdy, double dfdt[], void *params);