/*
These codes have been developed by J. Olmedo, V. Sreenath and I. Agullo. You are 
free to use them for any scientific or pedagogical purpose. However, the authors 
are not responsible of any mistake or typo in these codes. Nevertheless, we will 
acknowledge any feedback at this respect. Besides, if you plan to use these codes 
(or a modified version based on them) for scientific purposes (i.e. not pedagogical 
purposes), please, we will very much appreciate if you cite any of the following 
references: 
arXiv:1702.06036; arXiv:1601.01716.
Date: 05/11/19.
Place: Louisiana State University
*/


#include "vacua.h"


struct param_const {
/**
 *  Some global parameters.
 */
    double mass;
    double gamma;
    double G;
    double lambda;
    double lp;
    double kmin;
    double kmax;
    int N;
};


/****************  Initial conditions for perturbations in FRW in GR  *************************/

void
id_Mink (double y[], double z[], void *params)
{
/**
 *  0th order adiabatic (or massless Minkowski) vacuum state for scalar and tensor perturbations in FRW spacetimes in GR.
 */
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  int n = my_params_pointer->N;

  //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> Pphi.  y[5] = re u; y[6] -> re u'; y[7] -> im u;  y[8] -> im u'.
  // Initial data for the background
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];

  double H; //Hubble param
  H = hubble(y, &*my_params_pointer);

  //initial data for the perturbations
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  double Dk, Ck;
  int i; 
  for(i = 0; i < n; i++){
    Dk = k;
    Ck = 0.0;
    y[5+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[5+8*i+1] = sqrt(Dk / 2.0) * Ck / (exp(y[1] * 2.0/3.0)) - H/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[5+8*i+2] = 0.0;
    y[5+8*i+3] = -sqrt(Dk / 2.0) / (exp(y[1] * 2.0/3.0));
    y[9+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[9+8*i+1] = sqrt(Dk / 2.0) * Ck / (exp(y[1] * 2.0/3.0)) - H/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[9+8*i+2] = 0.0;
    y[9+8*i+3] = -sqrt(Dk / 2.0) / (exp(y[1] * 2.0/3.0));
    k = k * dk;
  }

}


void
id_BD (double y[], double z[], void *params)
{
/**
 *  Bunch-Davies vacuum state for scalar and tensor perturbations in FRW spacetimes in GR.
 */
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  int n = my_params_pointer->N;

  //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> Pphi.  y[5] = re u; y[6] -> re u'; y[7] -> im u;  y[8] -> im u'.
  // Initial data for the background
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];

  double H; //Hubble param
  H = hubble(y, &*my_params_pointer);

  //initial data for the perturbations
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  double Dk, Ck, eta0;
  int i;
  eta0 = 1.0e0;
  for(i = 0; i < n; i++){
    Dk = k / (1.0 + 1.0 / pow(k * eta0 , 2.0));
    Ck = -1.0 / pow(k * eta0 , 3.0);
    y[5+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[5+8*i+1] = sqrt(Dk / 2.0) * Ck / (exp(y[1] * 2.0/3.0)) - H/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[5+8*i+2] = 0.0;
    y[5+8*i+3] = -sqrt(Dk / 2.0) / (exp(y[1] * 2.0/3.0));
    y[9+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[9+8*i+1] = sqrt(Dk / 2.0) * Ck / (exp(y[1] * 2.0/3.0)) - H/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[9+8*i+2] = 0.0;
    y[9+8*i+3] = -sqrt(Dk / 2.0) / (exp(y[1] * 2.0/3.0));
    k = k * dk;
  }

}

/****************  Initial conditions for perturbations in FRW in LQC  *************************/

void
id_Mink_lqc (double y[], double z[], void *params)
{
/**
 *  0th order adiabatic (or massless Minkowski) vacuum state for scalar and tensor perturbations in FRW spacetimes in LQC.
 */
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  int n = my_params_pointer->N;

  //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> Pphi.  y[5] = re u; y[6] -> re u'; y[7] -> im u;  y[8] -> im u'.
  // Initial data for the background
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];

  double H; //Hubble param
  H = hubble_lqc(y, &*my_params_pointer);

  //initial data for the perturbations
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  int i; 
  for(i = 0; i < n; i++){
    y[5+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * k));
    y[5+8*i+1] = -H/(exp(y[1]/3.0) * sqrt(2.0 * k));
    y[5+8*i+2] = 0.0;
    y[5+8*i+3] = -k/(exp(y[1] * 2.0/3.0) * sqrt(2.0 * k));
    y[9+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * k));
    y[9+8*i+1] = -H/(exp(y[1]/3.0) * sqrt(2.0 * k));
    y[9+8*i+2] = 0.0;
    y[9+8*i+3] = -k/(exp(y[1] * 2.0/3.0) * sqrt(2.0 * k));
    k = k * dk;
  }

}


void
id_BD_lqc (double y[], double z[], void *params)
{
/**
 *  Bunch-Davies vacuum state for scalar and tensor perturbations in FRW spacetimes in LQC.
 */
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  int n = my_params_pointer->N;

  //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> Pphi.  y[5] = re u; y[6] -> re u'; y[7] -> im u;  y[8] -> im u'.
  // Initial data for the background
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];

  double H; //Hubble param
  H = hubble_lqc(y, &*my_params_pointer);

  //initial data for the perturbations
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  double Dk, Ck, eta0;
  int i;
  eta0 = 1.0e0;
  for(i = 0; i < n; i++){
    Dk = k / (1.0 + 1.0 / pow(k * eta0 , 2.0));
    Ck = -1.0 / pow(k * eta0 , 3.0);
    y[5+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[5+8*i+1] = sqrt(Dk / 2.0) * Ck / (exp(y[1] * 2.0/3.0)) - H/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[5+8*i+2] = 0.0;
    y[5+8*i+3] = -sqrt(Dk / 2.0) / (exp(y[1] * 2.0/3.0));
    y[9+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[9+8*i+1] = sqrt(Dk / 2.0) * Ck / (exp(y[1] * 2.0/3.0)) - H/(exp(y[1]/3.0) * sqrt(2.0 * Dk));
    y[9+8*i+2] = 0.0;
    y[9+8*i+3] = -sqrt(Dk / 2.0) / (exp(y[1] * 2.0/3.0));
    k = k * dk;
  }

}

/****************  Initial conditions for perturbations in FRW in RLQG a la hybrid  *************************/


void
id_Mink_rlqg (double y[], double z[], void *params)
{
/**
 *  0th order adiabatic (or massless Minkowski) vacuum state for scalar and tensor perturbations in FRW spacetimes in QRLG.
 */
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  int n = my_params_pointer->N;

  //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> Pphi.  y[5] = re u; y[6] -> re u'; y[7] -> im u;  y[8] -> im u'.
  // Initial data for the background
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];

  double H = hubble_rlqg(y, &*my_params_pointer); //Hubble param

  //initial data for the perturbations
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  int i; 
  for(i = 0; i < n; i++){
    y[5+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * k));
    y[5+8*i+1] = -H/(exp(y[1]/3.0) * sqrt(2.0 * k));
    y[5+8*i+2] = 0.0;
    y[5+8*i+3] = -k/(exp(y[1] * 2.0/3.0) * sqrt(2.0 * k));
    y[9+8*i] = 1.0/(exp(y[1]/3.0) * sqrt(2.0 * k));
    y[9+8*i+1] = -H/(exp(y[1]/3.0) * sqrt(2.0 * k));
    y[9+8*i+2] = 0.0;
    y[9+8*i+3] = -k/(exp(y[1] * 2.0/3.0) * sqrt(2.0 * k));
    k = k * dk;
  }

}


/****************  Initial conditions for perturbations in Bianchi I in GR  *************************/

struct param_const_b1 {
/**
 *  Some global parameters for perturbations in Bianchi I spacetimes.
 */
  double mass;
  double gamma;
  double G;
  double lambda;
  double lp;    
  double alp;
  double bet;
  double gam; 
  double kmin;
  double kmax; 
  int N; 
};


void
id_b1_Mink (double y[], double z[], void *params)
{
/**
 *  0th order adiabatic (or massless Minkowski) vacuum state for scalar and tensor perturbations in Bianchi I spacetimes in GR for Pereira et. al. gauge invariant perturbations.
 */
  //This initial data has been provided externally.
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;

  //ID for the homogeneous dof
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];
  y[5] = z[5];
  y[6] = z[6];
  y[7] = z[7];
  y[8] = z[8];
  y[9] = z[9];

  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  

  double n = my_params_pointer->N;
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles  
  double k1 = cosl(gam) * sinl(bet);
  double k2 = sinl(bet) * sinl(gam);
  double k3 = cosl(bet);

  double f1 = 1.0 / (g * l) * (y[7] + y[6]);
  double f2 = 1.0 / (g * l) * (y[5] + y[7]);
  double f3 = 1.0 / (g * l) * (y[6] + y[5]);
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H = 1.0 /3.0 * (h1 + h2 + h3);

  //ID for the inhomogeneities dof
  //Here y[9+4*i] = reu; y[9+4*i+1] -> reu'; y[9+4*i+2] -> imu; y[9+4*i+3] -> imu'; 
  int i; 
  for(i = 0; i < n; i++){
    y[10+36*i] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+1] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[10+36*i+2] = 0.0;
    y[10+36*i+3] = (- sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    y[10+36*i+4] = 0.0;
    y[10+36*i+5] = 0.0; 
    y[10+36*i+6] = 0.0;
    y[10+36*i+7] = 0.0;
    y[10+36*i+8] = 0.0;
    y[10+36*i+9] = 0.0; 
    y[10+36*i+10] = 0.0;
    y[10+36*i+11] = 0.0;

    y[10+36*i+12] = 0.0;
    y[10+36*i+13] = 0.0; 
    y[10+36*i+14] = 0.0;
    y[10+36*i+15] = 0.0;
    y[10+36*i+16] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+17] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[10+36*i+18] = 0.0;
    y[10+36*i+19] = ( - sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    y[10+36*i+20] = 0.0;
    y[10+36*i+21] = 0.0; 
    y[10+36*i+22] = 0.0;
    y[10+36*i+23] = 0.0;

    y[10+36*i+24] = 0.0;
    y[10+36*i+25] = 0.0; 
    y[10+36*i+26] = 0.0;
    y[10+36*i+27] = 0.0;
    y[10+36*i+28] = 0.0;
    y[10+36*i+29] = 0.0; 
    y[10+36*i+30] = 0.0;
    y[10+36*i+31] = 0.0;
    y[10+36*i+32] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+33] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[10+36*i+34] = 0.0;
    y[10+36*i+35] = ( - sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    k = k * dk;
    
  }
  
}


void
id_b1_Mink2 (double y[], double z[], void *params)
{
/**
 *  0th order adiabatic (or massless Minkowski) vacuum state for scalar and tensor perturbations in Bianchi I spacetimes in GR for AOV gauge invariant perturbations.
 */
  //This initial data has been provided externally.
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;

  //ID for the homogeneous dof
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];
  y[5] = z[5];
  y[6] = z[6];
  y[7] = z[7];
  y[8] = z[8];
  y[9] = z[9];

  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  

  double n = my_params_pointer->N;
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles  
  double k1 = cosl(gam) * sinl(bet);
  double k2 = sinl(bet) * sinl(gam);
  double k3 = cosl(bet);

  double f1 = 1.0 / (g * l) * (y[7] + y[6]);
  double f2 = 1.0 / (g * l) * (y[5] + y[7]);
  double f3 = 1.0 / (g * l) * (y[6] + y[5]);
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H = 1.0 /3.0 * (h1 + h2 + h3);

  //ID for the inhomogeneities dof
  //Here y[9+4*i] = reu; y[9+4*i+1] -> reu'; y[9+4*i+2] -> imu; y[9+4*i+3] -> imu'; 
  int i; 
  for(i = 0; i < n; i++){
    y[10+36*i] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+1] = 0.0; 
    y[10+36*i+2] = 0.0;
    y[10+36*i+3] = (- sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    y[10+36*i+4] = 0.0;
    y[10+36*i+5] = 0.0; 
    y[10+36*i+6] = 0.0;
    y[10+36*i+7] = 0.0;
    y[10+36*i+8] = 0.0;
    y[10+36*i+9] = 0.0; 
    y[10+36*i+10] = 0.0;
    y[10+36*i+11] = 0.0;

    y[10+36*i+12] = 0.0;
    y[10+36*i+13] = 0.0; 
    y[10+36*i+14] = 0.0;
    y[10+36*i+15] = 0.0;
    y[10+36*i+16] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+17] = 0.0; 
    y[10+36*i+18] = 0.0;
    y[10+36*i+19] = ( - sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    y[10+36*i+20] = 0.0;
    y[10+36*i+21] = 0.0; 
    y[10+36*i+22] = 0.0;
    y[10+36*i+23] = 0.0;

    y[10+36*i+24] = 0.0;
    y[10+36*i+25] = 0.0; 
    y[10+36*i+26] = 0.0;
    y[10+36*i+27] = 0.0;
    y[10+36*i+28] = 0.0;
    y[10+36*i+29] = 0.0; 
    y[10+36*i+30] = 0.0;
    y[10+36*i+31] = 0.0;
    y[10+36*i+32] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+33] = 0.0; 
    y[10+36*i+34] = 0.0;
    y[10+36*i+35] = ( - sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    k = k * dk;
    
  }
  
}

/****************  Initial conditions for perturbations in Bianchi I in AW LQC  *************************/


void
id_b1_Mink_lqc (double y[], double z[], void *params)
{
/**
 *  0th order adiabatic (or massless Minkowski) vacuum state for scalar and tensor perturbations in Bianchi I spacetimes in LQC for Pereira et. al. gauge invariant perturbations.
 */
  //This initial data has been provided externally.
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;

  //ID for the homogeneous dof
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];
  y[5] = z[5];
  y[6] = z[6];
  y[7] = z[7];
  y[8] = z[8];
  y[9] = z[9];

  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  

  double n = my_params_pointer->N;
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles  
  double k1 = cosl(gam) * sinl(bet);
  double k2 = sinl(bet) * sinl(gam);
  double k3 = cosl(bet);

  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H = 1.0 /3.0 * (h1 + h2 + h3);

  //ID for the inhomogeneities dof
  //Here y[9+4*i] = reu; y[9+4*i+1] -> reu'; y[9+4*i+2] -> imu; y[9+4*i+3] -> imu'; 
  int i; 
  for(i = 0; i < n; i++){
    y[10+36*i] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+1] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[10+36*i+2] = 0.0;
    y[10+36*i+3] = (- sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    y[10+36*i+4] = 0.0;
    y[10+36*i+5] = 0.0; 
    y[10+36*i+6] = 0.0;
    y[10+36*i+7] = 0.0;
    y[10+36*i+8] = 0.0;
    y[10+36*i+9] = 0.0; 
    y[10+36*i+10] = 0.0;
    y[10+36*i+11] = 0.0;

    y[10+36*i+12] = 0.0;
    y[10+36*i+13] = 0.0; 
    y[10+36*i+14] = 0.0;
    y[10+36*i+15] = 0.0;
    y[10+36*i+16] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+17] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[10+36*i+18] = 0.0;
    y[10+36*i+19] = ( - sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    y[10+36*i+20] = 0.0;
    y[10+36*i+21] = 0.0; 
    y[10+36*i+22] = 0.0;
    y[10+36*i+23] = 0.0;

    y[10+36*i+24] = 0.0;
    y[10+36*i+25] = 0.0; 
    y[10+36*i+26] = 0.0;
    y[10+36*i+27] = 0.0;
    y[10+36*i+28] = 0.0;
    y[10+36*i+29] = 0.0; 
    y[10+36*i+30] = 0.0;
    y[10+36*i+31] = 0.0;
    y[10+36*i+32] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+33] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[10+36*i+34] = 0.0;
    y[10+36*i+35] = ( - sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    k = k * dk;
    
  }
  
}


void
id_b1_Mink2_lqc (double y[], double z[], void *params)
{
/**
 *  0th order adiabatic (or massless Minkowski) vacuum state for scalar and tensor perturbations in Bianchi I spacetimes in LQC for AOV gauge invariant perturbations.
 */
  //This initial data has been provided externally.
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;

  //ID for the homogeneous dof
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];
  y[5] = z[5];
  y[6] = z[6];
  y[7] = z[7];
  y[8] = z[8];
  y[9] = z[9];

  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  

  double n = my_params_pointer->N;
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles  
  double k1 = cosl(gam) * sinl(bet);
  double k2 = sinl(bet) * sinl(gam);
  double k3 = cosl(bet);

  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H = 1.0 /3.0 * (h1 + h2 + h3);

  //ID for the inhomogeneities dof
  //Here y[9+4*i] = reu; y[9+4*i+1] -> reu'; y[9+4*i+2] -> imu; y[9+4*i+3] -> imu'; 
  int i; 
  for(i = 0; i < n; i++){
    y[10+36*i] = 1.0/sqrtl(2.0 * powl(a,3) * k * sqrtl(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+1] = 0.0; 
    y[10+36*i+2] = 0.0;
    y[10+36*i+3] = (- sqrtl(k * sqrtl(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrtl(2.0 * powl(a,3)));
    y[10+36*i+4] = 0.0;
    y[10+36*i+5] = 0.0; 
    y[10+36*i+6] = 0.0;
    y[10+36*i+7] = 0.0;
    y[10+36*i+8] = 0.0;
    y[10+36*i+9] = 0.0; 
    y[10+36*i+10] = 0.0;
    y[10+36*i+11] = 0.0;

    y[10+36*i+12] = 0.0;
    y[10+36*i+13] = 0.0; 
    y[10+36*i+14] = 0.0;
    y[10+36*i+15] = 0.0;
    y[10+36*i+16] = 1.0/sqrtl(2.0 * powl(a,3) * k * sqrtl(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+17] = 0.0; 
    y[10+36*i+18] = 0.0;
    y[10+36*i+19] = ( - sqrtl(k * sqrtl(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrtl(2.0 * powl(a,3)));
    y[10+36*i+20] = 0.0;
    y[10+36*i+21] = 0.0; 
    y[10+36*i+22] = 0.0;
    y[10+36*i+23] = 0.0;

    y[10+36*i+24] = 0.0;
    y[10+36*i+25] = 0.0; 
    y[10+36*i+26] = 0.0;
    y[10+36*i+27] = 0.0;
    y[10+36*i+28] = 0.0;
    y[10+36*i+29] = 0.0; 
    y[10+36*i+30] = 0.0;
    y[10+36*i+31] = 0.0;
    y[10+36*i+32] = 1.0/sqrtl(2.0 * powl(a,3) * k * sqrtl(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[10+36*i+33] = 0.0; 
    y[10+36*i+34] = 0.0;
    y[10+36*i+35] = ( - sqrtl(k * sqrtl(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrtl(2.0 * powl(a,3)));
    k = k * dk;
    
  }
  
}

void
id_b1_BD_lqc (double y[], double z[], void *params)
{
//   (void)(t);
  //This initial data has been provided externally.
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;

  //ID for the homogeneous dof
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];
  y[5] = z[5];
  y[6] = z[6];
  y[7] = z[7];
  y[8] = z[8];
  y[9] = z[9];

  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  

  double n = my_params_pointer->N;
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles  
  double k1 = cosl(gam) * sinl(bet);
  double k2 = sinl(bet) * sinl(gam);
  double k3 = cosl(bet);

  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H = 1.0 /3.0 * (h1 + h2 + h3);

  //ID for the inhomogeneities dof
  //Here y[9+4*i] = reu; y[9+4*i+1] -> reu'; y[9+4*i+2] -> imu; y[9+4*i+3] -> imu'; 
  int i;
  double kt, Dk, Ck, dDk;
  double eta = 1.0e0;
  for(i = 0; i < n; i++){
    kt = k * a * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3));
    Dk = kt / (1.0 + 1.0 / pow(kt * eta , 2.0));
    dDk = a * H * kt * pow(1.0 + 1.0 * pow(a,-2.0) * pow(kt * eta,-2.0),-1.0)
      - a*kt*(-2*pow(a,-2)*pow(eta,-3)*pow(kt,-2) - 2*H*pow(a,-2)*pow(eta,-2)*pow(kt,-2) - pow(a,-2)*pow(eta,-2)*(-2*h1*pow(a1,-2)*pow(k1,2) - 2*h2*pow(a2,-2)*pow(k2,2) - 2*h3*pow(a3,-2)*pow(k3,2))*pow(kt,-2)*pow(pow(a1,-2)*pow(k1,2) + pow(a2,-2)*pow(k2,2) + pow(a3,-2)*pow(k3,2),-1))*pow(1 + pow(a,-2)*pow(eta,-2)*pow(kt,-2),-2) 
      + (a*kt*(-2*h1*pow(a1,-2)*pow(k1,2) - 2*h2*pow(a2,-2)*pow(k2,2) -2*h3*pow(a3,-2)*pow(k3,2))*pow(pow(a1,-2)*pow(k1,2) + pow(a2,-2)*pow(k2,2) + pow(a3,-2)*pow(k3,2),-1)*pow(1 + pow(a,-2)*pow(eta,-2)*pow(kt,-2),-1))/2.;
    Ck = -1.0 / (pow(kt * eta , 3.0)) - 0.0 * 1.0 / 2.0 * dDk / (Dk * Dk);

    y[10+36*i] = 1.0 / sqrt(2.0 * Dk) / a;
    y[10+36*i+1] = sqrt(Dk / 2.0) * Ck / pow(a,2.0) - H / a / sqrt(2.0 * Dk); 
    y[10+36*i+2] = 0.0;
    y[10+36*i+3] = - sqrt(Dk / 2.0) / pow(a,2.0);
    y[10+36*i+4] = 0.0;
    y[10+36*i+5] = 0.0; 
    y[10+36*i+6] = 0.0;
    y[10+36*i+7] = 0.0;
    y[10+36*i+8] = 0.0;
    y[10+36*i+9] = 0.0; 
    y[10+36*i+10] = 0.0;
    y[10+36*i+11] = 0.0;

    y[10+36*i+12] = 0.0;
    y[10+36*i+13] = 0.0; 
    y[10+36*i+14] = 0.0;
    y[10+36*i+15] = 0.0;
    y[10+36*i+16] = 1.0 / sqrt(2.0 * Dk) / a;
    y[10+36*i+17] = sqrt(Dk / 2.0) * Ck / pow(a,2.0) - H / a / sqrt(2.0 * Dk); 
    y[10+36*i+18] = 0.0;
    y[10+36*i+19] = - sqrt(Dk / 2.0) / pow(a,2.0);
    y[10+36*i+20] = 0.0;
    y[10+36*i+21] = 0.0; 
    y[10+36*i+22] = 0.0;
    y[10+36*i+23] = 0.0;

    y[10+36*i+24] = 0.0;
    y[10+36*i+25] = 0.0; 
    y[10+36*i+26] = 0.0;
    y[10+36*i+27] = 0.0;
    y[10+36*i+28] = 0.0;
    y[10+36*i+29] = 0.0; 
    y[10+36*i+30] = 0.0;
    y[10+36*i+31] = 0.0;
    y[10+36*i+32] = 1.0 / sqrt(2.0 * Dk) / a;
    y[10+36*i+33] = sqrt(Dk / 2.0) * Ck / pow(a,2.0) - H / a / sqrt(2.0 * Dk); 
    y[10+36*i+34] = 0.0;
    y[10+36*i+35] = - sqrt(Dk / 2.0) / pow(a,2.0);
    k = k * dk;
    
  }
  
}


//Power spectra 

void
PS_b1 (const double y[], double ps[], int n, void *params)
{
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double k1 = cos(my_params_pointer->gam) * sin(my_params_pointer->bet);
  double k2 = sin(my_params_pointer->bet) * sin(my_params_pointer->gam);
  double k3 = cos(my_params_pointer->bet);
  double H = hubble_b1(y, &*my_params_pointer);
  double hatk = a * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3));
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/((double)n-1.0));
  int i;
  for(i = 0; i < n; i++){
    //Here we print teh PS
    ps[0 + 7 * i] = k * hatk;
    ps[1 + 7 * i] = pow(k * hatk,3) * (H * H / (y[8] * y[8])) * (y[10+36*i] * y[10+36*i] + y[10+36*i+2] * y[10+36*i+2] + y[10+36*i+12] * y[10+36*i+12] + y[10+36*i+14] * y[10+36*i+14] + y[10+36*i+24] * y[10+36*i+24] + y[10+36*i+26] * y[10+36*i+26])/(2.0*M_PI*M_PI);
    ps[2 + 7 * i] = (32.0 * M_PI * my_params_pointer->G) * pow(k * hatk,3) * (y[10+36*i+4] * y[10+36*i+4] + y[10+36*i+6] * y[10+36*i+6] + y[10+36*i+16] * y[10+36*i+16] + y[10+36*i+18] * y[10+36*i+18] + y[10+36*i+28] * y[10+36*i+28] + y[10+36*i+30] * y[10+36*i+30])/(2.0*M_PI*M_PI);
    ps[3 + 7 * i] = (32.0 * M_PI * my_params_pointer->G) * pow(k * hatk,3) * (y[10+36*i+8] * y[10+36*i+8] + y[10+36*i+10] * y[10+36*i+10] + y[10+36*i+20] * y[10+36*i+20] + y[10+36*i+22] * y[10+36*i+22] + y[10+36*i+32] * y[10+36*i+32] + y[10+36*i+34] * y[10+36*i+34])/(2.0*M_PI*M_PI);
    ps[4 + 7 * i] = sqrt(32.0 * M_PI * my_params_pointer->G) * (H / (y[8])) * pow(k * hatk,3) * (y[10+36*i] * y[10+36*i+6] + y[10+36*i+12] * y[10+36*i+18] + y[10+36*i+24] * y[10+36*i+30])/(2.0*M_PI*M_PI);
    ps[5 + 7 * i] = sqrt(32.0 * M_PI * my_params_pointer->G) * (H / (y[8])) * pow(k * hatk,3) * (y[10+36*i] * y[10+36*i+10] + y[10+36*i+12] * y[10+36*i+22] + y[10+36*i+24] * y[10+36*i+34])/(2.0*M_PI*M_PI);
    ps[6 + 7 * i] = (32.0 * M_PI * my_params_pointer->G) * pow(k * hatk,3) * (y[10+36*i+4] * y[10+36*i+10] + y[10+36*i+16] * y[10+36*i+22] + y[10+36*i+28] * y[10+36*i+34])/(2.0*M_PI*M_PI);
   }
}



//Norms of the (l) mode function y[]

void
norms_b1 (const double y[], double nsol[], int l)
{
//   (void)(t);
  double v = exp((y[1] + y[2] + y[3]) / 2.0);
  int j;
  //These norms come from [v,pv]=i, [mup,pmup]=i, [muc,pmuc]=i
  for(j = 0; j < 3; j++){
    nsol[j] = 2.0 * v * (y[10+12*j+36*l+2] * y[10+12*j+36*l+1] - y[10+12*j+36*l] * y[10+12*j+36*l+3] + y[14+12*j+36*l+2] * y[14+12*j+36*l+1] - y[14+12*j+36*l] * y[14+12*j+36*l+3] + y[18+12*j+36*l+2] * y[18+12*j+36*l+1] - y[18+12*j+36*l] * y[18+12*j+36*l+3]);
  }
  //These norms come from [v,mup]=0, [v,muc]=0, [mup,muc]=0
  nsol[3] = y[10+36*l+2] * y[14+36*l] - y[10+36*l] * y[14+36*l+2] + y[10+12+36*l+2] * y[14+12+36*l] - y[10+12+36*l] * y[14+12+36*l+2] + y[10+24+36*l+2] * y[14+24+36*l] - y[10+24+36*l] * y[14+24+36*l+2];
  nsol[4] = y[10+36*l+2] * y[18+36*l] - y[10+36*l] * y[18+36*l+2] + y[10+12+36*l+2] * y[18+12+36*l] - y[10+12+36*l] * y[18+12+36*l+2] + y[10+24+36*l+2] * y[18+24+36*l] - y[10+24+36*l] * y[18+24+36*l+2];
  nsol[5] = y[14+36*l+2] * y[18+36*l] - y[14+36*l] * y[18+36*l+2] + y[14+12+36*l+2] * y[18+12+36*l] - y[14+12+36*l] * y[18+12+36*l+2] + y[14+24+36*l+2] * y[18+24+36*l] - y[14+24+36*l] * y[18+24+36*l+2];
}



//alpha coefficients of the (l) mode for solutions y0[] and y1[]. alpha[0][i][j] and alpha[1][i][j] are the real and imaginary parts of alpha_{ij}

void
alpha_b1 (const double y0[], const double y1[], double alpha[][3][3], int l)
{
  double v = exp((y0[1] + y0[2] + y0[3]) / 2.0);
  int i,j,ki,kj;
    for(i = 0; i < 3; i++){
        for(j = 0; j < 3; j++){
	  ki = 10+36*l+12*i;
	  kj = 10+36*l+12*j;
	  alpha[0][i][j] = v * (
	    -(y0[ki+3] * y1[kj] - y0[ki+1] * y1[kj+2]) - (y0[ki] * y1[kj+3] - y0[ki+2] * y1[kj+1])
	    - (y0[ki+7] * y1[kj+4] - y0[ki+5] * y1[kj+6]) - (y0[ki+4] * y1[kj+7] - y0[ki+6] * y1[kj+5])
	    - (y0[ki+11] * y1[kj+8] - y0[ki+9] * y1[kj+10]) - (y0[ki+8] * y1[kj+11] - y0[ki+10] * y1[kj+9]));
	  
	  alpha[1][i][j] = v * (
	    + (y0[ki+1] * y1[kj] + y0[ki+3] * y1[kj+2]) - (y0[ki] * y1[kj+1] + y0[ki+2] * y1[kj+3])
	    + (y0[ki+5] * y1[kj+4] + y0[ki+7] * y1[kj+6]) - (y0[ki+4] * y1[kj+5] + y0[ki+6] * y1[kj+7])
	    + (y0[ki+9] * y1[kj+8] + y0[ki+11] * y1[kj+10]) - (y0[ki+8] * y1[kj+9] + y0[ki+10] * y1[kj+11]));
	}
    }
}

//beta coefficients of the (l) mode for solutions y0[] and y1[]. beta[0][i][j] and beta[1][i][j] are the real and imaginary parts of beta{ij}

void
beta_b1 (const double y0[], const double y1[], double beta[][3][3], int l)
{
  double v = exp((y0[1] + y0[2] + y0[3]) / 2.0);
  int i,j,ki,kj;
    for(i = 0; i < 3; i++){
        for(j = 0; j < 3; j++){
	  ki = 10+36*l+12*i;
	  kj = 10+36*l+12*j;
	  beta[0][i][j] = v * (
	    + (y0[ki+3] * y1[kj] + y0[ki+1] * y1[kj+2]) - (y0[ki] * y1[kj+3] + y0[ki+2] * y1[kj+1])
	    + (y0[ki+7] * y1[kj+4] + y0[ki+5] * y1[kj+6]) - (y0[ki+4] * y1[kj+7] + y0[ki+6] * y1[kj+5])
	    + (y0[ki+11] * y1[kj+8] + y0[ki+9] * y1[kj+10]) - (y0[ki+8] * y1[kj+11] + y0[ki+10] * y1[kj+9]));
	  
	  beta[1][i][j] = v * (
	    - (y0[ki+1] * y1[kj] - y0[ki+3] * y1[kj+2]) + (y0[ki] * y1[kj+1] - y0[ki+2] * y1[kj+3])
	    - (y0[ki+5] * y1[kj+4] - y0[ki+7] * y1[kj+6]) + (y0[ki+4] * y1[kj+5] - y0[ki+6] * y1[kj+7])
	    - (y0[ki+9] * y1[kj+8] - y0[ki+11] * y1[kj+10]) + (y0[ki+8] * y1[kj+9] - y0[ki+10] * y1[kj+11]));
	}
    }
}


/****************  Initial conditions for perturbations in Bianchi I in AW LQC in vacuum *************************/


void
id_b1_Mink_lqc_vac (double y[], double z[], void *params)
{
//   (void)(t);
  //This initial data has been provided externally.
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;

  //ID for the homogeneous dof
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];
  y[5] = z[5];
  y[6] = z[6];
  y[7] = z[7];

  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  

  double n = my_params_pointer->N;
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles  
  double k1 = cos(gam) * sin(bet);
  double k2 = sin(bet) * sin(gam);
  double k3 = cos(bet);

  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H = 1.0 /3.0 * (h1 + h2 + h3);

  //ID for the inhomogeneities dof
  //Here y[9+4*i] = reu; y[9+4*i+1] -> reu'; y[9+4*i+2] -> imu; y[9+4*i+3] -> imu'; 
  int i; 
  for(i = 0; i < n; i++){
    y[8+16*i] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[8+16*i+1] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[8+16*i+2] = 0.0;
    y[8+16*i+3] = (- sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    y[8+16*i+4] = 0.0;
    y[8+16*i+5] = 0.0; 
    y[8+16*i+6] = 0.0;
    y[8+16*i+7] = 0.0;

    y[8+16*i+8] = 0.0;
    y[8+16*i+9] = 0.0; 
    y[8+16*i+10] = 0.0;
    y[8+16*i+11] = 0.0;
    y[8+16*i+12] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[8+16*i+13] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[8+16*i+14] = 0.0;
    y[8+16*i+15] = ( - sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    k = k * dk;
    
  }
  
}


void
id_b1_BD_lqc_vac (double y[], double z[], void *params)
{
//   (void)(t);
  //This initial data has been provided externally.
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;

  //ID for the homogeneous dof
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  y[0] = z[0];
  y[1] = z[1];
  y[2] = z[2];
  y[3] = z[3];
  y[4] = z[4];
  y[5] = z[5];
  y[6] = z[6];
  y[7] = z[7];

  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  

  double n = my_params_pointer->N;
  double k = my_params_pointer->kmin;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles  
  double k1 = cos(gam) * sin(bet);
  double k2 = sin(bet) * sin(gam);
  double k3 = cos(bet);

  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H = 1.0 /3.0 * (h1 + h2 + h3);

  //ID for the inhomogeneities dof
  //Here y[9+4*i] = reu; y[9+4*i+1] -> reu'; y[9+4*i+2] -> imu; y[9+4*i+3] -> imu'; 
  int i; 
  for(i = 0; i < n; i++){
    y[8+16*i] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[8+16*i+1] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[8+16*i+2] = 0.0;
    y[8+16*i+3] = (- sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    y[8+16*i+4] = 0.0;
    y[8+16*i+5] = 0.0; 
    y[8+16*i+6] = 0.0;
    y[8+16*i+7] = 0.0;

    y[8+16*i+8] = 0.0;
    y[8+16*i+9] = 0.0; 
    y[8+16*i+10] = 0.0;
    y[8+16*i+11] = 0.0;
    y[8+16*i+12] = 1.0/sqrt(2.0 * pow(a,3) * k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)));
    y[8+16*i+13] = (-3.0 * H) / (2.0 * sqrt(2.0 * k * pow(a,3) * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3))))
      + (h1 * k1 * k1 /(a1 * a1) + h2 * k2 * k2 /(a2 * a2) + h3 * k3 * k3 /(a3 * a3)) / (2.0 * sqrt(2.0 * k * pow(a,3)) * pow(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3),1.25)); 
    y[8+16*i+14] = 0.0;
    y[8+16*i+15] = ( - sqrt(k * sqrt(k1 * k1 /(a1 * a1) + k2 * k2 /(a2 * a2) + k3 * k3 /(a3 * a3)))) / (sqrt(2.0 * pow(a,3)));
    k = k * dk;
    
  }
  
}


//Norms of the mode functions y[]

void
norms_b1_vac (const double y[], double nsol[], int l)
{
//   (void)(t);
  double v = exp((y[1] + y[2] + y[3]) / 2.0);
  int j;
  //These norms come from [mup,pmup]=i, [muc,pmuc]=i
  for(j = 0; j < 2; j++){
    nsol[j] = 2.0 * v * (y[8+8*j+16*l+2] * y[8+8*j+16*l+1] - y[8+8*j+16*l] * y[8+8*j+16*l+3] + y[12+8*j+16*l+2] * y[12+8*j+16*l+1] - y[12+8*j+16*l] * y[12+8*j+16*l+3]);
  }
  //These norms come from [v,mup]=0, [v,muc]=0, [mup,muc]=0
  nsol[3] = y[8+16*l+2] * y[12+16*l] - y[8+16*l] * y[12+16*l+2] + y[8+8+16*l+2] * y[12+8+16*l] - y[8+8+16*l] * y[12+8+16*l+2];
}