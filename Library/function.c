/*
These codes have been developed by J. Olmedo, V. Sreenath and I. Agullo. You are 
free to use them for any scientific or pedagogical purpose. However, the authors 
are not responsible of any mistake or typo in these codes. Nevertheless, we will 
acknowledge any feedback at this respect. Besides, if you plan to use these codes 
(or a modified version based on them) for scientific purposes (i.e. not pedagogical 
purposes), please, we will very much appreciate if you cite any of the following 
references: 
arXiv:1702.06036; arXiv:1601.01716.
Date: 05/11/19.
Place: Louisiana State University
*/


#include "functions.h"

/*************  Some observables for FRW in GR  *****************/


struct param_const {
/**
 *  Some global parameters.
 */
    double mass;
    double gamma;
    double G;
    double lambda;
    double lp;    
};

double
Vphi2 (const double y[], void *params)
{
/**
 *  Potential of a massive scalar field.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double V = pow(my_params_pointer->mass * y[2],2.0)/2.0;
  return V;
}


double
dVphi2 (const double y[], void *params)
{
/**
 *  1st derivative of the potential of a massive scalar field.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double dV = pow(my_params_pointer->mass,2.0) * y[2];
  return dV;
}


double
ddVphi2 (const double y[], void *params)
{
/**
 * 2nd derivative of the potential of a massive scalar field.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double ddV = pow(my_params_pointer->mass,2.0) ;
  return ddV;
}


double
Kphi (const double y[], void *params)
{
/**
 *  Standard kinetic energy density of a scalar field.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double r = pow(y[4],2)/2.0;
  return r;
}


double
rho (const double y[], void *params)
{
/**
 *  Energy density of a massive scalar field.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double r = pow(my_params_pointer->mass * y[2],2)/2.0 + pow(y[4],2)/2.0;
  return r;
}


double
rho_KV (double K, double V)
{
/**
 *  Energy density provided K and V.
 */
  double r = K + V;
  return r;
}


double
P_KV (double K, double V)
{
/**
 *  Pressure provided K and V.
 */
  double p = K - V;
  return p;
}


double
hubble (const double y[], void *params)
{
/**
 *  Hubble parameter.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double h = y[3]/(my_params_pointer->gamma);
  return h;
}


double
dhubble (const double y[], void *params)
{
/**
 * Time derivative (cosmic time) of the Hubble parameter.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double K = Kphi(y, &*my_params_pointer);
  double dh = - 8.0 * M_PI * my_params_pointer->G * K;
  return dh;
}


double
c_gr (const double y[], void *params)
{
/**
 * Densitized constraint (gravitational part).
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;
  double c = -(3.0 / (8.0 * M_PI * G * g * g)) * pow(y[3],2);
  return c;
}


double
constraint (const double y[], void *params)
{
/**
 *  Densitized (full) constraint.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double c = fabs(-(3.0/2.0)*pow(y[3],2)/(my_params_pointer->gamma)+4.0*M_PI*my_params_pointer->G*my_params_pointer->gamma*(pow(my_params_pointer->mass * y[2],2)/2.0 + pow(y[4],2)/2.0));
  return c;
}


double
rel_c (double C_gr, double Rho)
{
/**
 *  Densistized constraint provided $C_{gr}$ and $rho$.
 */
  double c = fabs(C_gr + Rho);
  return c;
}


double
dphi_sol (double C_gr, double V, double Dphi)
{
/**
 *  Solving $dot phi$ provided $C_{gr}$ and $V$.
 */
  double dphi;
  if(Dphi>0)
    dphi = sqrt(2.0 *(-C_gr - V));
  else
    dphi = -sqrt(2.0 *(-C_gr - V));
  return dphi;
}


double
ricci (const double y[], void *params)
{
/**
 *  Ricci scalar in GR $R = 6 (dot H + 2 H^2)$.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double r = 16.0*M_PI*my_params_pointer->G * (pow(my_params_pointer->mass * y[2],2) - pow(y[4],2)/2.0);
  return r;
}


double ricci_H (double H, double dH)
{
/**
 *  Ricci scalar provided $H$ and $dot H$.
 */
//   (void)(t);
  double r = 6.0 * (dH + 2.0 * H * H);
  return r;
}


double
slPS (const double y[], void *params)
{
/**
 *  Slow-roll power spectrum.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double h = (1.0/2.0)*sin(2.0*my_params_pointer->lambda*y[3])/(my_params_pointer->gamma*my_params_pointer->lambda);
  double ps = 1.0/(4.0 * M_PI * M_PI) * pow(pow(h,2)/y[4],2);
  return ps;
}


double
slns (const double y[], void *params)
{
/**
 *  Slow-roll ns (scalar spectra index).
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double ns = 1.0+2.0/(8.0 * M_PI * my_params_pointer->G) * 2.0 / pow(y[2],2) - 6.0 / (16.0 * M_PI * my_params_pointer->G) * 4.0 / pow(y[2],2);
  return ns;
}


double
sleps (const double y[], void *params)
{
/**
 *  Slow-roll parameter $epsilon_H$.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double mass = my_params_pointer->mass;
  double V = pow(mass*y[2],2)/2.0; // potential
  double dy3 = -(3.0/2.0)*pow(y[3],2)/g+4.0*M_PI*my_params_pointer->G* g *(V-pow(y[4],2)/2.0);
  double h = y[3]/g;
  double dh = dy3 / g;//time derivative (cosmic time) of hubble parameter
  double esp = -dh / h;
  return esp;
}


double
slepsV (const double y[], void *params)
{
/**
 *  Slow-roll parameter $epsilon_V$
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double mass = my_params_pointer->mass;
  double G = my_params_pointer->G;
  double V = pow(mass*y[2],2)/2.0; // potential
  double dV = pow(mass,2)*y[2]; //derivative of the potential
  double esp = 1.0 / (16.0 * M_PI * G) * pow(dV / V,2);
  return esp;
}


double
sletaV (const double y[], void *params)
{
/**
 *  Slow-roll parameter $eta_V$
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double mass = my_params_pointer->mass;
  double G = my_params_pointer->G;
  double V = pow(mass*y[2],2)/2.0; // potential
  double ddV = pow(mass,2); //2nd derivative of the potential
  double esp = 1.0 / (8.0 * M_PI * G) * ddV / V;
  return esp;
}


/****************  Some observables for FRW in LQC  *************************/


double Vpf_lqc (const double y[], void *params)
{
/**
 * Scalar field potential that reproduces perfect fluid dynamics. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double V0 = my_params_pointer->mass;
  double w = 0.0 / 3.0;
  double rho_c = 3.0/(8.0 * M_PI * pow(g * l,2));
  double kappa = 3.0 * (8.0 * M_PI * G);
  double V = V0 * (exp(sqrt(kappa * (1.0 + w)) * y[2]) / pow(1.0 + V0 * exp(sqrt(kappa * (1.0 + w)) * y[2]) / (2 * rho_c * (1.0 - w)),2.0));
  return V;
}

double dVpf_lqc (const double y[], void *params)
{
/**
 * Derivative of the scalar field potential that reproduces perfect fluid dynamics. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double V0 = my_params_pointer->mass;
  double w = 0.0 / 3.0;
  double rho_c = 3.0/(8.0 * M_PI * pow(g * l,2));
  double kappa = 3.0 * (8.0 * M_PI * G);
  double dV = 4.0*V0*pow(kappa,0.5)*exp(y[2]*pow(kappa,0.5)*pow(1.0 + w,0.5))*(2.0*rho_c*(-1.0 + w) + V0*exp(y[2]*pow(kappa,0.5)*pow(1.0 + w,0.5)))*pow(rho_c,2.0)*pow(-1.0 + w,2.0)*pow(1.0 + w,0.5)* pow(2.0*rho_c*(-1.0 + w) - V0*exp(y[2]*pow(kappa,0.5)*pow(1.0 + w,0.5)),-3.0);
  //double dV = V0 * sqrt(3.0 * (1.0 + w)) * exp(sqrt(3.0 * (1.0 + w)) * y[2]);
  return dV;
}

double ddVpf_lqc (const double y[], void *params)
{
/**
 * 2nd derivative of the scalar field potential that reproduces perfect fluid dynamics.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double V0 = my_params_pointer->mass;
  double w = 0.0 / 3.0;
  double rho_c = 3.0/(8.0 * M_PI * pow(g * l,2));
  double kappa = 3.0 * (8.0 * M_PI * G);
  double ddV = 4.0 * kappa * V0 * (1.0 + w) * exp(y[2] * pow(kappa * (1.0 + w),0.5)) * pow(rho_c,2.0) * pow(-1.0 + w,2.0) * (8.0 * rho_c * V0 * (-1.0 + w) * exp(y[2] * pow(kappa * (1.0 + w),0.5)) + exp(2.0 * y[2] * pow(kappa * (1.0 + w),0.5)) * pow(V0,2.0) + 4.0 * pow(rho_c,2.0) * pow(-1.0 + w,2.0)) * pow(-2.0 * rho_c * (-1.0 + w) + V0 * exp(y[2] * pow(kappa * (1.0 + w),0.5)),-4.0);
  return ddV;
}


double
rhoQ_H (double H, double Rho, void *params)
{
/**
 * Quantum effective energy density provided $H$ and $rho$.
 */
  struct param_const *my_params_pointer = params;
  double kappa = 8.0 * M_PI * my_params_pointer->G;
  double rhoq = 3.0 * H * H / kappa - Rho;
  return rhoq;
}


double
PQ_H (double H, double dH, double Rho, double P, void *params)
{
/**
 * Quantum effective pressure provided $H$, $dot H$ and $P$. 
 */
  struct param_const *my_params_pointer = params;
  double kappa = 8.0 * M_PI * my_params_pointer->G;
  double rhoq = 3.0 * H * H / kappa - Rho;
  double pq = - 2.0 * dH / (kappa) - Rho - P - rhoq;
  return pq;
}


double
hubble_lqc (const double y[], void *params)
{
/**
 * Hubble parameter in LQC. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double h = (1.0/2.0)*sin(2.0*my_params_pointer->lambda*y[3])/(my_params_pointer->gamma*my_params_pointer->lambda);
  return h;
}


double
dhubble_lqc (const double y[], void *params)
{
/**Time derivative (cosmic time) of the Hubble parameter in LQC.
 *  
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double K = Kphi(y, &*my_params_pointer);
  double dh = (cos(2.0*my_params_pointer->lambda*y[3])) * (- 8.0 * M_PI * my_params_pointer->G * K);
  return dh;
}


double
c_gr_lqc (const double y[], void *params)
{
/**
 * Densitized gravitational constraint in LQC. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double c = -(3.0/(8.0*M_PI*my_params_pointer->G))*pow(sin(my_params_pointer->lambda*y[3]),2)/(pow(my_params_pointer->gamma*my_params_pointer->lambda,2));
  return c;
}


double
constraint_lqc (const double y[], void *params)
{
/**
 * Densitized full constraint in LQC.
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double c = fabs(-(3.0/(8.0*M_PI*my_params_pointer->G))*pow(sin(my_params_pointer->lambda*y[3]),2)/(pow(my_params_pointer->gamma*my_params_pointer->lambda,2))+(pow(my_params_pointer->mass * y[2],2)/2.0 + pow(y[4],2)/2.0));
  return c;
}


double
slPS_lqc (const double y[], void *params)
{
/**
 * Slow-roll power spectrum. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double h = (1.0/2.0)*sin(2.0*my_params_pointer->lambda*y[3])/(my_params_pointer->gamma*my_params_pointer->lambda);
  double ps = 1.0/(4.0 * M_PI * M_PI) * pow(pow(h,2)/y[4],2);
  return ps;
}


double
sleps_lqc (const double y[], void *params)
{
/**
 * Slow-roll parameter $epsilon_H$ 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double mass = my_params_pointer->mass;
  double V = pow(mass*y[2],2)/2.0; // potential
  double dy3 = -(3.0/2.0)*pow(sin(l * y[3]),2)/(g * pow(l,2))+4.0*M_PI*my_params_pointer->G* g *(V-pow(y[4],2)/2.0);
  double h = (1.0/2.0)*sin(2.0 * l * y[3])/(g * l);
  double dh = cos(2.0 * l * y[3]) / g * dy3;//time derivative (cosmic time) of hubble parameter
  double esp = -dh / h;
  return esp;
}


void
ss_hyb (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of perturbations for the hybrid quantization. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double K = KVfs[1];
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential
  //EOMS for the perturbations

  double Rho = rho_KV(K,V);
  double sin2 = pow(sin(l * y[3])/(g * l),2);
  double sin2b = sin(2.0 * l * y[3])/(2.0 * g * l);
  double h2 = sin2 - 8.0 * M_PI * G / 3.0 * V;
  
  //ss[0] = h2 * (19.0 - 18.0 * h2 / sin2) + (ddV + 16.0 * M_PI * G * y[4] * sin2b / sin2 * dV - 16.0 * M_PI * G / 3.0 * V);//time-dependent mass of the MS-tensor variable divided by a^2
  ss[0] = (h2 * (1.0 + 18.0 * V / Rho) + (ddV + 6.0 * y[4] * sin2b / Rho * dV - 16.0 * M_PI * G / 3.0 * V));//time-dependent mass of the MS-tensor variable divided by a^2
  ss[1] = -(8.0 * M_PI * G) * V + sin2;//time-dependent mass of the MS-tensor variable divided by a^2
}



void
ss_drsd (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of perturbations for the dressed quantization. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double rho_c = 3.0 / ( 8.0 * M_PI * G * pow(g * l, 2.0));
  
  double h = KVfs[0];
  double K = KVfs[1];
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential
  double dh = KVfs[5];
  //EOMS for the perturbations

  double Rho = rho_KV(K,V);
  double P = Rho - 2.0 * V;//time derivative (cosmic time) of hubble parameter
  double f = 24.0 * M_PI * G / Rho;
  double calV = (ddV + (f * y[4] * y[4]) * V - 2.0 * sqrt(f) * y[4] * dV);
  //double f = 48.0 * M_PI * G * K / Rho;
  //double calV = (ddV + f * V + 2.0 * sqrt(f) * dV);
  ss[1] = - (2.0 *h * h + dh);
  //ss[1] = - 4.0 * M_PI * G / 3.0 * Rho * (1 + 2.0 * Rho / rho_c) + 4.0 * M_PI * G * P * (1 - 2.0 * Rho / rho_c);//time-dependent mass of the MS-tensor variable divided by a^2
  ss[0] = calV + ss[1];//time-dependent mass of the MS-scalar variable divided by a^2
}


void
ss_ppu (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of scalar perturbations for the Pereira-Pitrou-Uzan proposal. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double H = KVfs[0]; // Hubble
  double K = KVfs[1]; // Kinetic energy
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential
  double dH = KVfs[5]; //1st derivative of Hubble param
  //EOMS for the perturbations

  double Rho = rho_KV(K,V);
  double dRho = -6.0 * H * K;
  
  double Hpmspp = (3.0 * H) / (16.0 * M_PI * G * Rho);
  double dHpmspp = 3.0 * dH / (16.0 * M_PI * G * Rho) - Hpmspp * dRho / Rho ;
  
  //Time-dependent potentials
  ss[1] = -2.0 * H * H - dH;
  ss[0] = ss[1] + ddV - 16.0 * M_PI * G * (2.0 * K * dHpmspp + Hpmspp * (- 6.0 * H * K - 2.0 * dV * y[4]));

}


/****************  Some observables for FRW in QRLG  *************************/

struct param_int {
/**
 * Some useful parameters in QRLG 
 */
  double sigma;
  double lb;

};


double intnd0 (double x, void * params) {
/**
 * Integral 0 required for the evolution.
 */
  struct param_int *psvs_pointer = (struct  param_int *)params;
  double Ns = x / sqrt(psvs_pointer->sigma) + 1.0;
  double intnd0 = exp(-x * x) * pow(Ns * Ns,1.0/3.0) * pow(sin(pow(1.0/Ns,1.0/3.0) * psvs_pointer->lb),2);
  return intnd0;
}

double intnd1 (double x, void * params) {
/**
 * Integral 1 required for the evolution.
 */
  struct param_int *psvs_pointer = (struct  param_int *)params;
  double Ns = 1.0 + x / sqrt(psvs_pointer->sigma);
  double intnd1 = exp(-x * x) * pow(Ns ,1.0/3.0) * sin(2.0 * pow(1.0/Ns,1.0/3.0) * psvs_pointer->lb);
  return intnd1;
}


double intnd2 (double x, void * params) {
/**
 * Integral 2 required for the evolution. 
 */
  struct param_int *psvs_pointer = (struct  param_int *)params;
  double Ns = 1.0 + x / sqrt(psvs_pointer->sigma);
  double intnd2 = exp(-x * x) * pow(Ns * Ns,1.0/3.0) * pow(sin(pow(1.0/Ns,1.0/3.0) * psvs_pointer->lb),2) * (x * x / psvs_pointer->sigma + 2.0 * x / sqrt(psvs_pointer->sigma));
  return intnd2;
}

double intnd3 (double x, void * params) {
/**
 * Integral 3 required for the evolution.
 */
  struct param_int *psvs_pointer = (struct  param_int *)params;
  double Ns = 1.0 + x / sqrt(psvs_pointer->sigma);
  double intnd3 = exp(-x * x) * pow(Ns,1.0/3.0) * sin(2.0 * pow(1.0/Ns,1.0/3.0) * psvs_pointer->lb) * (x * x + 2.0 * x * sqrt(psvs_pointer->sigma));
  return intnd3;
}

double intnd4 (double x, void * params) {
/**
 * Integral 4 required for the evolution.
 */
  struct param_int *psvs_pointer = (struct  param_int *)params;
  double Ns = 1.0 + x / sqrt(psvs_pointer->sigma);
  //double intnd4 = exp(-x * x) * pow(Ns,2.0/3.0) * cos(2.0 * pow(1.0/Ns,1.0/3.0) * psvs_pointer->lb);
  double intnd4 = exp(-x * x) * cos(2.0 * pow(1.0/Ns,1.0/3.0) * psvs_pointer->lb);
  return intnd4;
}

double intgr (const gsl_function * f) {
/**
 * Main integration algorithm.
 */
  struct param_int *my_params_pointer = f->params;
  gsl_integration_workspace * w
  = gsl_integration_workspace_alloc (2000);

  gsl_function F;
  F.function = f->function;
  F.params = f->params;

  double xmax = sqrt(my_params_pointer->sigma);
  double xmin = -xmax + 1.0 / xmax;
  if(xmax > 8.0/sqrt(2.0)){//below 6.0/sqrt(2.0) the error is important!!!
    xmax = 8.0/sqrt(2.0);
    xmin = -xmax + 1.0 / xmax;
    }
    
  double result, error;

  //gsl_integration_qags (&F, xmin, xmax, 1.0e-12, 1.0e-9, 1000,
		       //w, &result, &error);

  // GSL_INTEG_GAUSS15; GSL_INTEG_GAUSS21; GSL_INTEG_GAUSS31; GSL_INTEG_GAUSS41; GSL_INTEG_GAUSS51; GSL_INTEG_GAUSS61;
  gsl_integration_qag (&F, xmin, xmax, 1.0e-12, 1.0e-9, 2000,
		       GSL_INTEG_GAUSS61, w, &result, &error);
  //  if(fabs(error/result)>1.0e-2)
  //printf ("carefull with integral \n");

  gsl_integration_workspace_free (w);

  return result;
}



double
hubble_rlqg (const double y[], void *params)
{
/**
 * Hubble parameter in QRLG. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;

  double sigma = exp(y[1])/pow(my_params_pointer->lambda,3.0);
  double lb = y[3] * my_params_pointer->lambda;

  struct param_int param = {sigma, lb};

  gsl_function F;
  F.function = &intnd1;
  F.params = &param;

  double INT1;

  INT1 = intgr(&F);

  double factor = 1.0 / (2.0 * sqrt(M_PI) * my_params_pointer->gamma * my_params_pointer->lambda);  
  double h = factor * INT1;
  return h;
}


double
dhubble_rlqg (const double y[], void *params)
{
/**
 * Time derivative of the Hubble parameter in QRLG. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;

  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;

  double sigma = exp(y[1])/pow(l,3.0);
  double lb = y[3] * l;

  struct param_int param = {sigma, lb};

  gsl_function F;
  F.function = &intnd1;
  F.params = &param;

  gsl_function F2;
  F2.function = &intnd2;
  F2.params = &param;

  gsl_function F3;
  F3.function = &intnd3;
  F3.params = &param;

  gsl_function F4;
  F4.function = &intnd4;
  F4.params = &param;

  double INT1, INT2, INT3, INT4;

  INT1 = intgr(&F);
  INT2 = intgr(&F2);
  INT3 = intgr(&F3);
  INT4 = intgr(&F4);

  //Here we define the hubble parameter
  double factor = 1.0 / (2.0 * sqrt(M_PI) * g * l);
  double h = factor * INT1;

  //Here we define the time derivative of b
  double dH = - 3.0 * pow(2.0 , 2.0/3.0) / (g * pow(l,2.0) * sqrt(M_PI)) * sqrt(sigma) * exp(-sigma) * pow(sin(l * y[3] / pow(2.0,1.0/3.0)),2.0);
  double V = Vphi2(y, &*my_params_pointer);
  double db = M_PI * my_params_pointer->G * g / 3.0 * (-5.0 * pow(y[4],2) + 14.0 * V) + dH - y[3] * factor * INT1 - 3.0 * sigma * factor * INT2 / l;

  double ddH = 3.0 * pow(2.0,4.0/3.0) * h * sqrt(sigma) * exp(-sigma) * sin(pow(2.0,2.0/3.0) * l * y[3]);
  //double dh = - 5.0 * h * h / 2.0 + factor * (ddH + 3.0 * h * INT3 + (2.0 / l) * INT4 * (db + y[3] * h));//time derivative (cosmic time) of hubble
  double dh = - 5.0 * h * h / 2.0 +  factor * (ddH + 3.0 * h * INT3 + (2.0 * l) * INT4 * (db + y[3] * h));//time derivative (cosmic time) of hubble
  return dh;
}


double
c_gr_rlqg (const double y[], void *params)
{
/**
 * Densitized gravitational constraint in QRLG. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;

  double sigma = exp(y[1])/pow(my_params_pointer->lambda,3.0);
  double lb = y[3] * my_params_pointer->lambda;

  struct param_int param = {sigma, lb};

  gsl_function F;
  F.function = &intnd0;
  F.params = &param;

  double INT0;

  INT0 = intgr(&F);

  double c = -3.0 / (8.0 * pow(M_PI,3.0/2.0) * my_params_pointer->G * pow(my_params_pointer->gamma * my_params_pointer->lambda,2.0)) * INT0;
  return c;
}


double
constraint_rlqg (const double y[], void *params)
{
/**
 * Densitized full constraint in QRLG. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;

  double sigma = exp(y[1])/pow(my_params_pointer->lambda,3.0);
  double lb = y[3] * my_params_pointer->lambda;

  struct param_int param = {sigma, lb};

  gsl_function F;
  F.function = &intnd0;
  F.params = &param;

  double INT0;

  INT0 = intgr(&F);

  double c = fabs(-3.0 / (8.0 * pow(M_PI,3.0/2.0) * my_params_pointer->G * pow(my_params_pointer->gamma * my_params_pointer->lambda,2.0)) * INT0 / (pow(my_params_pointer->mass * y[2],2)/2.0 + pow(y[4],2)/2.0) + 1.0);
  return c;
}


double
ricci_rlqg (const double y[], void *params)
{
/**
 * Ricci scalar in QRLG. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double l = my_params_pointer->lambda;

  double sigma = exp(y[1])/pow(my_params_pointer->lambda,3.0);
  double lb = y[3] * my_params_pointer->lambda;

  struct param_int param = {sigma, lb};

  gsl_function F;
  F.function = &intnd1;
  F.params = &param;

  gsl_function F3;
  F3.function = &intnd3;
  F3.params = &param;

  gsl_function F4;
  F4.function = &intnd4;
  F4.params = &param;

  double INT1, INT3, INT4;

  INT1 = intgr(&F);
  INT3 = intgr(&F3);
  INT4 = intgr(&F4);

  double factor = 1.0 / (2.0 * sqrt(M_PI) * my_params_pointer->gamma * my_params_pointer->lambda);
  double h = factor * INT1;
  double ddH = pow(2.0 , 4.0/3.0) / (pow(l,3.0) * sqrt(sigma)) * exp(-sigma) * sin(2.0 * l * y[3] / pow(2.0,1.0/3.0));
  double dh = factor * (ddH - 5.0 * h / (6.0 * exp(y[1])) + 2.0 * y[3] * l * INT4 / (3.0 * exp(y[1])) + INT3 / pow(l,3.0));//time derivative (cosmic time) of hubbl
  double r = 6.0 * (dh + 2.0 * h * h);
  return r;
}


double
slPS_rlqg (const double y[], void *params)
{
/**
 * Slow-roll power spectrum in QRLG. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double sigma = exp(y[1])/pow(my_params_pointer->lambda,3.0);
  double lb = y[3] * my_params_pointer->lambda;

  struct param_int param = {sigma, lb};

  gsl_function F;
  F.function = &intnd1;
  F.params = &param;

  double INT1;

  INT1 = intgr(&F);

  double factor = 1.0 / (2.0 * sqrt(M_PI) * my_params_pointer->gamma * my_params_pointer->lambda);
  double h = factor * INT1;
  double ps = 1.0/(4.0 * M_PI * M_PI) * pow(pow(h,2)/y[4],2);
  return ps;
}


void
ss_rlqg_hyb (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of perturbations for the hybrid quantization in QRLG. 
 */
  struct param_const *my_params_pointer = params;
  //double g = my_params_pointer->gamma;
  //double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double h = KVfs[0];
  double K = KVfs[1];
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential

  double Rho = rho_KV(K,V);
  double sin2 = h * h;
  double sin2b = h;
  double h2 = sin2 - 8.0 * M_PI * G / 3.0 * V;
  
  ss[0] = h2 * (19.0 - 18.0 * (1.0 - V / Rho)) + (ddV + 6.0 * y[4] * sin2b / Rho * dV - 16.0 * M_PI * G / 3.0 * V);//time-dependent mass of the MS-scalar variable divided by a^2
  ss[1] = -(8.0 * M_PI * G) * V + sin2;//time-dependent mass of the MS-tensor variable divided by a^2 
}


void
ss_rlqg_hyb_mink (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of perturbations for the hybrid quantization with the factor 3 * H^2/(8 *pi *G * rho) in QRLG. 
 */
  struct param_const *my_params_pointer = params;
  //double g = my_params_pointer->gamma;
  //double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double h = KVfs[0];
  double K = KVfs[1];
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential

  double Rho = rho_KV(K,V);
  double sin2 = h * h;
  double sin2b = h;
  double h2 = sin2 - 8.0 * M_PI * G / 3.0 * V;
  
  ss[0] = (3.0 * sin2 / (8.0 * M_PI * G * Rho)) * (h2 * (19.0 - 18.0 * (1.0 - V / Rho)) + (ddV + 6.0 * y[4] * sin2b / Rho * dV - 16.0 * M_PI * G / 3.0 * V));//time-dependent mass of the MS-scalar variable divided by a^2
  ss[1] = (3.0 * sin2 / (8.0 * M_PI * G * Rho)) * (-(8.0 * M_PI * G) * V + sin2);//time-dependent mass of the MS-tensor variable divided by a^2 
}


void
ss_rlqg_drsd (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of perturbations for the dressed quantization in QRLG. 
 */
  struct param_const *my_params_pointer = params;
  //double g = my_params_pointer->gamma;
  //double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double h = KVfs[0];
  double K = KVfs[1];
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential
  double dh = KVfs[5]; //derivative of the hubble parameter
  //double rho_c = 3.0 / ( 8.0 * M_PI * G * pow(g * l, 2.0));

  double Rho = rho_KV(K,V);
  //double P = Rho - 2.0 * V;//time derivative (cosmic time) of hubble parameter
  double f = 24.0 * M_PI * G / Rho;
  double calV = (ddV + (f * y[4] * y[4]) * V + 2.0 * sqrt(f) * y[4] * dV);
  //ss[1] = - 4.0 * M_PI * G / 3.0 * Rho * (1 + 2.0 * Rho / rho_c) + 4.0 * M_PI * G * P * (1 - 2.0 * Rho / rho_c);//time-dependent mass of the MS-tensor variable divided by a^2
  ss[1] = -2.0 * h * h - dh;//time-dependent mass of the MS-tensor variable divided by a^2
  ss[0] = calV + ss[1];//time-dependent mass of the MS-scalar variable divided by a^2
}


void
ss_rlqg_drsd_mink (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of perturbations for the dressed quantization with extra factors in QRLG. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  //double g = my_params_pointer->gamma;
  //double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  //double rho_c = 3.0 / ( 8.0 * M_PI * G * pow(g * l, 2.0));
  
  double H = KVfs[0]; // Hubble parameter
  double K = KVfs[1]; //Kinetic energy density
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential
  double dH = KVfs[5]; //derivative of the hubble parameter 
  //EOMS for the perturbations

  double Rho = rho_KV(K,V);
  //double P = Rho - 2.0 * V;//time derivative (cosmic time) of hubble parameter
  double f = 24.0 * M_PI * G / Rho;
  double calV = (ddV + (f * y[4] * y[4]) * V + 2.0 * sqrt(f) * y[4] * dV);
  //double f = 48.0 * M_PI * G * K / Rho;
  //double calV = (ddV + f * V - 2.0 * sqrt(f) * dV);
  //ss[1] = 3.0 * H * H / (8.0 * M_PI * G * Rho) * (- 4.0 * M_PI * G / 3.0 * Rho * (1 + 2.0 * Rho / rho_c) + 4.0 * M_PI * G * P * (1 - 2.0 * Rho / rho_c));//time-dependent mass of the MS-tensor variable divided by a^2
  ss[1] = 3.0 * H * H / (8.0 * M_PI * G * Rho) * (-2.0 * H * H - dH);//time-dependent mass of the MS-tensor variable divided by a^2
  ss[0] = 3.0 * H * H / (8.0 * M_PI * G * Rho) * calV + ss[1];//time-dependent mass of the MS-scalar variable divided by a^2
}


void
ss_rlqg (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of perturbations for the rlqg quantization proposal in QRLG. 
 */
  struct param_const *my_params_pointer = params;
  //double g = my_params_pointer->gamma;
  //double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double h = KVfs[0];
  double K = KVfs[1];
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential
  double dh = KVfs[5]; //derivative of the hubble parameter 

  double Rho = rho_KV(K,V);
  double kappa = 8.0 * M_PI * G;
  double calV = (ddV + 6.0 * kappa * V * (1.0 - V / Rho) + 6.0 * y[4] * h / Rho * dV);
  ss[1] = (-2.0 * h * h - dh);//time-dependent mass of the MS-tensor variable divided by a^2
  ss[0] = calV + ss[1];//time-dependent mass of the MS-scalar variable divided by a^2
}


void
ss_rlqg_mink (const double y[], const double KVfs[], double ss[], void *params)
{
/**
 * Time-dependent mass of perturbations for the rlqg quantization proposal with the factor 3 * H^2/(8 *pi *G * rho) in QRLG. 
 */
  struct param_const *my_params_pointer = params;
  //double g = my_params_pointer->gamma;
  //double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double h = KVfs[0];
  double K = KVfs[1];
  double V = KVfs[2]; // potential
  double dV = KVfs[3]; //derivative of the potential
  double ddV = KVfs[4]; //2nd derivative of the potential
  double dh = KVfs[5]; //derivative of the hubble parameter 

  double Rho = rho_KV(K,V);
  double kappa = 8.0 * M_PI * G;
  double calV = (ddV + 6.0 * kappa * V * (1.0 - V / Rho) + 6.0 * y[4] * h / Rho * dV);
  ss[1] = 3.0 * h * h / (kappa * Rho) * (-2.0 * h * h - dh);//time-dependent mass of the MS-tensor variable divided by a^2
  ss[0] = 3.0 * h * h / (kappa * Rho) * calV + ss[1];//time-dependent mass of the MS-scalar variable divided by a^2
}


/****************  Some observables for FRW in RLQG with suddle point approximation *************************/


double
hubble_rlqg_sp (const double y[], void *params)
{
/**
 * Hubble parameter in QRLG with suddle point approximation. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;

  double h = (1.0 / 3.0) * ((y[3] * pow(l,3.0) * cos(2.0 * y[3] * l)) / (exp(y[1]) * 6.0 * g) + (3.0 * sin(2.0 * y[3] * l))/(2.0 * g * l) - (l * l * sin(2.0 * y[3] * l))/(12.0 * g * exp(y[1])) - (y[3] * y[3] * pow(l,4.0) * sin(2.0 * y[3] * l)) / (6.0 * g * exp(y[1])));
  return h;
}


double
dhubble_rlqg_sp (const double y[], void *params)
{
/**
 * Time derivative (cosmic time) of the Hubble parameter in QRLG with suddle point approximation. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double K = Kphi(y, &*my_params_pointer);
  double V = Vphi2(y, &*my_params_pointer);

  double dh = -4.0*G*K*M_PI*cos(2.0*y[3]*l) + 4.0*G*M_PI*V*cos(2.0*y[3]*l) +(3.0*pow(g,-2.0)*pow(l,-2.0))/8.0 -(3.0*cos(2.0*y[3]*l)*pow(g,-2.0)*pow(l,-2.0))/4.0 +(3.0*cos(4.0*y[3]*l)*pow(g,-2.0)*pow(l,-2.0))/8.0 -(pow(g,-2.0)*pow(l,4.0)*exp(-2.0 * y[1]))/864.0 +(cos(4.0*y[3]*l)*pow(g,-2.0)*pow(l,4.0)*exp(-2.0 * y[1]))/864.0 -(pow(y[3],2.0)*pow(g,-2.0)*pow(l,6.0)*exp(-2.0 * y[1]))/108.0 -(pow(y[3],4.0)*pow(g,-2.0)*pow(l,8.0)*exp(-2.0 * y[1]))/216.0 +(cos(4.0*y[3]*l)*pow(y[3],4.0)*pow(g,-2.0)*pow(l,8.0)*exp(-2.0 * y[1]))/216.0 +(l*pow(g,-2.0)*exp(-y[1]))/48.0 -(l*cos(4.0*y[3]*l)*pow(g,-2.0)*exp(-y[1]))/48.0 +(cos(2.0*y[3]*l)*pow(y[3],2.0)*pow(g,-2.0)*pow(l,3.0)*exp(-y[1]))/12.0 -(cos(4.0*y[3]*l)*pow(y[3],2.0)*pow(g,-2.0)*pow(l,3.0)*exp(-y[1]))/12.0 +(4.0*G*K*M_PI*cos(2.0*y[3]*l)*pow(y[3],2.0)*pow(l,5.0)*exp(-y[1]))/9.0 -(4.0*G*M_PI*V*cos(2.0*y[3]*l)*pow(y[3],2.0)*pow(l,5.0)*exp(-y[1]))/9.0 +(y[3]*pow(g,-2.0)*pow(l,2.0)*exp(-y[1])*sin(2.0*y[3]*l))/6.0 +(8.0*y[3]*G*K*M_PI*pow(l,4.0)*exp(-y[1])*sin(2.0*y[3]*l))/9.0 -(8.0*y[3]*G*M_PI*V*pow(l,4.0)*exp(-y[1])*sin(2.0*y[3]*l))/9.0 +(y[3]*pow(g,-2.0)*pow(l,5.0)*exp(-2.0 * y[1])*sin(4.0*y[3]*l))/216.0 +(pow(y[3],3.0)*pow(g,-2.0)*pow(l,7.0)*exp(-2.0 * y[1])*sin(4.0*y[3]*l))/108.0 -(y[3]*pow(g,-2.0)*pow(l,2.0)*exp(-y[1])*sin(4.0*y[3]*l))/8.0;
  return dh;
}


double
c_gr_rlqg_sp (const double y[], void *params)
{
/**
 * Densitized gravitational constraint in QRLG with suddle point approximation. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;

  double c = (-3.0 * pow(sin(l * y[3]),2.0)) / (8.0 * pow(g,2.0) * G * pow(l,2.0) * M_PI) + (l * pow(sin(l * y[3]),2.0)) / (48.0 * pow(g,2.0) * G * M_PI * exp(y[1])) - (pow(l,3.0) * cos(2.0 * l * y[3]) * pow(y[3],2.0)) / (48.0 * pow(g,2.0) * G * M_PI * exp(y[1]));
  return c;
}


double
constraint_rlqg_sp (const double y[], void *params)
{
/**
 * Densitized full constraint in QRLG with suddle point approximation. 
 */
//   (void)(t);
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double K = Kphi(y, &*my_params_pointer);
  double V = Vphi2(y, &*my_params_pointer);

  double c = fabs(((-3.0 * pow(sin(l * y[3]),2.0)) / (8.0 * pow(g,2.0) * G * pow(l,2.0) * M_PI) + (l * pow(sin(l * y[3]),2.0)) / (48.0 * pow(g,2.0) * G * M_PI * exp(y[1])) - (pow(l,3.0) * cos(2.0 * l * y[3]) * pow(y[3],2.0)) / (48.0 * pow(g,2.0) * G * M_PI * exp(y[1]))) / (K + V) + 1.0);
  return c;
}


/****************  Some observables for Bianchi I in GR  *************************/


struct param_const_b1 {
/**
 * Some useful parameters in Bianchi I. 
 */
  double mass;
  double gamma;
  double G;
  double lambda;
  double lp;    
  double alp;
  double bet;
  double gam; 
  double kmin;
  double kmax; 
  int N; 
};

double
Vphi2_b1 (const double y[], void *params)
{
/**
 * Potential energy of a massive scalar field in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double r = pow(my_params_pointer->mass * y[4],2.0)/2.0;
  return r;
}


double
dVphi2_b1 (const double y[], void *params)
{
/**
 * 1st derivative of the potential energy of a massive scalar field in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double r = pow(my_params_pointer->mass,2.0) * y[4];
  return r;
}


double
ddVphi2_b1 (const double y[], void *params)
{
/**
 * 2nd derivative of the potential energy of a massive scalar field in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double r = pow(my_params_pointer->mass,2.0);
  return r;
}


double
Kphi_b1 (const double y[], void *params)
{
/**
 * Standard kinetic energy density of a scalar field in Bianchi I. 
 */
  double r = pow(y[8],2.0)/2.0;
  return r;
}


double
rho_b1 (const double y[], void *params)
{
/**
 * Energy density in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double r = pow(my_params_pointer->mass * y[4],2)/2.0 + pow(y[8],2)/2.0;
  return r;
}


double
hubble_b1 (const double y[], void *params)
{
/**
 * Average Hubble parameter in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double h = 1.0/6.0 * (1.0 / (g * l) * (y[7] + y[6])
		       + 1.0 / (g * l) * (y[5] + y[7])
		       + 1.0 / (g * l) * (y[6] + y[5]));
  return h;
}


double
dhubble_b1 (const double y[], void *params)
{
/**
 * Time derivative (in cosmic time) of the average Hubble parameter in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double h = 1.0/3.0 / (g * l) * (y[5] + y[6] + y[7]);
  //These are the 1st derivatives: logpi'
  double f1 = 1.0 / (g * l) * (y[7] + y[6]);
  double f2 = 1.0 / (g * l) * (y[5] + y[7]);
  double f3 = 1.0 / (g * l) * (y[6] + y[5]);
  //These are the 1st derivatives: bi'
  double f5 = 1.0/2.0
    * ( + y[6] * f2
	+ y[7] * f3
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[5] * (f2 + f3));

  double f6 = 1.0/2.0
    * ( + y[7] * f3 
	+ y[5] * f1  
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[6] * (f3 + f1));

  double f7 =  1.0/2.0 
    * ( + y[5] * f1  
	+ y[6] * f2  
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[7] * (f1 + f2));
  double dh = 1.0/3.0 / (g * l) * (f5 + f6 + f7);
  
  return dh;
}


double
c_gr_b1 (const double y[], void *params)
{
/**
 * Gravitational part of the densistized constraint in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double c = -(1.0/(8.0 * M_PI * my_params_pointer->G * pow(l * g,2))) * (y[5] * y[6] + y[7] * y[6]  + y[5] * y[7]);
  return c;
}


double
constraint_b1 (const double y[], void *params)
{
/**
 * Densistized full constraint in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double c = fabs(-(1.0/(8.0 * M_PI * my_params_pointer->G * pow(l * g,2))) * (y[5] * y[6] + y[7] * y[6]  + y[5] * y[7]) + (pow(y[8],2) + pow(my_params_pointer->mass * y[4],2)) / 2.0);
  return c;
}


double
ricci_b1 (const double y[], void *params)
{
/**
 * Average ricci scalar in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double r = 16.0*M_PI*my_params_pointer->G * (pow(my_params_pointer->mass * y[4],2) - pow(y[8],2)/2.0);
  return r;
}

double
ddaoa_b1 (const double y[], void *params)
{
/**
 * This is $a''/a$ in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  //These are the 1st derivatives: logpi'
  double f1 = 1.0 / (g * l) * (y[7] + y[6]);
  double f2 = 1.0 / (g * l) * (y[5] + y[7]);
  double f3 = 1.0 / (g * l) * (y[6] + y[5]);
  //These are the 1st derivatives: bi'
  double f5 = 1.0/2.0
    * ( + y[6] * f2
	+ y[7] * f3
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[5] * (f2 + f3));

  double f6 = 1.0/2.0
    * ( + y[7] * f3 
	+ y[5] * f1  
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[6] * (f3 + f1));

  double f7 =  1.0/2.0 
    * ( + y[5] * f1  
	+ y[6] * f2  
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[7] * (f1 + f2));

  //These are the 2nd derivatives: logpi''
  double df1 = 1.0 / (g * l) * (- f5 * y[5] * (y[7] + y[6]) + (f7 + f6 ));
  double df2 = 1.0 / (g * l) * (- f6 * y[6] * (y[5] + y[7]) + (f5 + f7 ));
  double df3 = 1.0 / (g * l) * (- f7 * y[7] * (y[6] + y[5]) + (f6 + f5 ));
  double DDaoa = 1.0 / 6.0 * (df1 + df2 + df3) + 1.0 / 36.0 * (f1 + f2 + f3 ) * (f1 + f2 + f3 );
  return DDaoa;
}


double
h1 (const double y[], void *params)
{
/**
 * Directional Hubble rate for a_1 in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * (y[7] + y[6]);
  double f2 = 1.0 / (g * l) * (y[5] + y[7]);
  double f3 = 1.0 / (g * l) * (y[6] + y[5]);
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  return h1;
}


double
h2 (const double y[], void *params)
{
/**
 * Directional Hubble rate for a_2 in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * (y[7] + y[6]);
  double f2 = 1.0 / (g * l) * (y[5] + y[7]);
  double f3 = 1.0 / (g * l) * (y[6] + y[5]);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  return h2;
}


double
h3 (const double y[], void *params)
{
/**
 * Directional Hubble rate for a_3 in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * (y[7] + y[6]);
  double f2 = 1.0 / (g * l) * (y[5] + y[7]);
  double f3 = 1.0 / (g * l) * (y[6] + y[5]);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  return h3;
}


double
sig2 (const double y[], void *params)
{
/**
 * Shear scalar in Bianchi I. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * (y[7] + y[6]);
  double f2 = 1.0 / (g * l) * (y[5] + y[7]);
  double f3 = 1.0 / (g * l) * (y[6] + y[5]);
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double s2 = 1.0/3.0 * (pow(h1 - h2,2) + pow(h1 - h3,2) + pow(h3 - h2,2));
  return s2;
}


double
slPS_b1 (const double y[], void *params)
{
/**
 * Slow roll scalar power spectrum in Bianchi I. 
 */
  //   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double h = 1.0/6.0 * (1.0 / (g * l) * (y[7] + y[6])
		       + 1.0 / (g * l) * (y[5] + y[7])
		       + 1.0 / (g * l) * (y[6] + y[5]));
  double ps = 1.0/(4.0 * M_PI * M_PI) * pow(pow(h,2)/y[8],2);
  return ps;
}


double
slns_b1 (const double y[], void *params)
{
/**
 * Slow-roll ns (scalar spectra index) in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double ns = 1.0+2.0/(8.0 * M_PI * my_params_pointer->G) * 2.0 / pow(y[2],2) - 6.0 / (16.0 * M_PI * my_params_pointer->G) * 4.0 / pow(y[2],2);
  return ns;
}


void
dy_b1_gr (const double y[], void *params, const double KVfs[], const double gii[], const double hi[], double f[])
{
/**
 * Here we compute the functions dy/dt for the background in GR in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;

  double H = KVfs[0];
  double K = KVfs[1];
  double dV = KVfs[3];
  double g11 = gii[0];
  double g22 = gii[1];
  double g33 = gii[2];
  double h1 = hi[0];
  double h2 = hi[1];
  
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  f[0] = 1 / a;
  f[1] = 1.0 / (g * l) * (y[7] + y[6]);
  f[2] = 1.0 / (g * l) * (y[5] + y[7]);
  f[3] = 1.0 / (g * l) * (y[6] + y[5]);
  f[4] = y[8];
  f[5] = 1.0/2.0
    * ( + y[6] * f[2] 
	+ y[7] * f[3]
	- 16.0 * M_PI * G * g * l * K - y[5] * (f[2] + f[3]));

  f[6] = 1.0/2.0
    * ( + y[7] * f[3] 
	+ y[5] * f[1]  
	- 16.0 * M_PI * G * g * l * K - y[6] * (f[3] + f[1]));

  f[7] =  1.0/2.0 
    * ( + y[5] * f[1]  
	+ y[6] * f[2]  
	- 16.0 * M_PI * G * g * l * K - y[7] * (f[1] + f[2]));
  //double H =1.0/6.0 * (f[1] + f[2] + f[3]);
  //These are the 2n derivatives: logpi''
  f[8] = -3.0 * H * y[8] - dV;
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
  //Time-dependent Euler angles
  double gam_t = atan(sqrt(g11 / g22) * tan(gam));
  double bet_t = atan((sqrt(g33 / g11) * cos(gam) / cos(gam_t) * tan(bet)));
  f[9] = -cos(bet_t) *  tan(gam) / (sqrt(g22 / g11) + sqrt(g11 / g22) * tan(gam) * tan(gam)) * (h1  - h2);

}


void
ddy_b1_gr (const double y[], void *params, const double f[], double df[])
{
/**
 * Here we compute the functions d^2y/dt^2 for the background in GR in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;

  //These are the 2nd derivatives: logpi''
  df[0] = 0.0;//for convenience, in the future might be changed
  df[1] = 1.0 / (g * l) * (- f[5] * y[5] * (y[7] + y[6]) + (f[7] + f[6] ));
  df[2] = 1.0 / (g * l) * (- f[6] * y[6] * (y[5] + y[7]) + (f[5] + f[7] ));
  df[3] = 1.0 / (g * l) * (- f[7] * y[7] * (y[6] + y[5]) + (f[6] + f[5] ));
}


void
shear (const double y[], double const f[], double const df[], double sij[], double dsij[])
{
/**
 * Shear components and its derivatives in the basis where $h_{ij}$ is diagonal in Bianchi I. 
 */
  //Shear and its time derivative
  double s1 = 2.0 / 3.0 * (-2.0 * f[1] + f[2] + f[3]);
  double s2 = 2.0 / 3.0 * (f[1] - 2.0 * f[2] + f[3]);
  double s3 = 2.0 / 3.0 * (f[1] + f[2] - 2.0 * f[3]);
  //These are the time derivatives of si
  double ds1 = 2.0 / 3.0 * (-2.0 * df[1] + df[2] + df[3]);
  double ds2 = 2.0 / 3.0 * (df[1] - 2.0 * df[2] + df[3]);
  double ds3 = 2.0 / 3.0 * (df[1] + df[2] - 2.0 * df[3]);
  //Finally the definitions ofthe shear and its time derivative
  sij[0] = 0.5 * s1 * exp(2.0 * (y[2] + y[3] - 2.0 * y[1]) / 3.0);
  sij[1] = 0.5 * s2 * exp(2.0 * (y[3] + y[1] - 2.0 * y[2]) / 3.0);
  sij[2] = 0.5 * s3 * exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);

  dsij[0] = 0.5 * (s1 * s1 + ds1) * exp(2.0 * (y[2] + y[3] - 2.0 * y[1]) / 3.0);
  dsij[1] = 0.5 * (s2 * s2 + ds2) * exp(2.0 * (y[3] + y[1] - 2.0 * y[2]) / 3.0);
  dsij[2] = 0.5 * (s3 * s3 + ds3) * exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);
}


void
hatkt (const double y[], void *params, const double KVfs[], const double gii[], const double hi[], double Kt[])
{
/**
 * Time-dependent normalized wavenumber vector $k(t)$ and its time derivative $dot k(t)$ in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
  double g11 = gii[0];
  double g22 = gii[1];
  double g33 = gii[2];
  //double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  //double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  //double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  //double a = exp((y[1] + y[2] + y[3]) / 6.0);
  //double a1oa2 = exp(- y[1] + y[2]);
  //double a3oa1 = exp(y[1] - y[3]);
  double h1 = hi[0];
  double h2 = hi[1];
  double h3 = hi[2];
  double H = KVfs[0];

  double k1 = cosl(gam) * sinl(bet);
  double k2 = sinl(bet) * sinl(gam);
  double k3 = cosl(bet);
  double gam_t = atanl(sqrtl(g11 / g22) * tanl(gam));
  double bet_t = atanl((sqrtl(g33 / g11) * cosl(gam) / cosl(gam_t) * tanl(bet)));
  double kt = powl(k1 * k1 / g11 + k2 * k2 / g22 + k3 * k3 / g33,0.5);
  double dkt = - (k1 * k1 * (h1 - H) / g11 + k2 * k2 * (h2 - H) / g22 + k3 * k3 * (h3 - H) / g33) / kt;
  Kt[0] = gam_t;
  Kt[1] = bet_t;
  Kt[2] = kt;
  Kt[3] = dkt;
}


void
fshear (const double y[], void *params, const double KVfs[], const double gii[], const double hi[], const double Kt[], const double f[], const double sij[], const double dsij[], double fsij[], double fdsij[])
{
/**
 * Shear components and its derivatives in the basis where of the Fourier space of perturbations in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
  double g11 = gii[0];
  double g22 = gii[1];
  double g33 = gii[2];
  //double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  //double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  //double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  //double a = exp((y[1] + y[2] + y[3]) / 6.0);
  //double a1oa2 = exp(- y[1] + y[2]);
  //double a3oa1 = exp(y[1] - y[3]);
  double h1 = hi[0];
  double h2 = hi[1];
  double h3 = hi[2];
  double H = KVfs[0];

  double k1 = cos(gam) * sin(bet);
  double k2 = sin(bet) * sin(gam);
  double k3 = cos(bet);
  /*
  double gam_t = atan(sqrt(g11 / g22) * tan(gam));
  double bet_t = atan((sqrt(g33 / g11) * cos(gam) / cos(gam_t) * tan(bet)));
  double kt = pow(k1 * k1 / g11 + k2 * k2 / g22 + k3 * k3 / g33,0.5);
  double dkt = -  (k1 * k1 * (h1 - H) / g11 + k2 * k2 * (h2 - H) / g22 + k3 * k3 * (h3 - H) / g33) / kt;
  */
  double gam_t = Kt[0];
  double bet_t = Kt[1];  
  double kt = Kt[2];
  double dkt = Kt[3];
  //Fourier vector basis
  double KI[3] = {k1 / (g11 * kt), k2 / (g22 * kt), k3 / ( g33 * kt)};
  //Alternative definition for KI
  //double k1 = cos(gam_t) * sin(bet_t);
  //double k2 = sin(bet_t) * sin(gam_t);
  //double k3 = cos(bet_t);
  //double KI[3] = {cos(gam_t) * sin(bet_t) / sqrt(g11), sin(bet_t) * sin(gam_t) / sqrt(g22),  cos(bet_t) / sqrt(g33)};
  double E1I[3] = {(sqrt(1.0 / g11) * (cos(y[9]) * cos(bet_t) * cos(gam_t) - sin(y[9]) * sin(gam_t))),
		   (sqrt(1.0 / g22) * (cos(gam_t) * sin(y[9]) + cos(y[9]) * cos(bet_t) * sin(gam_t))),
		   -((sqrt(1.0 / g33) * cos(y[9]) * sin(bet_t)))};
  double E2I[3] = {-((sqrt(1.0 / g11) * (cos(bet_t) * cos(gam_t) * sin(y[9]) + cos(y[9]) * sin(gam_t)))),
		   (sqrt(1.0 / g22) * (cos(y[9]) * cos(gam_t) - cos(bet_t) * sin(y[9]) * sin(gam_t))),
		   (sqrt(1.0 / g33) * sin(y[9]) * sin(bet_t))};

  fsij[0] = KI[0] *  KI[0] * sij[0] +  KI[1] *  KI[1] * sij[1] +  KI[2] *  KI[2] * sij[2] - 1.0 / 3.0 * (sij[0] / g11 + sij[1] / g22 + sij[2] / g33);//This is spp = sij (KI KJ - 1/3 hIJ)
  fsij[1] = sqrt(2.0) * (KI[0] * E1I[0] * sij[0] + KI[1] * E1I[1] * sij[1] + KI[2] * E1I[2] * sij[2]);//This is sv1 = sij(KI E1J+E1I KJ )/sqrt(2)
  fsij[2] = sqrt(2.0) * (KI[0] * E2I[0] * sij[0] + KI[1] * E2I[1] * sij[1] + KI[2] * E2I[2] * sij[2]);//This is sv2 = sij(KI E2J+E2I KJ )/sqrt(2)
  double s11 = E1I[0] * E1I[0] * sij[0] + E1I[1] * E1I[1] * sij[1] + E1I[2] * E1I[2] * sij[2];//This is s11 = sij(E1I E1J)
  double s22 = E2I[0] * E2I[0] * sij[0] + E2I[1] * E2I[1] * sij[1] + E2I[2] * E2I[2] * sij[2];//This is s22 = sij(E2I E2J)
  double s12 = E1I[0] * E2I[0] * sij[0] + E1I[1] * E2I[1] * sij[1] + E1I[2] * E2I[2] * sij[2];//This is s12 = sij(E1I E2J) = sij(E2I E1J)
  fsij[3] = (s11 - s22) / sqrt(2.0);//This is stp = sij(E1I E1J-E2I E2J )/sqrt(2)
  fsij[4] = sqrt(2.0) * s12;//This is stc = sij(EII E2J+E2I E1J )/sqrt(2)

  //Time derivative of the components of the shear. We need the time derivative of the vectors
  //Time derivative of the Fourier basis elements
  double dKI[3] = {-(2.0 * (h1 - H) + dkt / kt) * KI[0],
		   -(2.0 * (h2 - H) + dkt / kt) * KI[1],
		   -(2.0 * (h3 - H) + dkt / kt) * KI[2]};
  double dE1I[3] = {(- s11) * E1I[0] - s12 * E2I[0],
		    (- s11) * E1I[1] - s12 * E2I[1],
		    (- s11) * E1I[2] - s12 * E2I[2]};
  double dE2I[3] = {(- s22) * E2I[0] - s12 * E1I[0],
		    (- s22) * E2I[1] - s12 * E1I[1],
		    (- s22) * E2I[2] - s12 * E1I[2]};
  
  fdsij[0] = 2.0 * (KI[0] * dKI[0] * sij[0] +  KI[1] * dKI[1] * sij[1] + dKI[2] *  KI[2] * sij[2]) + KI[0] *  KI[0] * dsij[0] +  KI[1] *  KI[1] * dsij[1] +  KI[2] *  KI[2] * dsij[2] - 1.0 / 3.0 * (dsij[0] / g11 + dsij[1] / g22 + dsij[2] / g33) + 2.0 / 3.0 * (sij[0] * (h1 - H) / g11 + sij[1] * (h2 - H) / g22 + sij[2] * (h3 - H) / g33);
  fdsij[1] = sqrt(2.0) * (dKI[0] * E1I[0] * sij[0] + dKI[1] * E1I[1] * sij[1] + dKI[2] * E1I[2] * sij[2] +
			     KI[0] * dE1I[0] * sij[0] + KI[1] * dE1I[1] * sij[1] + KI[2] * dE1I[2] * sij[2] +
			     KI[0] * E1I[0] * dsij[0] + KI[1] * E1I[1] * dsij[1] + KI[2] * E1I[2] * dsij[2]);
  fdsij[2] = sqrt(2.0) * (dKI[0] * E2I[0] * sij[0] + dKI[1] * E2I[1] * sij[1] + dKI[2] * E2I[2] * sij[2] +
			     KI[0] * dE2I[0] * sij[0] + KI[1] * dE2I[1] * sij[1] + KI[2] * dE2I[2] * sij[2] +
			     KI[0] * E2I[0] * dsij[0] + KI[1] * E2I[1] * dsij[1] + KI[2] * E2I[2] * dsij[2]);
  fdsij[3] = (E1I[0] * E1I[0] * dsij[0] + E1I[1] * E1I[1] * dsij[1] + E1I[2] * E1I[2] * dsij[2] - (E2I[0] * E2I[0] * dsij[0] + E2I[1] * E2I[1] * dsij[1] + E2I[2] * E2I[2] * dsij[2])) / sqrt(2.0) +  (E1I[0] * dE1I[0] * sij[0] + E1I[1] * dE1I[1] * sij[1] + E1I[2] * dE1I[2] * sij[2] - (E2I[0] * dE2I[0] * sij[0] + E2I[1] * dE2I[1] * sij[1] + E2I[2] * dE2I[2] * sij[2])) * sqrt(2.0);
  fdsij[4] = sqrt(2.0) * (E1I[0] * E2I[0] * dsij[0] + E1I[1] * E2I[1] * dsij[1] + E1I[2] * E2I[2] * dsij[2] + dE1I[0] * E2I[0] * sij[0] + dE1I[1] * E2I[1] * sij[1] + dE1I[2] * E2I[2] * sij[2] + E1I[0] * dE2I[0] * sij[0] + E1I[1] * dE2I[1] * sij[1] + E1I[2] * dE2I[2] * sij[2]);


}


void
couplns (const double y[], void *params, double const KVfs[], double const f[], double const df[], double const fsij[], double const fdsij[], double couplns[])
{
/**
 * Time dependent functions in the EOMs of perturbations. These are the MS variables potentials plus $a''/a$ and divided by a^2. These expressions have been obtained from Eqs. 4.9, 4.10, 4.11 of arxiv:0707.0736. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;

  double H = KVfs[0];
  double K = KVfs[1];
  double V = KVfs[2];
  double dV = KVfs[3];
  double ddV = KVfs[4];
  //double dH = KVfs[5];

  //double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  //double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  //double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  //double H =1.0/6.0 * (f[1] + f[2] + f[3]);
  double dH =1.0/6.0 * (df[1] + df[2] + df[3]);
  
  double rho = rho_KV(K,V);
  double drho = -6.0 * H * K;
  double spp = fsij[0];
  double sv1 = fsij[1];
  double sv2 = fsij[2];
  double stp = fsij[3];
  double stc = fsij[4];
  double dspp = fdsij[0];
  double dsv1 = fdsij[1];
  double dsv2 = fdsij[2];
  double dstp = fdsij[3];
  double dstc = fdsij[4];

  //These factors have been introduced in this way in order to improve the efficienciy
  double kappa = 8.0 * M_PI * G;
  double factor = (4.0 * kappa * rho + 2.0 * (sv1 * sv1 + sv2 * sv2 + stp * stp + stc * stc)) / 3.0;
  double Hpmspp = (2.0 * H + spp) / factor;
  double dHpmspp = (- Hpmspp * ((4.0 * kappa * drho + 4.0 * (sv1 * dsv1 + sv2 * dsv2 + stp * dstp + stc * dstc)) / 3.0) + (2.0 * dH + dspp)) / factor;
  
  //diagonal terms
  double svv = ddV - 2.0 * kappa * (2.0 * K * dHpmspp + Hpmspp * (- 6.0 * H * K - 2.0 * dV * y[8]));
  double smupp = - 2.0 * stc * stc - (3.0 * H * spp + dspp) - 2.0 * stp * stp * dHpmspp - 2.0 * (3.0 * H * stp * stp + 2.0 * stp * dstp) * Hpmspp;
  double smucc = - 2.0 * stp * stp - (3.0 * H * spp + dspp) - 2.0 * stc * stc * dHpmspp - 2.0 * (3.0 * H * stc * stc + 2.0 * stc * dstc) * Hpmspp;
  //coupling terms
  double svmuc = - 2.0 * sqrt(kappa) * (y[8] * stc * dHpmspp + (3.0 * H * y[8] * stc + f[8] * stc + y[8] * dstc) * Hpmspp);
  double svmup = - 2.0 * sqrt(kappa) * (y[8] * stp * dHpmspp + (3.0 * H * y[8] * stp + f[8] * stp + y[8] * dstp) * Hpmspp);
  double smucmup = 2.0 * stc * stp - 2.0 * (stp * stc * dHpmspp + (3.0 * H * stp * stc + dstp * stc + stp * dstc) * Hpmspp);
    //diagonal terms
  couplns[0] = svv;
  couplns[1] = smupp;
  couplns[2] = smucc;
  //coupling terms
  couplns[3] = svmuc;
  couplns[4] = svmup;
  couplns[5] = smucmup;
  
}


double
bet_t (const double y[], void *params)
{
/**
 * Angle $beta(t)$ in the Fourier decomposition of perturbations in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  double bet0 = my_params_pointer->bet;//Final Euler angles
  double gam0 = my_params_pointer->gam;//Final Euler angles
  double gam = atan(a1 / a2 * tan(gam0));  
  double bet = atan((a3 / a1 * cos(gam0) / cos(gam) * tan(bet0)));
  return bet;
}


double
gamm_t (const double y[], void *params)
{
/**
 * Angle $gamma(t)$ in the Fourier decomposition of perturbations in Bianchi I. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  double gam0 = my_params_pointer->gam;//Final Euler angles
  double gam = atan(a1 / a2 * tan(gam0));  
  return gam;
}



/****************  Some observables for Bianchi I in LQC in AW  *************************/


double Vpf_b1_lqc (const double y[], void *params)
{
/**
 * Potential mimiking perfect fluid in Bianchi I in LQC. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double V0 = my_params_pointer->mass;
  double w = 0.0 / 3.0;
  double rho_c = 3.0/(8.0 * M_PI * pow(g * l,2));
  double kappa = 3.0 * (8.0 * M_PI * G);
  double V = V0 * (exp(sqrt(kappa * (1.0 + w)) * y[4]) / pow(1.0 + V0 * exp(sqrt(kappa * (1.0 + w)) * y[4]) / (2 * rho_c * (1.0 - w)),2.0));
  return V;
}


double dVpf_b1_lqc (const double y[], void *params)
{
/**
 * 1st derivative of the potential mimiking perfect fluid  in Bianchi I in LQC. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double V0 = my_params_pointer->mass;
  double w = 0.0 / 3.0;
  double rho_c = 3.0/(8.0 * M_PI * pow(g * l,2));
  double kappa = 3.0 * (8.0 * M_PI * G);
  double dV = 4.0*V0*pow(kappa,0.5)*exp(y[4]*pow(kappa,0.5)*pow(1.0 + w,0.5))*(2.0*rho_c*(-1.0 + w) + V0*exp(y[4]*pow(kappa,0.5)*pow(1.0 + w,0.5)))*pow(rho_c,2.0)*pow(-1.0 + w,2.0)*pow(1.0 + w,0.5)* pow(2.0*rho_c*(-1.0 + w) - V0*exp(y[4]*pow(kappa,0.5)*pow(1.0 + w,0.5)),-3.0);
  //double dV = V0 * sqrt(3.0 * (1.0 + w)) * exp(sqrt(3.0 * (1.0 + w)) * y[2]);
  return dV;
}


double ddVpf_b1_lqc (const double y[], void *params)
{
/**
 * 2nd derivative of the potential mimiking perfect fluid  in Bianchi I in LQC. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  double V0 = my_params_pointer->mass;
  double w = 0.0 / 3.0;
  double rho_c = 3.0/(8.0 * M_PI * pow(g * l,2));
  double kappa = 3.0 * (8.0 * M_PI * G);
  double ddV = 4.0 * kappa * V0 * (1.0 + w) * exp(y[4] * pow(kappa * (1.0 + w),0.5)) * pow(rho_c,2.0) * pow(-1.0 + w,2.0) * (8.0 * rho_c * V0 * (-1.0 + w) * exp(y[4] * pow(kappa * (1.0 + w),0.5)) + exp(2.0 * y[4] * pow(kappa * (1.0 + w),0.5)) * pow(V0,2.0) + 4.0 * pow(rho_c,2.0) * pow(-1.0 + w,2.0)) * pow(-2.0 * rho_c * (-1.0 + w) + V0 * exp(y[4] * pow(kappa * (1.0 + w),0.5)),-4.0);
  return ddV;
}


double
hubble_b1_lqc (const double y[], void *params)
{
/**
 * Average Hubble parameter in Bianchi I in LQC. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double h = 1.0/6.0 * (1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]))
		       + 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]))
		       + 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5])));
  return h;
}

double
dhubble_b1_lqc (const double y[], void *params)
{
/**
 * Time derivative of the average Hubble parameter in Bianchi I in LQC. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  //These are the 1st derivatives: logpi'
  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  //These are the 1st derivatives: bi'
  double f5 = 1.0/2.0
    * ( + y[6] * f2
	+ y[7] * f3
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[5] * (f2 + f3));

  double f6 = 1.0/2.0
    * ( + y[7] * f3 
	+ y[5] * f1  
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[6] * (f3 + f1));

  double f7 =  1.0/2.0 
    * ( + y[5] * f1  
	+ y[6] * f2  
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[7] * (f1 + f2));

  //These are the 2nd derivatives: logpi''
  double df1 = 1.0 / (g * l) * (- f5 * sin(y[5]) * (sin(y[7]) + sin(y[6])) + cos(y[5]) * (f7 * cos(y[7]) + f6 * cos(y[6])));
  double df2 = 1.0 / (g * l) * (- f6 * sin(y[6]) * (sin(y[5]) + sin(y[7])) + cos(y[6]) * (f5 * cos(y[5]) + f7 * cos(y[7])));
  double df3 = 1.0 / (g * l) * (- f7 * sin(y[7]) * (sin(y[6]) + sin(y[5])) + cos(y[7]) * (f6 * cos(y[6]) + f5 * cos(y[5])));
  double dh = 1.0/6.0 * (df1 + df2 + df3);
  return dh;
}


double
c_gr_b1_lqc (const double y[], void *params)
{
/**
 * Densistized gravitational constraint in Bianchi I in LQC. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double c = -(1.0/(8.0 * M_PI * my_params_pointer->G * pow(l * g,2))) * (sin(y[5]) * sin(y[6]) + sin(y[7]) * sin(y[6])  + sin(y[5]) * sin(y[7]));
  return c;
}


double
constraint_b1_lqc (const double y[], void *params)
{
/**
 * Densistized full constraint in Bianchi I in LQC. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double c = fabs(-(1.0/(8.0 * M_PI * my_params_pointer->G * pow(l * g,2))) * (sin(y[5]) * sin(y[6]) + sin(y[7]) * sin(y[6])  + sin(y[5]) * sin(y[7])) + (pow(y[8],2) + pow(my_params_pointer->mass * y[4],2)) / 2.0);
  return c;
}


double
ddaoa_b1_lqc (const double y[], void *params)
{
/**
 * Function $a''/a$ in Bianchi I in LQC. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  //These are the 1st derivatives: logpi'
  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  //These are the 1st derivatives: bi'
  double f5 = 1.0/2.0
    * ( + y[6] * f2
	+ y[7] * f3
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[5] * (f2 + f3));

  double f6 = 1.0/2.0
    * ( + y[7] * f3 
	+ y[5] * f1  
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[6] * (f3 + f1));

  double f7 =  1.0/2.0 
    * ( + y[5] * f1  
	+ y[6] * f2  
	- 8.0 * M_PI * G * g * l * pow(y[8],2) - y[7] * (f1 + f2));

  //These are the 2nd derivatives: logpi''
  double df1 = 1.0 / (g * l) * (- f5 * y[5] * (y[7] + y[6]) + (f7 + f6 ));
  double df2 = 1.0 / (g * l) * (- f6 * y[6] * (y[5] + y[7]) + (f5 + f7 ));
  double df3 = 1.0 / (g * l) * (- f7 * y[7] * (y[6] + y[5]) + (f6 + f5 ));
  double DDaoa = 1.0 / 6.0 * (df1 + df2 + df3) + 1.0 / 36.0 * (f1 + f2 + f3 ) * (f1 + f2 + f3 );
  return DDaoa;
}


double
h1_lqc (const double y[], void *params)
{
/**
 * Directional hubble rate for $a_1$ in Bianchi I in LQC. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  return h1;
}


double
h2_lqc (const double y[], void *params)
{
/**
 * Directional hubble rate for $a_2$ in Bianchi I in LQC. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  return h2;
}


double
h3_lqc (const double y[], void *params)
{
/**
 * Directional hubble rate for $a_3$ in Bianchi I in LQC. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  return h3;
}


double
sig2_lqc (const double y[], void *params)
{
/**
 * Shear scalar in Bianchi I in LQC. 
 */
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  double f2 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  double f3 = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double s2 = 1.0/3.0 * (pow(h1 - h2,2) + pow(h1 - h3,2) + pow(h3 - h2,2));
  return s2;
}


double
slPS_b1_lqc (const double y[], void *params)
{
/**
 * Slow roll scalar power spectrum in Bianchi I in LQC. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double h = 1.0/6.0 * (1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]))
		       + 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]))
		       + 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5])));
  double ps = 1.0/(4.0 * M_PI * M_PI) * pow(pow(h,2)/y[8],2);
  return ps;
}


void
dy_b1_lqc (const double y[], void *params, const double KVfs[], const double gii[], const double hi[], double f[])
{
/**
 * Here we compute the functions dy/dt for the background in Bianchi I in LQC. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;

  double H = KVfs[0];
  double K = KVfs[1];
  double dV = KVfs[3];
  double g11 = gii[0];
  double g22 = gii[1];
  double g33 = gii[2];
  double h1 = hi[0];
  double h2 = hi[1];
  
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  f[0] = 1.0 / a;
  f[1] = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  f[2] = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  f[3] = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  f[4] = y[8];
  f[5] = 1.0/2.0
    * ( + y[6] * f[2] 
	+ y[7] * f[3]
	- 16.0 * M_PI * G * g * l * K - y[5] * (f[2] + f[3]));

  f[6] = 1.0/2.0
    * ( + y[7] * f[3] 
	+ y[5] * f[1]  
	- 16.0 * M_PI * G * g * l * K - y[6] * (f[3] + f[1]));

  f[7] =  1.0/2.0 
    * ( + y[5] * f[1]  
	+ y[6] * f[2]  
	- 16.0 * M_PI * G * g * l * K - y[7] * (f[1] + f[2]));
  //double H =1.0/6.0 * (f[1] + f[2] + f[3]);
  //These are the 2n derivatives: logpi''
  f[8] = -3.0 * H * y[8] - dV;
  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
  //Time-dependent Euler angles
  double gam_t = atanl(sqrtl(g11 / g22) * tanl(gam));
  double bet_t = atanl((sqrtl(g33 / g11) * cosl(gam) / cosl(gam_t) * tanl(bet)));
  f[9] = -cosl(bet_t) *  tanl(gam) / (sqrtl(g22 / g11) + sqrtl(g11 / g22) * tanl(gam) * tanl(gam)) * (h1  - h2);

}


void
ddy_b1_lqc (const double y[], void *params, const double f[], double df[])
{
/**
 * Here we compute the functions d^2y/dt^2 for the background in Bianchi I in LQC. 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;

  //These are the 2nd derivatives: logpi''
  df[0] = 0.0;//for convenience, in the future might be changed
  df[1] = 1.0 / (g * l) * (- f[5] * sin(y[5]) * (sin(y[7]) + sin(y[6])) + cos(y[5]) * (f[7] * cos(y[7]) + f[6] * cos(y[6])));
  df[2] = 1.0 / (g * l) * (- f[6] * sin(y[6]) * (sin(y[5]) + sin(y[7])) + cos(y[6]) * (f[5] * cos(y[5]) + f[7] * cos(y[7])));
  df[3] = 1.0 / (g * l) * (- f[7] * sin(y[7]) * (sin(y[6]) + sin(y[5])) + cos(y[7]) * (f[6] * cos(y[6]) + f[5] * cos(y[5])));
}


void
couplns_lqc (const double y[], void *params, double const KVfs[], double const f[], double const df[], double const fsij[], double const fdsij[], double couplns[])
{
/**
 * Time dependent functions in the EOMs of perturbations. These are the MS variables potentials plus $a''/a$ and divided by $a^2$. These expressions have been obtained from Eqs. 4.9, 4.10, 4.11 of arxiv:0707.0736 but replacing Eqs 3.11, 3.12, 3.13, the Raychaudhuri (Eq. 1.18) and scalar field EOM (Eq. 1.19). 
 */
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;
  double kappa = 8.0 * M_PI * G;

  double H = KVfs[0];
  double K = KVfs[1];
  double V = KVfs[2];
  double dV = KVfs[3];
  double ddV = KVfs[4];
  //double dH = KVfs[5];
  double rho = rho_KV(K,V);
  double drho = -6.0 * H * K;

  double spp = fsij[0];
  double sv1 = fsij[1];
  double sv2 = fsij[2];
  double stp = fsij[3];
  double stc = fsij[4];
  //options for fdsij;
  //double dspp = fdsij[0];
  //double dsv1 = fdsij[1];
  //double dsv2 = fdsij[2];
  //double dstp = fdsij[3];
  //double dstc = fdsij[4];
  //Here we replace Eqs 3.11, 3.12, 3.13 of arxiv:0707.0736, with the difference that sv1 and sv2 have a different normalization (svi_pereira=svi/sqrt(2))
  double dspp = - 3.0 * H * spp - sv1 * sv1 - sv2 * sv2;
  double dsv1 = - 3.0 * H * sv1 + 3.0 / 2.0 * spp * sv1 - (stp * sv1 + stc * sv2) / sqrt(2.0);
  double dsv2 = - 3.0 * H * sv2 + 3.0 / 2.0 * spp * sv2 + (stp * sv2 - stc * sv1) / sqrt(2.0);
  double dstp = - 3.0 * H * stp + (sv1 * sv1 - sv2 * sv2) / sqrt(2.0);
  double dstc = - 3.0 * H * stc + sqrt(2.0) * sv1 * sv2;

  //double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  //double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  //double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  //double H =1.0/6.0 * (f[1] + f[2] + f[3]);
  //Options for dH:
  //double dH =1.0/6.0 * (df[1] + df[2] + df[3]);
  //Here we replace Raychaudhuri equation (Eq. 1.18 in arxiv:0707.0736). 
  double dH = kappa * V - 3.0 * H * H;
  //double dH = kappa * (V - rho) - 0.5 * s2;

  //These factors have been introduced in this way in order to improve the efficienciy
  double factor = (4.0 * kappa * rho + 2.0 * (sv1 * sv1 + sv2 * sv2 + stp * stp + stc * stc)) / 3.0;
  double Hpmspp = (2.0 * H + spp) / factor;
  double dHpmspp = (- Hpmspp * ((4.0 * kappa * drho + 4.0 * (sv1 * dsv1 + sv2 * dsv2 + stp * dstp + stc * dstc)) / 3.0) + (2.0 * dH + dspp)) / factor;
  
  //diagonal terms
  double svv = ddV - 2.0 * kappa * (2.0 * K * dHpmspp + Hpmspp * (- 6.0 * H * K - 2.0 * dV * y[8]));
  double smupp = - 2.0 * stc * stc - (3.0 * H * spp + dspp) - 2.0 * stp * stp * dHpmspp - 2.0 * (3.0 * H * stp * stp + 2.0 * stp * dstp) * Hpmspp;
  double smucc = - 2.0 * stp * stp - (3.0 * H * spp + dspp) - 2.0 * stc * stc * dHpmspp - 2.0 * (3.0 * H * stc * stc + 2.0 * stc * dstc) * Hpmspp;
  //coupling terms
  double svmuc = - 2.0 * sqrt(kappa) * (y[8] * stc * dHpmspp + (3.0 * H * y[8] * stc + f[8] * stc + y[8] * dstc) * Hpmspp);
  double svmup = - 2.0 * sqrt(kappa) * (y[8] * stp * dHpmspp + (3.0 * H * y[8] * stp + f[8] * stp + y[8] * dstp) * Hpmspp);
  double smucmup = 2.0 * stc * stp - 2.0 * (stp * stc * dHpmspp + (3.0 * H * stp * stc + dstp * stc + stp * dstc) * Hpmspp);
    //diagonal terms
  couplns[0] = svv;
  couplns[1] = smupp;
  couplns[2] = smucc;
  //coupling terms
  couplns[3] = svmuc;
  couplns[4] = svmup;
  couplns[5] = smucmup;
  
}

/****************  Some observables for Bianchi I in LQC in AW in vacuum  *************************/


//Average Hubble parameter
double
hubble_b1_lqc_vac (const double y[], void *params)
{
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double h = 1.0/6.0 * (1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]))
		       + 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]))
		       + 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4])));
  return h;
}

//Densistized constraint
double
constraint_b1_lqc_vac (const double y[], void *params)
{
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;;
  double c = fabs(-(1.0/(8.0 * M_PI * my_params_pointer->G * pow(l * g,2))) * (sin(y[4]) * sin(y[5]) + sin(y[6]) * sin(y[5])  + sin(y[4]) * sin(y[6])));
  return c;
}


double
ddaoa_b1_lqc_vac (const double y[], void *params)
{
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  //These are the 1st derivatives: logpi'
  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  //These are the 1st derivatives: bi'
  double f5 = 1.0/2.0
    * ( + y[5] * f2 + y[6] * f3 - y[4] * (f2 + f3));

  double f6 = 1.0/2.0
    * ( + y[6] * f3 + y[4] * f1 - y[5] * (f3 + f1));

  double f7 =  1.0/2.0 
    * ( + y[4] * f1 + y[5] * f2 - y[6] * (f1 + f2));

  //These are the 2nd derivatives: logpi''
  double df1 = 1.0 / (g * l) * (- f5 * y[4] * (y[6] + y[5]) + (f7 + f6 ));
  double df2 = 1.0 / (g * l) * (- f6 * y[5] * (y[4] + y[6]) + (f5 + f7 ));
  double df3 = 1.0 / (g * l) * (- f7 * y[6] * (y[5] + y[4]) + (f6 + f5 ));
  double DDaoa = 1.0 / 6.0 * (df1 + df2 + df3) + 1.0 / 36.0 * (f1 + f2 + f3 ) * (f1 + f2 + f3 );
  return DDaoa;
}

//Directional hubble rate for a_1
double
h1_lqc_vac (const double y[], void *params)
{
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  return h1;
}

//Directional hubble rate for a_a
double
h2_lqc_vac (const double y[], void *params)
{
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  return h2;
}

//Directional hubble rate for a_3
double
h3_lqc_vac (const double y[], void *params)
{
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  return h3;
}

//Scalar shear
double
sig2_lqc_vac (const double y[], void *params)
{
  struct param_const_b1 *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double s2 = 1.0/3.0 * (pow(h1 - h2,2) + pow(h1 - h3,2) + pow(h3 - h2,2));
  return s2;
}


// Shear and its derivatives in the basis where hij is diagonal
void
shear_lqc_vac (const double y[], void *params, double sij[], double dsij[])
{
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;
  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double f5 = 1.0/2.0
    * ( + y[5] * f2 + y[6] * f3 - y[4] * (f2 + f3));

  double f6 = 1.0/2.0
    * ( + y[6] * f3 + y[4] * f1 - y[5] * (f3 + f1));

  double f7 =  1.0/2.0 
    * ( + y[4] * f1 + y[5] * f2 - y[6] * (f1 + f2));
   //Shear and its time derivative
  double s1 = 2.0 / 3.0 * (-2.0 * f1 + f2 + f3);
  double s2 = 2.0 / 3.0 * (f1 - 2.0 * f2 + f3);
  double s3 = 2.0 / 3.0 * (f1 + f2 - 2.0 * f3);
  //These are the 2n derivatives: logpi''
  double df1 = 1.0 / (g * l) * (- f5 * y[4] * (y[6] + y[5]) + (f7 + f6 ));
  double df2 = 1.0 / (g * l) * (- f6 * y[5] * (y[4] + y[6]) + (f5 + f7 ));
  double df3 = 1.0 / (g * l) * (- f7 * y[6] * (y[5] + y[4]) + (f6 + f5 ));
  //This is the time derivative of H
  //These are the time derivatives of si
  double ds1 = 2.0 / 3.0 * (-2.0 * df1 + df2 + df3);
  double ds2 = 2.0 / 3.0 * (df1 - 2.0 * df2 + df3);
  double ds3 = 2.0 / 3.0 * (df1 + df2 - 2.0 * df3);
  //Finally the definitions ofthe shear and its time derivative
  sij[0] = 0.5 * s1 * exp(2.0 * (y[2] + y[3] - 2.0 * y[1]) / 3.0);
  sij[1] = 0.5 * s2 * exp(2.0 * (y[3] + y[1] - 2.0 * y[2]) / 3.0);
  sij[2] = 0.5 * s3 * exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);

  dsij[0] = 0.5 * (s1 * s1 + ds1) * exp(2.0 * (y[2] + y[3] - 2.0 * y[1]) / 3.0);
  dsij[1] = 0.5 * (s2 * s2 + ds2) * exp(2.0 * (y[3] + y[1] - 2.0 * y[2]) / 3.0);
  dsij[2] = 0.5 * (s3 * s3 + ds3) * exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);
}

// Shear and its derivatives in the basis where of the Fourier space of perturbations
void
fshear_lqc_vac (const double y[], void *params, const double sij[], const double dsij[], double fsij[], double fdsij[])
{
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
  double g11 = exp(2.0 * (-2.0 * y[1] + y[2] + y[3]) / 3.0);
  double g22 = exp(2.0 * (y[1] - 2.0 * y[2] + y[3]) / 3.0);
  double g33 = exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);
  //double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  //double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  //double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  //double a = exp((y[1] + y[2] + y[3]) / 6.0);
  //double a1oa2 = exp(- y[1] + y[2]);
  //double a3oa1 = exp(y[1] - y[3]);
  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H =1.0/6.0 * (f1 + f2 + f3);

  double k1 = cosl(gam) * sinl(bet);
  double k2 = sinl(bet) * sinl(gam);
  double k3 = cosl(bet);
  double gam_t = atanl(sqrtl(g11 / g22) * tanl(gam));
  double bet_t = atanl((sqrt(g33 / g11) * cosl(gam) / cosl(gam_t) * tanl(bet)));

  
  double kt = powl(k1 * k1 / g11 + k2 * k2 / g22 + k3 * k3 / g33,0.5);
  double dkt = -  (k1 * k1 * (h1 - H) / g11 + k2 * k2 * (h2 - H) / g22 + k3 * k3 * (h3 - H) / g33) / kt;
  //Fourier vector basis
  double KI[3] = {k1 / (g11 * kt), k2 / (g22 * kt), k3 / ( g33 * kt)};
  //Alternative definition for KI
  //double k1 = cos(gam_t) * sin(bet_t);
  //double k2 = sin(bet_t) * sin(gam_t);
  //double k3 = cos(bet_t);
  //double KI[3] = {cos(gam_t) * sin(bet_t) / sqrt(g11), sin(bet_t) * sin(gam_t) / sqrt(g22),  cos(bet_t) / sqrt(g33)};
  double E1I[3] = {(sqrt(1.0 / g11) * (cos(y[7]) * cos(bet_t) * cos(gam_t) - sin(y[7]) * sin(gam_t))),
		   (sqrt(1.0 / g22) * (cos(gam_t) * sin(y[7]) + cos(y[7]) * cos(bet_t) * sin(gam_t))),
		   -((sqrt(1.0 / g33) * cos(y[7]) * sin(bet_t)))};
  double E2I[3] = {-((sqrt(1.0 / g11) * (cos(bet_t) * cos(gam_t) * sin(y[7]) + cos(y[7]) * sin(gam_t)))),
		   (sqrt(1.0 / g22) * (cos(y[7]) * cos(gam_t) - cos(bet_t) * sin(y[7]) * sin(gam_t))),
		   (sqrt(1.0 / g33) * sin(y[7]) * sin(bet_t))};

  fsij[0] = KI[0] *  KI[0] * sij[0] +  KI[1] *  KI[1] * sij[1] +  KI[2] *  KI[2] * sij[2] - 1.0 / 3.0 * (sij[0] / g11 + sij[1] / g22 + sij[2] / g33);//This is spp = sij (KI KJ - 1/3 hIJ)
  fsij[1] = sqrt(2.0) * (KI[0] * E1I[0] * sij[0] + KI[1] * E1I[1] * sij[1] + KI[2] * E1I[2] * sij[2]);//This is sv1 = sij(KI E1J+E1I KJ )/sqrt(2)
  fsij[2] = sqrt(2.0) * (KI[0] * E2I[0] * sij[0] + KI[1] * E2I[1] * sij[1] + KI[2] * E2I[2] * sij[2]);//This is sv2 = sij(KI E2J+E2I KJ )/sqrt(2)
  double s11 = E1I[0] * E1I[0] * sij[0] + E1I[1] * E1I[1] * sij[1] + E1I[2] * E1I[2] * sij[2];//This is s11 = sij(E1I E1J)
  double s22 = E2I[0] * E2I[0] * sij[0] + E2I[1] * E2I[1] * sij[1] + E2I[2] * E2I[2] * sij[2];//This is s22 = sij(E2I E2J)
  double s12 = E1I[0] * E2I[0] * sij[0] + E1I[1] * E2I[1] * sij[1] + E1I[2] * E2I[2] * sij[2];//This is s12 = sij(E1I E2J) = sij(E2I E1J)
  fsij[3] = (s11 - s22) / sqrt(2.0);//This is stp = sij(E1I E1J-E2I E2J )/sqrt(2)
  fsij[4] = sqrt(2.0) * s12;//This is stc = sij(EII E2J+E2I E1J )/sqrt(2)

  //Time derivative of the components of the shear. We need the time derivative of the vectors
  //Time derivative of the Fourier basis elements
  double dKI[3] = {-(2.0 * (h1 - H) + dkt / kt) * KI[0],
		   -(2.0 * (h2 - H) + dkt / kt) * KI[1],
		   -(2.0 * (h3 - H) + dkt / kt) * KI[2]};
  double dE1I[3] = {(- s11) * E1I[0] - s12 * E2I[0],
		    (- s11) * E1I[1] - s12 * E2I[1],
		    (- s11) * E1I[2] - s12 * E2I[2]};
  double dE2I[3] = {(- s22) * E2I[0] - s12 * E1I[0],
		    (- s22) * E2I[1] - s12 * E1I[1],
		    (- s22) * E2I[2] - s12 * E1I[2]};
  
  fdsij[0] = 2.0 * (KI[0] * dKI[0] * sij[0] +  KI[1] * dKI[1] * sij[1] + dKI[2] *  KI[2] * sij[2]) + KI[0] *  KI[0] * dsij[0] +  KI[1] *  KI[1] * dsij[1] +  KI[2] *  KI[2] * dsij[2] - 1.0 / 3.0 * (dsij[0] / g11 + dsij[1] / g22 + dsij[2] / g33) + 2.0 / 3.0 * (sij[0] * (h1 - H) / g11 + sij[1] * (h2 - H) / g22 + sij[2] * (h3 - H) / g33);
  fdsij[1] = sqrt(2.0) * (dKI[0] * E1I[0] * sij[0] + dKI[1] * E1I[1] * sij[1] + dKI[2] * E1I[2] * sij[2] +
			     KI[0] * dE1I[0] * sij[0] + KI[1] * dE1I[1] * sij[1] + KI[2] * dE1I[2] * sij[2] +
			     KI[0] * E1I[0] * dsij[0] + KI[1] * E1I[1] * dsij[1] + KI[2] * E1I[2] * dsij[2]);
  fdsij[2] = sqrt(2.0) * (dKI[0] * E2I[0] * sij[0] + dKI[1] * E2I[1] * sij[1] + dKI[2] * E2I[2] * sij[2] +
			     KI[0] * dE2I[0] * sij[0] + KI[1] * dE2I[1] * sij[1] + KI[2] * dE2I[2] * sij[2] +
			     KI[0] * E2I[0] * dsij[0] + KI[1] * E2I[1] * dsij[1] + KI[2] * E2I[2] * dsij[2]);
  fdsij[3] = (E1I[0] * E1I[0] * dsij[0] + E1I[1] * E1I[1] * dsij[1] + E1I[2] * E1I[2] * dsij[2] - (E2I[0] * E2I[0] * dsij[0] + E2I[1] * E2I[1] * dsij[1] + E2I[2] * E2I[2] * dsij[2])) / sqrt(2.0) +  (E1I[0] * dE1I[0] * sij[0] + E1I[1] * dE1I[1] * sij[1] + E1I[2] * dE1I[2] * sij[2] - (E2I[0] * dE2I[0] * sij[0] + E2I[1] * dE2I[1] * sij[1] + E2I[2] * dE2I[2] * sij[2])) * sqrt(2.0);
  fdsij[4] = sqrt(2.0) * (E1I[0] * E2I[0] * dsij[0] + E1I[1] * E2I[1] * dsij[1] + E1I[2] * E2I[2] * dsij[2] + dE1I[0] * E2I[0] * sij[0] + dE1I[1] * E2I[1] * sij[1] + dE1I[2] * E2I[2] * sij[2] + E1I[0] * dE2I[0] * sij[0] + E1I[1] * dE2I[1] * sij[1] + E1I[2] * dE2I[2] * sij[2]);


}

// Time dependent functions in the eoms of perturbations. These are the MS variables potentials plus a''/a and divide by a^2
void
couplns_lqc_vac (const double y[], void *params, double const fsij[], double const fdsij[], double couplns[])
{
//   (void)(t);
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;
  double mass = my_params_pointer->mass;
  double f1 = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  double f2 = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  double f3 = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));
  double f5 = 1.0/2.0
    * ( + y[5] * f2 + y[6] * f3 - y[4] * (f2 + f3));

  double f6 = 1.0/2.0
    * ( + y[6] * f3 + y[4] * f1 - y[5] * (f3 + f1));

  double f7 =  1.0/2.0 
    * ( + y[4] * f1 + y[5] * f2 - y[6] * (f1 + f2));

  //These are the 2n derivatives: logpi''
  double df1 = 1.0 / (g * l) * (- f5 * y[4] * (y[6] + y[5]) + (f7 + f6 ));
  double df2 = 1.0 / (g * l) * (- f6 * y[5] * (y[4] + y[6]) + (f5 + f7 ));
  double df3 = 1.0 / (g * l) * (- f7 * y[6] * (y[5] + y[4]) + (f6 + f5 ));
  //double h1 = 1.0 / 2.0 * (-f1 + f2 + f3);
  //double h2 = 1.0 / 2.0 * (f1 - f2 + f3);
  //double h3 = 1.0 / 2.0 * (f1 + f2 - f3);
  double H =1.0/6.0 * (f1 + f2 + f3);
  double dH =1.0/6.0 * (df1 + df2 + df3);
  double spp = fsij[0];
  double sv1 = fsij[1];
  double sv2 = fsij[2];
  double stp = fsij[3];
  double stc = fsij[4];
  double dspp = fdsij[0];
  double dsv1 = fdsij[1];
  double dsv2 = fdsij[2];
  double dstp = fdsij[3];
  double dstc = fdsij[4];

  double Hpmspp = (2.0 * H + spp) / ((2.0 * sv1 * sv1 + 2.0 * sv2 * sv2 + stp * stp + stc * stc) / 3.0);
  double dHpmspp = (- Hpmspp * (2.0 / 3.0 * (2.0 * sv1 * dsv1 + 2.0 * sv2 * dsv2 + stp * dstp + stc * dstc)) + (2.0 * dH + dspp)) /
    ((2.0 * sv1 * sv1 + 2.0 * sv2 * sv2 + stp * stp + stc * stc) / 3.0);
  
  //diagonal terms
  couplns[0] = - 2.0 * stc * stc - (3.0 * H * spp + dspp) - 2.0 * stp * stp * dHpmspp - 2.0 * (3.0 * H * stp * stp + 2.0 * stp * dstp) * Hpmspp;
  couplns[1] = - 2.0 * stp * stp - (3.0 * H * spp + dspp) - 2.0 * stc * stc * dHpmspp - 2.0 * (3.0 * H * stc * stc + 2.0 * stc * dstc) * Hpmspp;
  //coupling terms
  couplns[2] = 2.0 * stc * stp - 2.0 * (stp * stc * dHpmspp + (3.0 * H * stp * stc + dstp * stc + stp * dstc) * Hpmspp);
  
}