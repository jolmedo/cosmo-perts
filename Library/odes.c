/*
These codes have been developed by J. Olmedo, V. Sreenath and I. Agullo. You are 
free to use them for any scientific or pedagogical purpose. However, the authors 
are not responsible of any mistake or typo in these codes. Nevertheless, we will 
acknowledge any feedback at this respect. Besides, if you plan to use these codes 
(or a modified version based on them) for scientific purposes (i.e. not pedagogical 
purposes), please, we will very much appreciate if you cite any of the following 
references: 
arXiv:1702.06036; arXiv:1601.01716.
Date: 05/11/19.
Place: Louisiana State University
*/


#include "odes.h"


struct param_const {
/**
 *  Some global parameters.
 */
    double mass;
    double gamma;
    double G;
    double lambda;
    double lp;
    double kmin;
    double kmax;
    int N;    
};

/****************  Definition of the odes of FRW in GR  *************************/

int
func (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of FRW in GR.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> dphi. 
  struct param_const *my_params_pointer = params;
  double a = exp(y[1]/3.0);
  double dV = dVphi2(y, &*my_params_pointer);
  double K = Kphi(y, &*my_params_pointer);
  f[0] = 1.0 / a;
  f[1] = 3.0 * y[3] / (my_params_pointer->gamma);
  f[2] = y[4];
  f[3] = - 8.0 * M_PI * my_params_pointer->G * my_params_pointer->gamma * K;
  f[4] = - f[1] * y[4] - dV;
  return GSL_SUCCESS;
}


int
jac (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of FRW in GR necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, 5, 5);
  gsl_matrix * m = &dfdy_mat.matrix; 
  int i, j;
  for(i = 0; i < 5; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < 5; j++){
      gsl_matrix_set (m, i, j, 0.0);
    }
  }
  return GSL_SUCCESS;
}

/****************  Definition of the odes of FRW in LQC  *************************/

int
func_lqc (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of FRW in LQC.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> dphi. 
  struct param_const *my_params_pointer = params;
  double a = exp(y[1]/3.0);
  double K = Kphi(y, &*my_params_pointer);
  double dV = dVphi2(y, &*my_params_pointer);
  //double dV = dVpf_lqc(y, &*my_params_pointer);
  
  f[0] = 1.0 / a;
  f[1] = (3.0 / 2.0) * sin(2.0 * my_params_pointer->lambda * y[3]) / (my_params_pointer->gamma * my_params_pointer->lambda);
  f[2] = y[4];
  f[3] = - 8.0 * M_PI * my_params_pointer->G * my_params_pointer->gamma * K;
  //f[3] = - (3.0 / 2.0) * pow(sin(my_params_pointer->lambda * y[3]),2.0)/(my_params_pointer->gamma * pow(my_params_pointer->lambda,2.0)) + 4.0 * M_PI * my_params_pointer->G * my_params_pointer->gamma*(-P);
  f[4] = - f[1] * y[4] - dV;
  return GSL_SUCCESS;
}

int
jac_lqc (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of FRW in LQC necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  struct param_const *my_params_pointer = params;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, 5, 5);
  gsl_matrix * m = &dfdy_mat.matrix; 
  gsl_matrix_set (m, 0, 0, 0.0); 
  gsl_matrix_set (m, 0, 1, -1.0/3.0 * exp(-y[1]/3.0));
  gsl_matrix_set (m, 0, 2, 0.0);
  gsl_matrix_set (m, 0, 3, 0.0);
  gsl_matrix_set (m, 0, 4, 0.0);
  gsl_matrix_set (m, 1, 0, 0.0);
  gsl_matrix_set (m, 1, 1, 0.0);
  gsl_matrix_set (m, 1, 2, 0.0);
  gsl_matrix_set (m, 1, 3, (3.0)*cos(2.0*my_params_pointer->lambda*y[3])/(my_params_pointer->gamma));
  gsl_matrix_set (m, 1, 4, 0.0);
  gsl_matrix_set (m, 2, 0, 0.0);
  gsl_matrix_set (m, 2, 1, 0.0);
  gsl_matrix_set (m, 2, 2, 0.0);
  gsl_matrix_set (m, 2, 3, 0.0);
  gsl_matrix_set (m, 2, 4, 1.0);
  gsl_matrix_set (m, 3, 0, 0.0);
  gsl_matrix_set (m, 3, 1, 0.0);
  gsl_matrix_set (m, 3, 2, 4.0*M_PI*my_params_pointer->G*my_params_pointer->gamma*(pow((my_params_pointer->mass),2))*y[2]);
  gsl_matrix_set (m, 3, 3, -(3.0/2.0)*sin(2.0*my_params_pointer->lambda*y[3])/(my_params_pointer->gamma*my_params_pointer->lambda));
  gsl_matrix_set (m, 3, 4, 4.0*M_PI*my_params_pointer->G*my_params_pointer->gamma*(-y[4]));
  gsl_matrix_set (m, 4, 0, 0.0); 
  gsl_matrix_set (m, 4, 1, 0.0);
  gsl_matrix_set (m, 4, 2, -pow(my_params_pointer->mass,2));
  gsl_matrix_set (m, 4, 3, -y[4] * (3.0)*cos(2.0*my_params_pointer->lambda*y[3])/(my_params_pointer->gamma));
  gsl_matrix_set (m, 4, 4, -(3.0/2.0)*sin(2.0*my_params_pointer->lambda*y[3])/(my_params_pointer->gamma*my_params_pointer->lambda));
  dfdt[0] = 0.0;
  dfdt[1] = 0.0;
  dfdt[2] = 0.0;
  dfdt[3] = 0.0;
  dfdt[4] = 0.0;
  return GSL_SUCCESS;
}


/****************  Definition of the odes of FRW in QRLG  *************************/


struct param_int {
/**
 *  Some useful parameters for QRLG.
 */
  double sigma;
  double lb;

};

int
func_rlqg (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of FRW in QRLG.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> dphi. 
  struct param_const *my_params_pointer = params;
  double a = exp(y[1]/3.0);
  double K = Kphi(y, &*my_params_pointer);
  double V = Vphi2(y, &*my_params_pointer);
  double dV = dVphi2(y, &*my_params_pointer);

  double sigma = exp(y[1])/pow(my_params_pointer->lambda,3.0);
  double lb = y[3] * my_params_pointer->lambda;

  struct param_int param = {sigma, lb};
  
  double INT1, INT2;

  //  gsl_function F0;
  //  F0.function = &intnd0;
  //  F0.params = my_params_pointer;
  
  gsl_function F1;
  F1.function = &intnd1;
  F1.params = &param;
  gsl_function F2;
  F2.function = &intnd2;
  F2.params = &param;

  //  INT0 = intgr(&F0);
    INT1 = intgr(&F1);
    INT2 = intgr(&F2);

  //  double sigma = exp(my_params_pointer->logv)/pow(my_params_pointer->lambda,3.0);
  double factor = 1.0 / (2.0 * sqrt(M_PI) * my_params_pointer->gamma * my_params_pointer->lambda);
  double dH = - 3.0 * pow(2.0 , 2.0/3.0) / (my_params_pointer->gamma * pow(my_params_pointer->lambda,2.0) * sqrt(M_PI)) * sqrt(sigma) * exp(-sigma) * pow(sin(my_params_pointer->lambda * y[3] / pow(2.0,1.0/3.0)),2.0);
 
  f[0] = 1.0 / a;
  f[1] = 3.0 * factor * INT1;
  f[2] = y[4];
  f[3] = M_PI * my_params_pointer->G * my_params_pointer->gamma / 3.0 * (-5.0 * pow(y[4],2) + 14.0 * V) + dH - y[3] * factor * INT1 - 3.0 * sigma * factor * INT2 / my_params_pointer->lambda;
  f[4] = - f[1] * y[4] - dV;
  return GSL_SUCCESS;
}


int
func_rlqg_sp (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of FRW in QRLG within the suddle point approximation.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> dphi. 
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double G = my_params_pointer->G;
  
  double a = exp(y[1]/3.0);
  double K = Kphi(y, &*my_params_pointer);
  double V = Vphi2(y, &*my_params_pointer);
  double dV = dVphi2(y, &*my_params_pointer);

  f[0] = 1.0 / a;
  f[1] = (y[3] * pow(l,3.0) * cos(2.0 * y[3] * l)) / (exp(y[1]) * 6.0 * g) + (3.0 * sin(2.0 * y[3] * l))/(2.0 * g * l) - (l * l * sin(2.0 * y[3] * l))/(12.0 * g * exp(y[1])) - (y[3] * y[3] * pow(l,4.0) * sin(2.0 * y[3] * l)) / (6.0 * g * exp(y[1]));
  f[2] = y[4];
  f[3] = (-3.0 * pow(sin(l * y[3]),2.0)) / (2.0 * g * pow(l,2.0)) + 4.0 * g * G * M_PI * (V - K); 
  f[4] = - f[1] * y[4] - dV;
  return GSL_SUCCESS;
}

int
jac_rlqg (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of FRW in QRLG necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, 5, 5);
  gsl_matrix * m = &dfdy_mat.matrix; 
  int i, j;
  for(i = 0; i < 5; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < 5; j++){
      gsl_matrix_set (m, i, j, 0.0);
    }
  }
  return GSL_SUCCESS;
}


/****************  Definition of the odes of BianchiI in GR  *************************/

struct param_const_b1 {
/**
 *  Some gobal parameters for Bianchi I models.
 */

  double mass;
  double gamma;
  double G;
  double lambda;
  double lp;    
  double alp;
  double bet;
  double gam;  
  double kmin;
  double kmax; 
  int N; 
};

int
func_b1 (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of Bianchi I spacetimes in GR.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double K = Kphi_b1(y, &*my_params_pointer);
  double dV = dVphi2_b1(y, &*my_params_pointer);
  f[0] = 1.0 / a;
  f[1] = 1.0 / (g * l) * (y[7] + y[6]);
  f[2] = 1.0 / (g * l) * (y[5] + y[7]);
  f[3] = 1.0 / (g * l) * (y[6] + y[5]);
  f[4] = y[8];

  f[5] = 1.0/2.0
    * ( + y[6] * f[2] 
	+ y[7] * f[3]
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[5] * (f[2] + f[3]));

  f[6] = 1.0/2.0
    * ( + y[7] * f[3] 
	+ y[5] * f[1]  
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[6] * (f[3] + f[1]));


  f[7] =  1.0/2.0 
    * ( + y[5] * f[1]  
	+ y[6] * f[2]  
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[7] * (f[1] + f[2]));

  double H =1.0/6.0 * (f[1] + f[2] + f[3]);

  f[8] = -3.0 * H * y[8] - dV;
  return GSL_SUCCESS;
}


int
jac_b1 (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of Bianchi I spacetimes necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, 9, 9);
  gsl_matrix * m = &dfdy_mat.matrix; 
  
  int i, j;
  for(i = 0; i < 9; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < 9; j++){
       gsl_matrix_set (m, i, j, 0.0);
    }
  }
 
  return GSL_SUCCESS;
}



int
func_b1_alpha (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of Bianchi I spacetimes in GR including the angle $alpha(t)$ introduced in arxiv:0801.3596 for the evolution of perturbations.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  //double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  //double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  //double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  double g11 = exp(2.0 * (-2.0 * y[1] + y[2] + y[3]) / 3.0);
  double g22 = exp(2.0 * (y[1] - 2.0 * y[2] + y[3]) / 3.0);
  double g33 = exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);

  //double a1oa2 = exp(- y[1] + y[2]);
  //double a3oa1 = exp(y[1] - y[3]);
   
  double K = Kphi_b1(y, &*my_params_pointer);
  double dV = dVphi2_b1(y, &*my_params_pointer);
  //double dV = dVpf_b1_lqc(y,&*my_params_pointer);
  f[0] = 1.0 / a;
  f[1] = 1.0 / (g * l) * (y[7] + y[6]);
  f[2] = 1.0 / (g * l) * (y[5] + y[7]);
  f[3] = 1.0 / (g * l) * (y[6] + y[5]);
  f[4] = y[8];

  f[5] = 1.0/2.0
    * ( + y[6] * f[2] 
	+ y[7] * f[3]
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[5] * (f[2] + f[3]));

  f[6] = 1.0/2.0
    * ( + y[7] * f[3] 
	+ y[5] * f[1]  
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[6] * (f[3] + f[1]));


  f[7] =  1.0/2.0 
    * ( + y[5] * f[1]  
	+ y[6] * f[2]  
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[7] * (f[1] + f[2]));

  double h1 = 1.0 / 2.0 * (-f[1] + f[2] + f[3]);
  double h2 = 1.0 / 2.0 * (f[1] - f[2] + f[3]);
  double H =1.0/6.0 * (f[1] + f[2] + f[3]);

  f[8] = -3.0 * H * y[8] - dV;

 //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
 //Time-dependent Euler angles
  double gam_t = atan(sqrt(g11 / g22) * tan(gam));
  double bet_t = atan((sqrt(g33 / g11) * cos(gam) / cos(gam_t) * tan(bet)));
  f[9] = -cos(bet_t) *  tan(gam) / (sqrt(g22 / g11) + sqrt(g11 / g22) * tan(gam) * tan(gam)) * (h1  - h2);

  return GSL_SUCCESS;
}

int
jac_b1_alpha (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of Bianchi I spacetimes in GR (including the angle $alpha(t)$ introduced in arxiv:0801.3596 for the evolution of perturbations) necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  //struct param_const_b1 *my_params_pointer = params;
  //double l = my_params_pointer->lambda;
  //double g = my_params_pointer->gamma;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, 10, 10);
  gsl_matrix * m = &dfdy_mat.matrix; 
  
  int i, j;
  for(i = 0; i < 10; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < 10; j++){
       gsl_matrix_set (m, i, j, 0.0);
    }
  }
 
  return GSL_SUCCESS;
}


/****************  Definition of the odes of BianchiI in AW LQC dynamics   *************************/


int
func_b1_lqc (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of Bianchi I spacetimes in AW LQC in arXiv:0903.3397.
 */
  //(void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
   
  double K = Kphi_b1(y, &*my_params_pointer);
  double dV = dVphi2_b1(y, &*my_params_pointer);
  //double dV = dVpf_b1_lqc(y,&*my_params_pointer);
  f[0] = 1.0 / a;
  f[1] = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  f[2] = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  f[3] = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  f[4] = y[8];

  f[5] = 1.0/2.0
    * ( + y[6] * f[2] 
	+ y[7] * f[3]
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[5] * (f[2] + f[3]));

  f[6] = 1.0/2.0
    * ( + y[7] * f[3] 
	+ y[5] * f[1]  
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[6] * (f[3] + f[1]));


  f[7] =  1.0/2.0 
    * ( + y[5] * f[1]  
	+ y[6] * f[2]  
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[7] * (f[1] + f[2]));

  double H =1.0/6.0 * (f[1] + f[2] + f[3]);

  f[8] = -3.0 * H * y[8] - dV;
  return GSL_SUCCESS;
}


int
jac_b1_lqc (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of Bianchi I spacetimes in AW LQC in arXiv:0903.3397 necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods). 
 */
  (void)(t); /* avoid unused parameter warning */
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, 9, 9);
  gsl_matrix * m = &dfdy_mat.matrix; 
  
  int i, j;
  for(i = 0; i < 9; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < 9; j++){
       gsl_matrix_set (m, i, j, 0.0);
    }
  }

  gsl_matrix_set (m, 0, 1, exp((y[1] + y[2] + y[3]) / 6.0) / 6.0);
  gsl_matrix_set (m, 0, 2, exp((y[1] + y[2] + y[3]) / 6.0) / 6.0);
  gsl_matrix_set (m, 0, 3, exp((y[1] + y[2] + y[3]) / 6.0) / 6.0);
  gsl_matrix_set (m, 1, 5, - 1.0 / (g * l) * sin(y[5]) * (sin(y[7]) + sin(y[6])));
  gsl_matrix_set (m, 1, 6, 1.0 / (g * l) * cos(y[5]) * cos(y[6]));
  gsl_matrix_set (m, 1, 7, 1.0 / (g * l) * sin(y[5]) * cos(y[7]));
  gsl_matrix_set (m, 2, 5, 1.0 / (g * l) * cos(y[6]) * cos(y[5]));
  gsl_matrix_set (m, 2, 6, - 1.0 / (g * l) * sin(y[6]) * (sin(y[7]) + sin(y[5])));
  gsl_matrix_set (m, 2, 7, 1.0 / (g * l) * cos(y[6]) * cos(y[7]));
  gsl_matrix_set (m, 3, 5, 1.0 / (g * l) * cos(y[7]) * cos(y[5]));
  gsl_matrix_set (m, 3, 6, 1.0 / (g * l) * cos(y[7]) * cos(y[6]));
  gsl_matrix_set (m, 3, 7, - 1.0 / (g * l) * sin(y[7]) * (sin(y[6]) + sin(y[5])));
  gsl_matrix_set (m, 4, 8, 1.0);

  gsl_matrix_set (m, 5, 5, 1.0 / 2.0 / (g * l)
  * ( + y[6] * cos(y[6]) * cos(y[5]) 
	+ y[7] * cos(y[7]) * cos(y[5])
	- y[5] * (cos(y[6]) * cos(y[5]) + cos(y[7]) * cos(y[5])) - (cos(y[6]) * (sin(y[5]) + sin(y[7])) + cos(y[7]) * (sin(y[6]) + sin(y[5])))));
  gsl_matrix_set (m, 5, 6, 1.0 / 2.0 / (g * l)
	* ( cos(y[6]) * (sin(y[5]) + sin(y[7])) + y[6] * (-sin(y[6]) * (sin(y[7]) + sin(y[5]))) 
	+ y[7] *  cos(y[7]) * cos(y[6])
	- y[5] * (-sin(y[6]) * (sin(y[7]) + sin(y[5])) +  cos(y[7]) * cos(y[6]))));
  gsl_matrix_set (m, 5, 7, 1.0 / 2.0 / (g * l)
    * ( cos(y[7]) * (sin(y[6]) + sin(y[5])) + y[6] * cos(y[6]) * cos(y[7]) 
	+ y[7] * (-sin(y[7]) * (sin(y[6]) + sin(y[5])))
	- y[5] * (cos(y[6]) * cos(y[7]) - sin(y[7]) * (sin(y[6]) + sin(y[5])))));
  gsl_matrix_set (m, 5, 8, - 8.0 * M_PI * my_params_pointer->G * g * l * y[8]);

  gsl_matrix_set (m, 6, 5, 1.0 / 2.0 / (g * l)
    * ( cos(y[5]) * (sin(y[7]) + sin(y[6])) + y[7] * cos(y[7]) * cos(y[5]) 
	+ y[5] * (-sin(y[5]) * (sin(y[7]) + sin(y[6])))  
	- y[6] * (cos(y[7]) * cos(y[5]) - sin(y[5]) * (sin(y[7]) + sin(y[6])))));
  gsl_matrix_set (m, 6, 6, 1.0 / 2.0 / (g * l)
    * ( - (cos(y[7]) * (sin(y[6]) + sin(y[5])) + cos(y[5]) * (sin(y[7]) + sin(y[6]))) + y[7] * cos(y[7]) * cos(y[6]) 
	+ y[5] * cos(y[5]) * cos(y[6])  
	- y[6] * (cos(y[7]) * cos(y[6]) + cos(y[5]) * cos(y[6]))));
  gsl_matrix_set (m, 6, 7, 1.0 / 2.0 / (g * l)
	* ( cos(y[7]) * (sin(y[6]) + sin(y[5])) + y[7] * (-sin(y[7]) * (sin(y[6]) + sin(y[5]))) 
	+ y[5] * sin(y[5]) * cos(y[7])  
	- y[6] * (- sin(y[7]) * (sin(y[6]) + sin(y[5])) + sin(y[5]) * cos(y[7]))));
  gsl_matrix_set (m, 6, 8, - 8.0 * M_PI * my_params_pointer->G * g * l * y[8]);

  gsl_matrix_set (m, 7, 5, 1.0 / 2.0 / (g * l)
	* ( cos(y[5]) * (sin(y[7]) + sin(y[6])) + y[5] * (-sin(y[5]) * (sin(y[7]) + sin(y[6])))  
	+ y[6] * cos(y[6]) * cos(y[5])  
	- y[7] * (-sin(y[5]) * (sin(y[7]) + sin(y[6])) + cos(y[6]) * cos(y[5]))));
  gsl_matrix_set (m, 7, 6, 1.0 / 2.0 / (g * l)
    * ( cos(y[6]) * (sin(y[5]) + sin(y[7])) + y[5] * cos(y[5]) * cos(y[6])  
	+ y[6] * (-sin(y[6]) * (sin(y[7]) + sin(y[5])))  
	- y[7] * (cos(y[5]) * cos(y[6]) - sin(y[6]) * (sin(y[7]) + sin(y[5])))));
  gsl_matrix_set (m, 7, 7, 1.0 / 2.0 / (g * l)
    * ( - (cos(y[5]) * (sin(y[7]) + sin(y[6])) + cos(y[6]) * (sin(y[5]) + sin(y[7])))) + y[5] * sin(y[5]) * cos(y[7])  
	+ y[6] * cos(y[6]) * cos(y[7])
        - y[7] * (sin(y[5]) * cos(y[7]) + cos(y[6]) * cos(y[7])));
  gsl_matrix_set (m, 7, 8, - 8.0 * M_PI * my_params_pointer->G * g * l * y[8]);

  gsl_matrix_set (m, 8, 4, - pow(my_params_pointer->mass,2));
  gsl_matrix_set (m, 8, 5, -y[8] /2.0 / (g * l) * (-sin(y[5]) * (sin(y[7]) + sin(y[6]))
  + cos(y[6]) * cos(y[5]) + cos(y[7]) * cos(y[5])));
  gsl_matrix_set (m, 8, 6, -y[8] /2.0 / (g * l) * (cos(y[5]) * cos(y[6])
  - sin(y[6]) * (sin(y[5]) + sin(y[7])) + cos(y[7]) * cos(y[6]) ));
  gsl_matrix_set (m, 8, 7, -y[8] /2.0 / (g * l) * (cos(y[5]) * cos(y[7]) 
  + cos(y[6]) * cos(y[7]) - sin(y[7]) * (sin(y[6]) + sin(y[5]))));
  gsl_matrix_set (m, 8, 8, - 1.0 / 2.0 /  (g * l) * (cos(y[5]) * (sin(y[7]) + sin(y[6]))
  + cos(y[6]) * (sin(y[5]) + sin(y[7])) + cos(y[7]) * (sin(y[6]) + sin(y[5])))); 
  
  return GSL_SUCCESS;
}


int
func_b1_lqc_alpha (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of Bianchi I spacetimes in AW LQC in arXiv:0903.3397 including the angle $alpha(t)$ introduced in arxiv:0801.3596 for the evolution of perturbations.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  //double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  //double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  //double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  double g11 = exp(2.0 * (-2.0 * y[1] + y[2] + y[3]) / 3.0);
  double g22 = exp(2.0 * (y[1] - 2.0 * y[2] + y[3]) / 3.0);
  double g33 = exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);

  //double a1oa2 = exp(- y[1] + y[2]);
  //double a3oa1 = exp(y[1] - y[3]);

  double K = Kphi_b1(y,&*my_params_pointer);
  double dV = dVphi2_b1(y,&*my_params_pointer);
  //double dV = dVpf_b1_lqc(y,&*my_params_pointer);
  f[0] = 1.0 / a;
  f[1] = 1.0 / (g * l) * cos(y[5]) * (sin(y[7]) + sin(y[6]));
  f[2] = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[7]));
  f[3] = 1.0 / (g * l) * cos(y[7]) * (sin(y[6]) + sin(y[5]));
  f[4] = y[8];

  f[5] = 1.0/2.0
    * ( + y[6] * f[2] 
	+ y[7] * f[3]
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[5] * (f[2] + f[3]));

  f[6] = 1.0/2.0
    * ( + y[7] * f[3] 
	+ y[5] * f[1]  
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[6] * (f[3] + f[1]));


  f[7] =  1.0/2.0 
    * ( + y[5] * f[1]  
	+ y[6] * f[2]  
	- 16.0 * M_PI * my_params_pointer->G * g * l * K - y[7] * (f[1] + f[2]));

  double h1 = 1.0 / 2.0 * (-f[1] + f[2] + f[3]);
  double h2 = 1.0 / 2.0 * (f[1] - f[2] + f[3]);
  double H =1.0 / 6.0 * (f[1] + f[2] + f[3]);
  
  f[8] = -3.0 * H * y[8] - dV;

 //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
 //Time-dependent Euler angles
  double gam_t = atan(sqrt(g11 / g22) * tan(gam));
  double bet_t = atan((sqrt(g33 / g11) * cos(gam) / cos(gam_t) * tan(bet)));
  f[9] = -cos(bet_t) *  tan(gam) / (sqrt(g22 / g11) + sqrt(g11 / g22) * tan(gam) * tan(gam)) * (h1  - h2);

  return GSL_SUCCESS;
}


int
jac_b1_lqc_alpha (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of Bianchi I spacetimes in AW LQC in arXiv:0903.3397 (including the angle $alpha(t)$ introduced in arxiv:0801.3596 for the evolution of perturbations) necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  //struct param_const_b1 *my_params_pointer = params;
  //double l = my_params_pointer->lambda;
  //double g = my_params_pointer->gamma;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, 10, 10);
  gsl_matrix * m = &dfdy_mat.matrix; 
  
  int i, j;
  for(i = 0; i < 10; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < 10; j++){
       gsl_matrix_set (m, i, j, 0.0);
    }
  }
 
  return GSL_SUCCESS;
}


/****************  Definition of the odes of BianchiI in AW LQC in vacuum dynamics   *************************/



int
func_b1_lqc_alp_vac (double t, const double y[], double f[],
      void *params)
{
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  //double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  //double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  //double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  double g11 = exp(2.0 * (-2.0 * y[1] + y[2] + y[3]) / 3.0);
  double g22 = exp(2.0 * (y[1] - 2.0 * y[2] + y[3]) / 3.0);
  double g33 = exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);

  //double a1oa2 = exp(- y[1] + y[2]);
  //double a3oa1 = exp(y[1] - y[3]);
   
  f[0] = 1.0 / a;
  f[1] = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  f[2] = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  f[3] = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));

  //Directional hubble parameters
  double h1 = 1.0 / 2.0 * (-f[1] + f[2] + f[3]);
  double h2 = 1.0 / 2.0 * (f[1] - f[2] + f[3]);
  double H =1.0/6.0 * (f[1] + f[2] + f[3]);

  f[4] = 1.0/2.0
    * (y[5] * f[2] + y[6] * f[3] - y[4] * (f[2] + f[3]));

  f[5] = 1.0/2.0
    * (y[6] * f[3] + y[4] * f[1] - y[5] * (f[3] + f[1]));

  f[6] =  1.0/2.0 
    * (y[4] * f[1] + y[5] * f[2] - y[6] * (f[1] + f[2]));

 //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
 //Time-dependent Euler angles
  double gam_t = atan(sqrt(g11 / g22) * tan(gam));
  double bet_t = atan((sqrt(g33 / g11) * cos(gam) / cos(gam_t) * tan(bet)));
  f[7] = -cos(bet_t) *  tan(gam) / (sqrt(g22 / g11) + sqrt(g11 / g22) * tan(gam) * tan(gam)) * (h1  - h2);

  return GSL_SUCCESS;
}


int
jac_b1_lqc_alp_vac (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
  (void)(t); /* avoid unused parameter warning */
  //struct param_const_b1 *my_params_pointer = params;
  //double l = my_params_pointer->lambda;
  //double g = my_params_pointer->gamma;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, 8, 8);
  gsl_matrix * m = &dfdy_mat.matrix; 
  
  int i, j;
  for(i = 0; i < 8; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < 8; j++){
       gsl_matrix_set (m, i, j, 0.0);
    }
  }
 
  return GSL_SUCCESS;
}



/****************  Definition of the odes of perturbations on FRW in GR  *************************/


int
func_perts (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of perturbations in FRW spacetimes in GR.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> dphi. 
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double mass = my_params_pointer->mass;
  double G = my_params_pointer->G;
  
  double a = exp(y[1]/3.0); //scale factor
  double h = y[3] / g; // hubble parameter
  double V = pow(mass*y[2],2)/2.0; // potential
  double dV = pow(mass,2)*y[2]; //derivative of the potential
  double ddV = pow(mass,2); //2nd derivative of the potential
  //EOMs for the background
  f[0] = 1.0/a;
  f[1] = 3.0 * h;
  f[2] = y[4];
  f[3] = -4.0 * M_PI * G * g * pow(y[4],2);
  f[4] = -f[1] * y[4] - dV;
  //EOMS for the perturbations

  double dh = f[3] / g;//time derivative (cosmic time) of hubble parameter
  double hh = pow(y[3]/g,2);
  double h2 = hh - 8.0 * M_PI * G / 3.0 * V;
  
  double ss_u = h2 * (19.0 - 18.0 * h2 / hh) + (ddV + 16.0 * M_PI * G * y[4] / h * dV - 16.0 * M_PI * G / 3.0 * V);//time-dependent mass of the MS-tensor variable divided by a^2
  double st_mu = -(8.0 * M_PI * G) * V + hh;//time-dependent mass of the MS-tensor variable divided by a^2
  int n = my_params_pointer->N;
  double kmin = my_params_pointer->kmin;
  double kmax = my_params_pointer->kmax;
  double dk = pow(kmax/kmin,1.0/(n-1));

  int i;
  double k = kmin, h3 = 3.0 * h, w2, w2s, w2t;
  for(i = 0; i < n; i++){
    w2 = (k * k) / ( a * a ) + 2.0 * hh + dh;
    w2s = w2 + ss_u;
    w2t = w2 + st_mu;
    f[5+8*i] = y[5+8*i+1];
    f[5+8*i+1] = - h3 * y[5+8*i+1] - w2s * y[5+8*i];
    f[5+8*i+2] = y[5+8*i+3];
    f[5+8*i+3] = - h3 * y[5+8*i+3] - w2s * y[5+8*i+2];
    f[9+8*i] = y[9+8*i+1];
    f[9+8*i+1] = - h3 * y[9+8*i+1] - w2t * y[9+8*i];
    f[9+8*i+2] = y[9+8*i+3];
    f[9+8*i+3] = - h3 * y[9+8*i+3] - w2t * y[9+8*i+2];
    k = k*dk;
  }
  return GSL_SUCCESS;
}

int
jac_perts (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of perturbations in FRW spacetimes in GR necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  struct param_const *my_params_pointer = params;
  int n = my_params_pointer->N;
  int l = 5 + 8*n;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, l, l);
  gsl_matrix * m = &dfdy_mat.matrix;

  int i, j;
  for(i = 0; i < l; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < l; j++){
      gsl_matrix_set (m, i, j, 0.0);
    }
  }

  return GSL_SUCCESS;
}


/****************  Definition of the odes of perturbations on FRW in LQC  *************************/


int
func_perts_lqc (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of perturbations in FRW spacetimes in LQC (currently the prescriptions implemented are the hybrid (arXiv:1809.09874), dressed metric (arXiv:1302.0254), isotropic pereira-pitrou (arXiv:0707.0736) and QRLG (arXiv:1811.04327)).
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> dphi. 
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double mass = my_params_pointer->mass;
  double G = my_params_pointer->G;
  
  double a = exp(y[1]/3.0); //scale factor
  double h = hubble_lqc(y, &*my_params_pointer); // hubble parameter
  double dh = dhubble_lqc(y, &*my_params_pointer); // hubble parameter
  double K = Kphi(y, &*my_params_pointer); //Kinetic energy
  double V = Vphi2(y, &*my_params_pointer); // potential
  double dV = dVphi2(y, &*my_params_pointer); //derivative of the potential
  double ddV = ddVphi2(y, &*my_params_pointer); //2nd derivative of the potential
  //double V = Vpf_lqc(y, &*my_params_pointer); // potential
  //double dV = dVpf_lqc(y, &*my_params_pointer); //derivative of the potential
  //double ddV = ddVpf_lqc(y, &*my_params_pointer); //2nd derivative of the potential
  
  
  //EOMs for the background
  f[0] = 1.0 / a;
  f[1] = 3.0 * h;
  f[2] = y[4];
  //f[3] = -(3.0/2.0)*pow(sin(l * y[3]),2)/(g * pow(l,2))+4.0 * M_PI * G * g * (V-pow(y[4],2)/2.0);
  f[3] = - 8.0 * M_PI * G * g * K; 
  f[4] = - f[1] * y[4] - dV;
  //EOMS for the perturbations
  //double dh = cos(2.0 * l * y[3]) / g * f[3];//time derivative (cosmic time) of hubble parameter


  double KVfs[6] = {h, K, V, dV, ddV,dh};
  double ss[2];
  //ss_hyb(y, KVfs, ss, &*my_params_pointer);
  //ss_drsd(y, KVfs, ss, &*my_params_pointer);
  //ss_ppu(y, KVfs, ss, &*my_params_pointer);
  ss_rlqg(y, KVfs, ss, &*my_params_pointer);
  double ss_u = ss[0];
  double st_mu = ss[1];

  int n = my_params_pointer->N;
  double kmin = my_params_pointer->kmin;
  double kmax = my_params_pointer->kmax;
  double dk = pow(kmax/kmin,1.0/(n-1));

  int i;
  double k = kmin, h3 = 3.0 * h, w2, w2s, w2t;
  for(i = 0; i < n; i++){
    w2 = (k * k) / ( a * a ) + 2.0 * h * h + dh;
    w2s = w2 + ss_u;
    w2t = w2 + st_mu;
    f[5+8*i] = y[5+8*i+1];
    f[5+8*i+1] = - h3 * y[5+8*i+1] - w2s * y[5+8*i];
    f[5+8*i+2] = y[5+8*i+3];
    f[5+8*i+3] = - h3 * y[5+8*i+3] - w2s * y[5+8*i+2];
    f[9+8*i] = y[9+8*i+1];
    f[9+8*i+1] = - h3 * y[9+8*i+1] - w2t * y[9+8*i];
    f[9+8*i+2] = y[9+8*i+3];
    f[9+8*i+3] = - h3 * y[9+8*i+3] - w2t * y[9+8*i+2];
    k = k*dk;
  }
  return GSL_SUCCESS;
}

int
jac_perts_lqc (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of perturbations in FRW spacetimes in GR necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  struct param_const *my_params_pointer = params;
  int n = my_params_pointer->N;
  int l = 5 + 8*n;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, l, l);
  gsl_matrix * m = &dfdy_mat.matrix;

  int i, j;
  for(i = 0; i < l; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < l; j++){
      gsl_matrix_set (m, i, j, 0.0);
    }
  }

  return GSL_SUCCESS;
}


/****************  Definition of the odes of perturbations on FRW in QRLG a la hybrid  *************************/


int
func_perts_rlqg (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of perturbations in FRW spacetimes in QRLG (currently the prescriptions implemented are the hybrid (arXiv:1809.09874), dressed metric (arXiv:1302.0254), isotropic pereira-pitrou (arXiv:0707.0736) and QRLG (arXiv:1811.04327)).
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> dphi. 
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double mass = my_params_pointer->mass;
  double G = my_params_pointer->G;
  
  double a = exp(y[1]/3.0); //scale factor
  double V = Vphi2(y, &*my_params_pointer); // potential
  double dV = dVphi2(y, &*my_params_pointer); //derivative of the potential
  double ddV = ddVphi2(y, &*my_params_pointer); //2nd derivative of the potential

  double sigma = exp(y[1])/pow(my_params_pointer->lambda,3.0);
  double lb = y[3] * my_params_pointer->lambda;

  struct param_int param = {sigma, lb};
  
  double INT1, INT2, INT3, INT4;

  //  gsl_function F0;
  //  F0.function = &intnd0;
  //  F0.params = my_params_pointer;
  
  gsl_function F1;
  F1.function = &intnd1;
  F1.params = &param;
  gsl_function F2;
  F2.function = &intnd2;
  F2.params = &param;
  gsl_function F3;
  F3.function = &intnd3;
  F3.params = &param;
  gsl_function F4;
  F4.function = &intnd4;
  F4.params = &param;

  //  INT0 = intgr(&F0);
    INT1 = intgr(&F1);
    INT2 = intgr(&F2);
    INT3 = intgr(&F3);
    INT4 = intgr(&F4);

  //  double sigma = exp(my_params_pointer->logv)/pow(my_params_pointer->lambda,3.0);
  double factor = 1.0 / (2.0 * sqrt(M_PI) * g * l);
  double dH = - 3.0 * pow(2.0 , 2.0/3.0) / (g * pow(l,2.0) * sqrt(M_PI)) * sqrt(sigma) * exp(-sigma) * pow(sin(l * y[3] / pow(2.0,1.0/3.0)),2.0);

  //EOMS for the background
  f[0] = 1.0/a;
  //  f[1] = 3.0 / (my_params_pointer->gamma) * y[3]  ;
  f[1] = 3.0 * factor * INT1;
  f[2] = y[4];
  //  f[3] = -4.0*M_PI*my_params_pointer->G*my_params_pointer->gamma*pow(y[4],2);
  f[3] = M_PI * G * g / 3.0 * (-5.0 * pow(y[4],2) + 14.0 * V) + dH - y[3] * factor * INT1 - 3.0 * sigma * factor * INT2 / l;
  f[4] = -f[1]*y[4]-dV;
  //EOMS for the perturbations

  double h = f[1] / 3.0; // hubble parameter
  double K = Kphi(y, &*my_params_pointer);
  double ddH = 3.0 * pow(2,4.0/3.0) * h * sqrt(sigma) * exp(-sigma) * sin(pow(2.0,2.0/3.0) * l * y[3]);
  double dh = - 5.0 * h * h / 2.0 + factor * (ddH + 3.0 * h * INT3 + (2.0 / l) * INT4 * (f[3] + y[3] * h));//time derivative (cosmic time) of hubble
  double KVfs[6] = {h, K, V, dV, ddV, dh};
  double ss[2];
  //ss_hyb(y, KVfs, ss, &*my_params_pointer);
  //ss_drsd(y, KVfs, ss, &*my_params_pointer);
  //ss_ppu(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_hyb(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_hyb_mink(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_drsd(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_drsd_mink(y, KVfs, ss, &*my_params_pointer);
  ss_rlqg(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_mink(y, KVfs, ss, &*my_params_pointer);
  double ss_u = ss[0];
  double st_mu = ss[1];

  int n = my_params_pointer->N;
  double kmin = my_params_pointer->kmin;
  double kmax = my_params_pointer->kmax;
  double dk = pow(kmax/kmin,1.0/(n-1));
  int i;
  double k = kmin, h3 = 3.0 * h, w2, w2s, w2t;
  for(i = 0; i < n; i++){
    w2 = (k * k) / ( a * a ) + 2.0 * h * h + dh;
    w2s = w2 + ss_u;
    w2t = w2 + st_mu;
    f[5+8*i] = y[5+8*i+1];
    f[5+8*i+1] = - h3 * y[5+8*i+1] - w2s * y[5+8*i];
    f[5+8*i+2] = y[5+8*i+3];
    f[5+8*i+3] = - h3 * y[5+8*i+3] - w2s * y[5+8*i+2];
    f[9+8*i] = y[9+8*i+1];
    f[9+8*i+1] = - h3 * y[9+8*i+1] - w2t * y[9+8*i];
    f[9+8*i+2] = y[9+8*i+3];
    f[9+8*i+3] = - h3 * y[9+8*i+3] - w2t * y[9+8*i+2];
    k = k*dk;
  }
  return GSL_SUCCESS;
}


int
func_perts_rlqg_sp (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of perturbations in FRW spacetimes in QRLG within the suddle point approximation (currently the prescriptions implemented are the hybrid (arXiv:1809.09874), dressed metric (arXiv:1302.0254), isotropic pereira-pitrou (arXiv:0707.0736) and QRLG (arXiv:1811.04327)).
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> v; y[2] -> phi;  y[3] -> b; y[4] -> dphi. 
  struct param_const *my_params_pointer = params;
  double g = my_params_pointer->gamma;
  double l = my_params_pointer->lambda;
  double mass = my_params_pointer->mass;
  double G = my_params_pointer->G;
  
  double a = exp(y[1]/3.0); //scale factor
  double h = hubble_rlqg_sp(y, &*my_params_pointer); // hubble parameter
  double dh = dhubble_rlqg_sp(y, &*my_params_pointer);//time derivative (cosmic time) of hubble parameter
  double K = Kphi(y, &*my_params_pointer); //Kinetic energy
  double V = Vphi2(y, &*my_params_pointer); // potential
  double dV = dVphi2(y, &*my_params_pointer); //derivative of the potential
  double ddV = ddVphi2(y, &*my_params_pointer); //2nd derivative of the potential  
  
  //EOMs for the background
  f[0] = 1.0 / a;
  f[1] = (y[3] * pow(l,3.0) * cos(2.0 * y[3] * l)) / (exp(y[1]) * 6.0 * g) + (3.0 * sin(2.0 * y[3] * l))/(2.0 * g * l) - (l * l * sin(2.0 * y[3] * l))/(12.0 * g * exp(y[1])) - (y[3] * y[3] * pow(l,4.0) * sin(2.0 * y[3] * l)) / (6.0 * g * exp(y[1]));
  f[2] = y[4];
  f[3] = (-3.0 * pow(sin(l * y[3]),2.0)) / (2.0 * g * pow(l,2.0)) + 4.0 * g * G * M_PI * (V - K); 
  f[4] = - f[1] * y[4] - dV;
  //EOMS for the perturbations

  double KVfs[6] = {h, K, V, dV, ddV,dh};
  double ss[2];
  //ss_hyb(y, KVfs, ss, &*my_params_pointer);
  //ss_drsd(y, KVfs, ss, &*my_params_pointer);
  //ss_ppu(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_hyb(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_hyb_mink(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_drsd(y, KVfs, ss, &*my_params_pointer);
  // ss_rlqg_drsd_mink(y, KVfs, ss, &*my_params_pointer);
  ss_rlqg(y, KVfs, ss, &*my_params_pointer);
  //ss_rlqg_mink(y, KVfs, ss, &*my_params_pointer);
  double ss_u = ss[0];
  double st_mu = ss[1];

  int n = my_params_pointer->N;
  double kmin = my_params_pointer->kmin;
  double kmax = my_params_pointer->kmax;
  double dk = pow(kmax/kmin,1.0/(n-1));

  int i;
  double k = kmin, h3 = 3.0 * h, w2, w2s, w2t;
  for(i = 0; i < n; i++){
    w2 = (k * k) / ( a * a ) + 2.0 * h * h + dh;
    w2s = w2 + ss_u;
    w2t = w2 + st_mu;
    f[5+8*i] = y[5+8*i+1];
    f[5+8*i+1] = - h3 * y[5+8*i+1] - w2s * y[5+8*i];
    f[5+8*i+2] = y[5+8*i+3];
    f[5+8*i+3] = - h3 * y[5+8*i+3] - w2s * y[5+8*i+2];
    f[9+8*i] = y[9+8*i+1];
    f[9+8*i+1] = - h3 * y[9+8*i+1] - w2t * y[9+8*i];
    f[9+8*i+2] = y[9+8*i+3];
    f[9+8*i+3] = - h3 * y[9+8*i+3] - w2t * y[9+8*i+2];
    k = k*dk;
  }
  return GSL_SUCCESS;
}

int
jac_perts_rlqg (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of perturbations in FRW spacetimes in LQC necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  struct param_const *my_params_pointer = params;
  int n = my_params_pointer->N;
  int l = 5 + 8*n;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, l, l);
  gsl_matrix * m = &dfdy_mat.matrix;

  int i, j;
  for(i = 0; i < l; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < l; j++){
      gsl_matrix_set (m, i, j, 0.0);
    }
  }
  return GSL_SUCCESS;
}



/****************  Definition of the odes of perturbations on Bianchi I in GR  *************************/

int
func_b1_perts (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of perturbations in Bianchi I spacetimes in GR as introduced in arXiv:0707.0736 and arxiv:0801.3596.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;
  //  double mass = my_params_pointer->mass;

  //EOMS for the background
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double KVfs[6] = {hubble_b1(y,&*my_params_pointer),
		    Kphi_b1(y,&*my_params_pointer),
		      Vphi2_b1(y,&*my_params_pointer),
		      dVphi2_b1(y,&*my_params_pointer),
		    ddVphi2_b1(y,&*my_params_pointer),dhubble_b1(y,&*my_params_pointer)};
  double gii[3] = {exp(2.0 * (-2.0 * y[1] + y[2] + y[3]) / 3.0),
		   exp(2.0 * (y[1] - 2.0 * y[2] + y[3]) / 3.0),
		   exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0)};
  double hi[3] = {h1(y, &*my_params_pointer),
		  h2(y, &*my_params_pointer),
		  h3(y, &*my_params_pointer)};

  //Generation of the dy/dt = f equations ofr the background
  dy_b1_gr(y, &*my_params_pointer, KVfs, gii, hi, f);

  double df[4], sij[3], dsij[3];
  //2nd time derivatives of y: d^2y/dt^2 
  ddy_b1_gr(y, &*my_params_pointer, f, df);
  // Components of the shear
  shear (y, f, df, sij, dsij);
  
  double Kt[4];
  // time-dependent angles gamma and beta, and norm and time derivative of the wavenumber vector
  hatkt(y, &*my_params_pointer, KVfs, gii, hi, Kt);
  double kt = Kt[2];
  double dkt = Kt[3];
    
  double fsij[5], fdsij[5], cplns[6];
  //Components of the shear in the SVT Fourier space decomposition of the perturbations
  fshear (y, &*my_params_pointer, KVfs, gii, hi, Kt, f, sij, dsij, fsij, fdsij);
  // Time-dependent functions in the equation of motion of the perturbations
  couplns (y, &*my_params_pointer, KVfs, f, df, fsij, fdsij, cplns);

  //diagonal terms
  double svv = cplns[0];
  double smupp = cplns[1];
  double smucc = cplns[2];
  //coupling terms
  double svmuc = cplns[3];
  double svmup = cplns[4];
  double smucmup = cplns[5];

  int n = my_params_pointer->N;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  double kmin = my_params_pointer->kmin;
  
  //We must recall that we have 10 homogeneous eoms
  int i,j, lbi, lbj;// i runs the number of modes while j the three orthogonal complex solutions
  double H = KVfs[0], H3 = 3.0 * H, w2, ws2, wp2, wc2, k = kmin;
  for(i = 0; i < my_params_pointer->N; i++){
    lbi = 36 * i;
    w2 = k * k * kt * kt / (a * a);
    ws2 = w2 + svv;
    wp2 = w2 + smupp;
    wc2 = w2 + smucc;
    for(j = 0; j < 3; j++){
      lbj = 10 + 12 * j + lbi;
      f[lbj] = y[lbj+1];
      f[lbj+1] = - H3 * y[lbj+1] - ws2 * y[lbj] - svmup * y[lbj+4] - svmuc * y[lbj+8];
      f[lbj+2] = y[lbj+3];
      f[lbj+3] = - H3 * y[lbj+3] - ws2 * y[lbj+2] - svmup * y[lbj+6] - svmuc * y[lbj+10];
      f[lbj+4] = y[lbj+5];
      f[lbj+5] = - H3 * y[lbj+5] - wp2 * y[lbj+4] - svmup * y[lbj] - smucmup * y[lbj+8];
      f[lbj+6] = y[lbj+7];
      f[lbj+7] = - H3 * y[lbj+7] - wp2 * y[lbj+6] - svmup * y[lbj+2] - smucmup * y[lbj+10];
      f[lbj+8] = y[lbj+9];
      f[lbj+9] = - H3 * y[lbj+9] - wc2 * y[lbj+8] - svmuc * y[lbj] - smucmup * y[lbj+4];
      f[lbj+10] = y[lbj+11];
      f[lbj+11] = - H3 * y[lbj+11] - wc2 * y[lbj+10] - svmuc * y[lbj+2] - smucmup * y[lbj+6];
    }
    k = k*dk;
  }

  return GSL_SUCCESS;
}

int
jac_b1_perts (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of perturbations in Bianchi I spacetimes in GR as introduced in arXiv:0707.0736 and arxiv:0801.3596 necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  struct param_const_b1 *my_params_pointer = params;
  //double l = my_params_pointer->lambda;
  //double g = my_params_pointer->gamma;
  int n = my_params_pointer->N;
  int nn = 10 + 36*n;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, nn, nn);
  gsl_matrix * m = &dfdy_mat.matrix; 
  
  int i, j;
  for(i = 0; i < nn; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < nn; j++){
       gsl_matrix_set (m, i, j, 0.0);
    }
  }
  
  return GSL_SUCCESS;
}


/****************  Definition of the odes of perturbations on Bianchi I in AW LQC  *************************/

int
func_b1_perts_lqc (double t, const double y[], double f[],
      void *params)
{
/**
 *  Definition of the odes of perturbations in Bianchi I spacetimes in AW LQC of arXiv:0903.3397 as introduced in arXiv:0707.0736 and arxiv:0801.3596.
 */
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;
  //  double mass = my_params_pointer->mass;

  //EOMS for the background
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  double KVfs[5] = {hubble_b1_lqc(y,&*my_params_pointer),
		    Kphi_b1(y,&*my_params_pointer),
		      Vphi2_b1(y,&*my_params_pointer),
		      dVphi2_b1(y,&*my_params_pointer),
		    ddVphi2_b1(y,&*my_params_pointer)};//Adding dhubble_lqc is very unefficient!!!
  /*double KVfs[5] = {hubble_b1_lqc(y,&*my_params_pointer),
		    Kphi_b1(y,&*my_params_pointer),
		      Vpf_b1_lqc(y,&*my_params_pointer),
		      dVpf_b1_lqc(y,&*my_params_pointer),
		      ddVpf_b1_lqc(y,&*my_params_pointer)};//Adding dhubble_lqc is very unefficient!!!
  */
  double gii[3] = {exp(2.0 * (-2.0 * y[1] + y[2] + y[3]) / 3.0),
		   exp(2.0 * (y[1] - 2.0 * y[2] + y[3]) / 3.0),
		   exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0)};
  double hi[3] = {h1_lqc(y, &*my_params_pointer),
		  h2_lqc(y, &*my_params_pointer),
		  h3_lqc(y, &*my_params_pointer)};

  //Generation of the dy/dt = f equations ofr the background
  dy_b1_lqc(y, &*my_params_pointer, KVfs, gii, hi, f);

  double df[4], sij[3], dsij[3];
  //2nd time derivatives of y: d^2y/dt^2 
  ddy_b1_lqc(y, &*my_params_pointer, f, df);
  // Components of the shear
  shear (y, f, df, sij, dsij);
  
  double Kt[4];
  // time-dependent angles gamma and beta, and norm and time derivative of the wavenumber vector
  hatkt(y, &*my_params_pointer, KVfs, gii, hi, Kt);
  double kt = Kt[2];
  double dkt = Kt[3];
    
  double fsij[5], fdsij[5], cplns[6];
  //Components of the shear in the SVT Fourier space decomposition of the perturbations
  fshear (y, &*my_params_pointer, KVfs, gii, hi, Kt, f, sij, dsij, fsij, fdsij);
  // Time-dependent functions in the equation of motion of the perturbations
  //couplns (y, &*my_params_pointer, KVfs, f, df, fsij, fdsij, cplns);
  couplns_lqc (y, &*my_params_pointer, KVfs, f, df, fsij, fdsij, cplns);

  //diagonal terms
  double svv = cplns[0];
  double smupp = cplns[1];
  double smucc = cplns[2];
  //coupling terms
  double svmuc = cplns[3];
  double svmup = cplns[4];
  double smucmup = cplns[5];

  int n = my_params_pointer->N;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  double kmin = my_params_pointer->kmin;
  
  //We must recall that we have 10 homogeneous eoms
  int i,j, lbi, lbj;// i runs the number of modes while j the three orthogonal complex solutions
  double H = KVfs[0], H3 = 3.0 * H, w2, ws2, wp2, wc2, k = kmin;
  for(i = 0; i < my_params_pointer->N; i++){
    lbi = 36 * i;
    H3 = 3.0 * H;
    w2 = k * k * kt * kt / (a * a);
    ws2 = w2 + svv;
    wp2 = w2 + smupp;
    wc2 = w2 + smucc;
    for(j = 0; j < 3; j++){
      lbj = 10 + 12 * j + lbi;
      f[lbj] = y[lbj+1];
      f[lbj+1] = - H3 * y[lbj+1] - ws2 * y[lbj] - svmup * y[lbj+4] - svmuc * y[lbj+8];
      f[lbj+2] = y[lbj+3];
      f[lbj+3] = - H3 * y[lbj+3] - ws2 * y[lbj+2] - svmup * y[lbj+6] - svmuc * y[lbj+10];
      f[lbj+4] = y[lbj+5];
      f[lbj+5] = - H3 * y[lbj+5] - wp2 * y[lbj+4] - svmup * y[lbj] - smucmup * y[lbj+8];
      f[lbj+6] = y[lbj+7];
      f[lbj+7] = - H3 * y[lbj+7] - wp2 * y[lbj+6] - svmup * y[lbj+2] - smucmup * y[lbj+10];
      f[lbj+8] = y[lbj+9];
      f[lbj+9] = - H3 * y[lbj+9] - wc2 * y[lbj+8] - svmuc * y[lbj] - smucmup * y[lbj+4];
      f[lbj+10] = y[lbj+11];
      f[lbj+11] = - H3 * y[lbj+11] - wc2 * y[lbj+10] - svmuc * y[lbj+2] - smucmup * y[lbj+6];
    }
    k = k*dk;
  }

  return GSL_SUCCESS;
}


int
jac_b1_perts_lqc (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
/**
 *  Definition of the jacobian of odes of perturbation in Bianchi I spacetimes in AW LQC in arXiv:0903.3397 (including the angle $alpha(t)$ introduced in arxiv:0801.3596 for the evolution of perturbations) necessary for implicit methods (in some cases we set it to zero since we focus on explicit methods).
 */
  (void)(t); /* avoid unused parameter warning */
  struct param_const_b1 *my_params_pointer = params;
  //double l = my_params_pointer->lambda;
  //double g = my_params_pointer->gamma;
  int n = my_params_pointer->N;
  int nn = 10 + 36*n;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, nn, nn);
  gsl_matrix * m = &dfdy_mat.matrix; 
  
  int i, j;
  for(i = 0; i < nn; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < nn; j++){
       gsl_matrix_set (m, i, j, 0.0);
    }
  }
  
  return GSL_SUCCESS;
}


/****************  Definition of the odes of perturbations on Bianchi I in AW LQC in vacuum  *************************/

int
func_b1_perts_lqc_vac (double t, const double y[], double f[],
      void *params)
{
  (void)(t); /* avoid unused parameter warning */
    //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi.
  
  struct param_const_b1 *my_params_pointer = params;
  double l = my_params_pointer->lambda;
  double g = my_params_pointer->gamma;
  double G = my_params_pointer->G;

  //EOMS for the background
  double a = exp((y[1] + y[2] + y[3]) / 6.0);
  //double a1 = exp((- y[1] + y[2] + y[3]) / 2.0);
  //double a2 = exp((y[1] - y[2] + y[3]) / 2.0);
  //double a3 = exp((y[1] + y[2] - y[3]) / 2.0);
  double g11 = exp(2.0 * (-2.0 * y[1] + y[2] + y[3]) / 3.0);
  double g22 = exp(2.0 * (y[1] - 2.0 * y[2] + y[3]) / 3.0);
  double g33 = exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0);
  
  f[0] = 1.0 / a;
  f[1] = 1.0 / (g * l) * cos(y[4]) * (sin(y[6]) + sin(y[5]));
  f[2] = 1.0 / (g * l) * cos(y[5]) * (sin(y[4]) + sin(y[6]));
  f[3] = 1.0 / (g * l) * cos(y[6]) * (sin(y[5]) + sin(y[4]));

  //Directional hubble parameters
  double h1 = 1.0 / 2.0 * (-f[1] + f[2] + f[3]);
  double h2 = 1.0 / 2.0 * (f[1] - f[2] + f[3]);
  double h3 = 1.0 / 2.0 * (f[1] + f[2] - f[3]);
  double H =1.0/6.0 * (f[1] + f[2] + f[3]);

  f[4] = 1.0/2.0
    * (y[5] * f[2] + y[6] * f[3] - y[4] * (f[2] + f[3]));

  f[5] = 1.0/2.0
    * (y[6] * f[3] + y[4] * f[1] - y[5] * (f[3] + f[1]));

  f[6] =  1.0/2.0 
    * (y[4] * f[1] + y[5] * f[2] - y[6] * (f[1] + f[2]));


  //Shear and its time derivative
  double s1 = 2.0 / 3.0 * (-2.0 * f[1] + f[2] + f[3]);
  double s2 = 2.0 / 3.0 * (f[1] - 2.0 * f[2] + f[3]);
  double s3 = 2.0 / 3.0 * (f[1] + f[2] - 2.0 * f[3]);
  //These are the 2n derivatives: logpi''
  double df1 = 1.0 / (g * l) * (- f[4] * sin(y[4]) * (sin(y[6]) + sin(y[5])) + cos(y[4]) * (f[6] * cos(y[6]) + f[5] * cos(y[5])));
  double df2 = 1.0 / (g * l) * (- f[5] * sin(y[5]) * (sin(y[4]) + sin(y[6])) + cos(y[5]) * (f[4] * cos(y[4]) + f[6] * cos(y[6])));
  double df3 = 1.0 / (g * l) * (- f[6] * sin(y[6]) * (sin(y[5]) + sin(y[4])) + cos(y[6]) * (f[5] * cos(y[5]) + f[4] * cos(y[4])));
  //This is the time derivative of H
  double dH =1.0/6.0 * (df1 + df2 + df3);
  //These are the time derivatives of si
  double ds1 = 2.0 / 3.0 * (-2.0 * df1 + df2 + df3);
  double ds2 = 2.0 / 3.0 * (df1 - 2.0 * df2 + df3);
  double ds3 = 2.0 / 3.0 * (df1 + df2 - 2.0 * df3);
  //Finally the definitions ofthe shear and its time derivative
  double sij[3] = {0.5 * s1 * exp(2.0 * (y[2] + y[3] - 2.0 * y[1]) / 3.0),
		   0.5 * s2 * exp(2.0 * (y[3] + y[1] - 2.0 * y[2]) / 3.0),
		   0.5 * s3 * exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0)};

  double dsij[3] = {0.5 * (s1 * s1 + ds1) * exp(2.0 * (y[2] + y[3] - 2.0 * y[1]) / 3.0),
		    0.5 * (s2 * s2 + ds2) * exp(2.0 * (y[3] + y[1] - 2.0 * y[2]) / 3.0),
		    0.5 * (s3 * s3 + ds3) * exp(2.0 * (y[1] + y[2] - 2.0 * y[3]) / 3.0)};
  

  //double alp = my_params_pointer->alp;//Final Euler angles
  double bet = my_params_pointer->bet;//Final Euler angles
  double gam = my_params_pointer->gam;//Final Euler angles
  //Time-dependent Euler angles
  double gam_t = atan(sqrt(g11 / g22) * tan(gam));
  double bet_t = atan((sqrt(g33 / g11) * cos(gam) / cos(gam_t) * tan(bet)));
  f[7] = -cos(bet_t) *  tan(gam) / (sqrt(g22 / g11) + sqrt(g11 / g22) * tan(gam) * tan(gam)) * (h1  - h2);

  
  //EOMS for the perturbations
  //Here y[9+4*i] = reu; y[9+4*i+1] -> reu'; y[9+4*i+2] -> imu; y[9+4*i+3] -> imu'; 
  int n = my_params_pointer->N;
  double dk = pow(my_params_pointer->kmax/my_params_pointer->kmin,1.0/(n-1));
  double kmin = my_params_pointer->kmin;
  
    
  double k1 = cosl(gam) * sinl(bet);
  double k2 = sinl(bet) * sinl(gam);
  double k3 = cosl(bet);

  double kt = pow(k1 * k1 / g11 + k2 * k2 / g22 + k3 * k3 / g33,0.5);
  double dkt = -  (k1 * k1 * (h1 - H) / g11 + k2 * k2 * (h2 - H) / g22 + k3 * k3 * (h3 - H) / g33) / kt;
  //Fourier vector basis
  double KI[3] = {k1 / (g11 * kt), k2 / (g22 * kt), k3 / ( g33 * kt)};
  double E1I[3] = {(sqrt(1.0 / g11) * (cos(y[7]) * cos(bet_t) * cos(gam_t) - sin(y[7]) * sin(gam_t))),
		   (sqrt(1.0 / g22) * (cos(gam_t) * sin(y[7]) + cos(y[7]) * cos(bet_t) * sin(gam_t))),
		   -((sqrt(1.0 / g33) * cos(y[7]) * sin(bet_t)))};
  double E2I[3] = {-((sqrt(1.0 / g11) * (cos(bet_t) * cos(gam_t) * sin(y[7]) + cos(y[7]) * sin(gam_t)))),
		   (sqrt(1.0 / g22) * (cos(y[7]) * cos(gam_t) - cos(bet_t) * sin(y[7]) * sin(gam_t))),
		   (sqrt(1.0 / g33) * sin(y[7]) * sin(bet_t))};
  
  //Components of the shear
  double spp =  KI[0] *  KI[0] * sij[0] +  KI[1] *  KI[1] * sij[1] +  KI[2] *  KI[2] * sij[2] - 1.0 / 3.0 * (sij[0] / g11 + sij[1] / g22 + sij[2] / g33);//This is spp = sij (KI KJ - 1/3 hIJ)
  double sv1 = sqrt(2.0) * (KI[0] * E1I[0] * sij[0] + KI[1] * E1I[1] * sij[1] + KI[2] * E1I[2] * sij[2]);//This is sv1 = sij(KI E1J+E1I KJ )/sqrt(2)
  double sv2 = sqrt(2.0) * (KI[0] * E2I[0] * sij[0] + KI[1] * E2I[1] * sij[1] + KI[2] * E2I[2] * sij[2]);//This is sv2 = sij(KI E2J+E2I KJ )/sqrt(2)
  double s11 = E1I[0] * E1I[0] * sij[0] + E1I[1] * E1I[1] * sij[1] + E1I[2] * E1I[2] * sij[2];//This is s11 = sij(E1I E1J)
  double s22 = E2I[0] * E2I[0] * sij[0] + E2I[1] * E2I[1] * sij[1] + E2I[2] * E2I[2] * sij[2];//This is s22 = sij(E2I E2J)
  double s12 = E1I[0] * E2I[0] * sij[0] + E1I[1] * E2I[1] * sij[1] + E1I[2] * E2I[2] * sij[2];//This is s12 = sij(E1I E2J) = sij(E2I E1J)
  double stp = (s11 - s22) / sqrt(2.0);//This is stp = sij(E1I E1J-E2I E2J )/sqrt(2)
  double stc = sqrt(2.0) * s12;//This is stc = sij(EII E2J+E2I E1J )/sqrt(2)

  //Time derivative of the components of the shear. We need the time derivative of the vectors
  //Time derivative of the Fourier basis elements
  double dKI[3] = {-(2.0 * (h1 - H) + dkt / kt) * KI[0],
		   -(2.0 * (h2 - H) + dkt / kt) * KI[1],
		   -(2.0 * (h3 - H) + dkt / kt) * KI[2]};
  double dE1I[3] = {(- s11) * E1I[0] - s12 * E2I[0],
		    (- s11) * E1I[1] - s12 * E2I[1],
		    (- s11) * E1I[2] - s12 * E2I[2]};
  double dE2I[3] = {(- s22) * E2I[0] - s12 * E1I[0],
		    (- s22) * E2I[1] - s12 * E1I[1],
		    (- s22) * E2I[2] - s12 * E1I[2]};
  
  double dspp = 2.0 * (KI[0] * dKI[0] * sij[0] +  KI[1] * dKI[1] * sij[1] + dKI[2] *  KI[2] * sij[2]) + KI[0] *  KI[0] * dsij[0] +  KI[1] *  KI[1] * dsij[1] +  KI[2] *  KI[2] * dsij[2] - 1.0 / 3.0 * (dsij[0] / g11 + dsij[1] / g22 + dsij[2] / g33) + 2.0 / 3.0 * (sij[0] * (h1 - H) / g11 + sij[1] * (h2 - H) / g22 + sij[2] * (h3 - H) / g33);
  double dsv1 = sqrt(2.0) * (dKI[0] * E1I[0] * sij[0] + dKI[1] * E1I[1] * sij[1] + dKI[2] * E1I[2] * sij[2] +
			     KI[0] * dE1I[0] * sij[0] + KI[1] * dE1I[1] * sij[1] + KI[2] * dE1I[2] * sij[2] +
			     KI[0] * E1I[0] * dsij[0] + KI[1] * E1I[1] * dsij[1] + KI[2] * E1I[2] * dsij[2]);
  double dsv2 = sqrt(2.0) * (dKI[0] * E2I[0] * sij[0] + dKI[1] * E2I[1] * sij[1] + dKI[2] * E2I[2] * sij[2] +
			     KI[0] * dE2I[0] * sij[0] + KI[1] * dE2I[1] * sij[1] + KI[2] * dE2I[2] * sij[2] +
			     KI[0] * E2I[0] * dsij[0] + KI[1] * E2I[1] * dsij[1] + KI[2] * E2I[2] * dsij[2]);
  double dstp = (E1I[0] * E1I[0] * dsij[0] + E1I[1] * E1I[1] * dsij[1] + E1I[2] * E1I[2] * dsij[2] - (E2I[0] * E2I[0] * dsij[0] + E2I[1] * E2I[1] * dsij[1] + E2I[2] * E2I[2] * dsij[2])) / sqrt(2.0) +  (E1I[0] * dE1I[0] * sij[0] + E1I[1] * dE1I[1] * sij[1] + E1I[2] * dE1I[2] * sij[2] - (E2I[0] * dE2I[0] * sij[0] + E2I[1] * dE2I[1] * sij[1] + E2I[2] * dE2I[2] * sij[2])) * sqrt(2.0);
  double dstc = sqrt(2.0) * (E1I[0] * E2I[0] * dsij[0] + E1I[1] * E2I[1] * dsij[1] + E1I[2] * E2I[2] * dsij[2] + dE1I[0] * E2I[0] * sij[0] + dE1I[1] * E2I[1] * sij[1] + dE1I[2] * E2I[2] * sij[2] + E1I[0] * dE2I[0] * sij[0] + E1I[1] * dE2I[1] * sij[1] + E1I[2] * dE2I[2] * sij[2]);

  //Time-dependent functions of the eoms
  double Hpmspp = (2.0 * H + spp) / ((2.0 * sv1 * sv1 + 2.0 * sv2 * sv2 + stp * stp + stc * stc) / 3.0);
  double dHpmspp = (- Hpmspp * (2.0 / 3.0 * (2.0 * sv1 * dsv1 + 2.0 * sv2 * dsv2 + stp * dstp + stc * dstc)) + (2.0 * dH + dspp)) /
    ((2.0 * sv1 * sv1 + 2.0 * sv2 * sv2 + stp * stp + stc * stc) / 3.0);
  
  //diagonal terms
  double smupp = - 2.0 * stc * stc - (3.0 * H * spp + dspp) - 2.0 * stp * stp * dHpmspp - 2.0 * (3.0 * H * stp * stp + 2.0 * stp * dstp) * Hpmspp;
  double smucc = - 2.0 * stp * stp - (3.0 * H * spp + dspp) - 2.0 * stc * stc * dHpmspp - 2.0 * (3.0 * H * stc * stc + 2.0 * stc * dstc) * Hpmspp;
  //coupling terms
  double smucmup = 2.0 * stc * stp - 2.0 * (stp * stc * dHpmspp + (3.0 * H * stp * stc + dstp * stc + stp * dstc) * Hpmspp);
  
  //We must recall that we have 10 homogeneous eoms
  int i,j;// i runs the number of modes while j the three orthogonal complex solutions
  int lbi, lbj;//These auxiliary integers improve the efficiency of computations and store the label of a given mode and the label of the solution, respectively.
  double H3, w2, ws2, wp2, wc2, k = kmin;
  for(i = 0; i < my_params_pointer->N; i++){
    lbi = 16 * i;
    H3 = 3.0 * H;
    w2 = k * k * kt * kt / (a * a);
    wp2 = w2 + smupp;
    wc2 = w2 + smucc;
    for(j = 0; j < 2; j++){
      lbj = 8 + 8 * j + lbi;
      f[lbj] = y[lbj+1];
      f[lbj+1] = - H3 * y[lbj+1] - wp2 * y[lbj] - smucmup * y[lbj+4];
      f[lbj+2] = y[lbj+3];
      f[lbj+3] = - H3 * y[lbj+3] - wp2 * y[lbj+2] - smucmup * y[lbj+6];
      f[lbj+4] = y[lbj+5];
      f[lbj+5] = - H3 * y[lbj+5] - wc2 * y[lbj+4] - smucmup * y[lbj];
      f[lbj+6] = y[lbj+7];
      f[lbj+7] = - H3 * y[lbj+7] - wc2 * y[lbj+6] - smucmup * y[lbj+2];
    }
    k = k*dk;
  }

  return GSL_SUCCESS;
}

int
jac_b1_perts_lqc_vac (double t, const double y[], double *dfdy, 
     double dfdt[], void *params)
{
  (void)(t); /* avoid unused parameter warning */
  struct param_const_b1 *my_params_pointer = params;
  //double l = my_params_pointer->lambda;
  //double g = my_params_pointer->gamma;
  int n = my_params_pointer->N;
  int nn = 8 + 16*n;
  gsl_matrix_view dfdy_mat 
    = gsl_matrix_view_array (dfdy, nn, nn);
  gsl_matrix * m = &dfdy_mat.matrix; 
  
  int i, j;
  for(i = 0; i < nn; i++){
    dfdt[i] = 0.0;
    for(j = 0; j < nn; j++){
       gsl_matrix_set (m, i, j, 0.0);
    }
  }
  
  return GSL_SUCCESS;
}