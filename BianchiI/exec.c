/*
These codes have been developed by J. Olmedo, V. Sreenath and I. Agullo. You are 
free to use them for any scientific or pedagogical purpose. However, the authors 
are not responsible of any mistake or typo in these codes. Nevertheless, we will 
acknowledge any feedback at this respect. Besides, if you plan to use these codes 
(or a modified version based on them) for scientific purposes (i.e. not pedagogical 
purposes), please, we will very much appreciate if you cite any of the following 
references: 
arXiv:1702.06036; arXiv:1601.01716.
Date: 05/11/19.
Place: Louisiana State University
*/

#include "../Library/functions.h"
#include "../Library/odes.h"
#include "../Library/vacua.h"


struct param_const_b1 {
  double mass;
  double gamma;
  double G;
  double lambda;
  double lp;
  double alp;
  double bet;
  double gam;
  double kmin;
  double kmax;  
  int N;
};

struct param_const {
    double mass;
    double gamma;
    double G;
    double lambda;
    double lp;
    double kmin;
    double kmax;
    int N;
};

void
id0 (double y[], void *params)
{
  struct param_const_b1 *my_params_pointer = params;

  double y1tf = 1.2697727407e+02; double y2tf = 1.2482597870e+02; double y3tf = 1.2917393036e+02;
  //double y1tf = 1.3302488962e+02; double y2tf = 1.3046777250e+02; double y3tf = 1.3562736767e+02;
  double x1 = (4.0 * y1tf - 2.0 * y2tf - 2.0 * y3tf) / 6.0;
  double x2 = (4.0 * y2tf - 2.0 * y3tf - 2.0 * y1tf) / 6.0;
  double x3 = (4.0 * y3tf - 2.0 * y1tf - 2.0 * y2tf) / 6.0;

  double aux = 1.0;//if data is already normalized, set to 0

  y[0] = 1.0;
  y[1]=0.0 + aux * (x3 + x2);
  y[2]=0.0 + aux * (x1 + x3);
  y[3]=0.0 + aux * (x1 + x2);
  y[4]=1.38;
  //  y[4]=1.1;
  y[5]=1.570796327;y[6]=2.083767235;y[7]=1.057825419; 
  double Vb = Vphi2_b1(y,&*my_params_pointer);
  double C_gr = c_gr_b1_lqc(y,&*my_params_pointer);
  y[8] = sqrt(-2.0 * C_gr - 2.0 * Vb);
  y[9] = M_PI/4.0 - aux * (7.8539816637e-01 - 1.0e-4 * M_PI);
  y[9] = 0.0;
  //y[9] = M_PI/4.0 - aux * (1.0000042 * 7.8542882652e-01 - 1.0e-4 * M_PI);
  
}

void
id_ext (double y[], void *params)
{
//   (void)(t);
  //This initial data has been provided externally.
  struct param_const_b1 *my_params_pointer = params;

  y[0] = 1.0;
  y[1]= 7.2837950353e+00; 
  y[2]= 7.4745149252e+00;
  y[3]= 7.0930751372e+00;
  y[4]= -8.9142712569e-01;    
  y[5]= 3.1415746281e+00;
  y[6]= 3.1415750044e+00;
  y[7]= 3.1415742518e+00;
  double Vb = Vphi2_b1(y,&*my_params_pointer);
  double C_gr = c_gr_b1_lqc(y,&*my_params_pointer);
  y[8] = sqrt(-2.0 * C_gr - 2.0 * Vb);
  y[9] = my_params_pointer->alp;
  
}


int
main (void)
{
  //Here we initiallyze the values of some parameters
  
  double gamma = 0.2375;
  double lambda = 10.0*sqrt(4.0*sqrt(3.0)*M_PI*gamma);
  //If variables initiated externally, uncomment and provide the corresponding data.
  char outfile1 [80];
  double kmin, kmax, t0, tf;
  double Beta0, Gamma0;
  
  int status = scanf ("%79s %lf %lf %lf %lf %lf %lf", outfile1, &kmin, &kmax, &t0, &tf, &Beta0, &Gamma0);
  if (status == 0) 
    printf("Failed to read input file.\n");
  
  //These values are computed externally
  double Alpha = 0.0*3.1419185695e-04, Beta = 1.570796 + Beta0, Gamma = 0.00000314159 + Gamma0;
  //double Alpha = 0.0*3.1420317212e-04, Beta = 1.57064, Gamma = 1.57064;
  //double Alpha = 0.0*4.7063133391e-04, Beta = 0.000314159, Gamma = 0.000314159;
  //These values are provided by hand
  //  double Alpha = 1.0e-4 * M_PI, Beta = 1.0e-4 * M_PI, Gamma = 1.0e-4 * M_PI;

  struct param_const_b1 my_params = {1.27795e-6, gamma, 1.0, lambda, 1.0, Alpha, Beta, Gamma, kmin, kmax, 16};
  
  //Here we initialize the odes
  int n = my_params.N;
  gsl_odeiv2_system sys0 = {func_b1_lqc_alpha, jac_b1_lqc_alpha, 10, &my_params};
  gsl_odeiv2_system sys = {func_b1_perts_lqc, jac_b1_perts_lqc, 10 + 36 * n, &my_params};

  //These are the absolute and relative tolerances

  int i;
  double t = 0.0;
  double hinit, step0;

  double abs_e = 1.0e-16;
  double rel_e = 0.0;
  hinit = 1.0e-10;

  //Here I define the forward BG integrator to set alpha = 0 in the future
  gsl_odeiv2_driver * d2 = 
    gsl_odeiv2_driver_alloc_y_new (&sys0, gsl_odeiv2_step_rk8pd,
				   hinit, abs_e, rel_e);   
  
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi. 

  double y0[10];
  id0(y0, &my_params);
  //id_ext(y0, &my_params);

  // We do forward evolution in order to set alpha = 0.0 in the future.
  double dt0, ti;// Step. For backward evolution it must be negative!!!
  dt0 = 1.0e1;
  for (t = 0.0; t < 3.5e5; t=t)
    {      
      //We define a step that is finer around the bounce
      step0 = dt0 * (1.0 + (1.0e-3 / (1.0+pow(kmax,1.0)) / abs(dt0) - 1.0) / cosh(0.001 * t));
      ti = t + step0;

      int status = gsl_odeiv2_driver_apply (d2, &t, ti, y0);

      if (status != GSL_SUCCESS)
	{
	  printf("error, return value=%d\n", status);
	  break;
	}
    }

   //We set alpha = 0
   y0[9] = 0.0;

   hinit = -1.0e-10;
   dt0 = -1.0e1;

  //Here I define the backward BG integrator to set alpha = 0 in the future
  gsl_odeiv2_driver * d1 = 
    gsl_odeiv2_driver_alloc_y_new (&sys0, gsl_odeiv2_step_rk8pd,
				   hinit, abs_e, rel_e);   
  
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi. 
  for (t = ti + step0; t >= 0.0; t=t)
    {      
      //We define a step that is finer around the bounce
      step0 = dt0 * (1.0 + (1.0e-3/ (1.0+pow(kmax,1.0)) / abs(dt0) - 1.0) / cosh(0.001 * t));
      ti = t + step0;
      int status = gsl_odeiv2_driver_apply (d1, &t, ti, y0);

      if (status != GSL_SUCCESS)
	{
	  printf("error, return value=%d\n", status);
	  break;
	}
    }
   //   printf ("#Initial data: t = %e  y[0] = %e; y[1] = %e; y[2] = %e; y[3] = %e; y[4] = %e; y[5] = %e; y[6] = %e; y[7] = %e; y[8] = %e; y[9] = %e; \n", ti, y0[0], y0[1], y0[2], y0[3], y0[4], y0[5], y0[6], y0[7], y0[8], y0[9]);


   gsl_odeiv2_driver_free (d2);
   gsl_odeiv2_driver_free (d1);

   //Now we move to the time at which we want to start the evolution of perturbations

   t = 0.0; 

  if(t<t0)
    {
      hinit = 1.0e-10;
      dt0 = 1.0e1;
    }
  
  if(t>t0)
    {
      hinit = -1.0e-10;
      dt0 = -1.0e1;
    }

  //The backward-forward EOMs 

  gsl_odeiv2_driver * d0 = 
    gsl_odeiv2_driver_alloc_y_new (&sys0, gsl_odeiv2_step_rk8pd,
				   hinit, abs_e, rel_e);   
  
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi. 

  id0(y0, &my_params);
  //id_ext(y0, &my_params);

  for (t = ti + step0; abs(t) <= abs(t0); t=t)
    {      
      //We define a step that is finer around the bounce
      step0 = dt0 * (1.0 + (1.0e-3 / (1.0+pow(kmax,1.0)) / abs(dt0) - 1.0) / cosh(0.001 * t));
      ti = t + step0;
      int status = gsl_odeiv2_driver_apply (d0, &t, ti, y0);
      
      if (status != GSL_SUCCESS)
	{
	  printf("error, return value=%d\n", status);
	  break;
	}
    }

   t0 = ti;
   hinit = 1.0e-10;
   abs_e = 1.0e-17;// /pow(kmax,1.0);
   //rel_e = 1.0e-17;
   double ta, tb;
   ta = -5.0e2;
   tb = 1.55e6;
   //gsl_odeiv2_step_rk8pd, gsl_odeiv2_step_rk4, gsl_odeiv2_step_msadams
  gsl_odeiv2_driver * d = 
    gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_msadams,
				   hinit, abs_e, rel_e);
  
  
  //Here y[0] = eta; y[1] -> logp1; y[2] -> logp2; y[3] -> logp3; y[4] -> phi;  y[5] -> c1; y[6] -> c2; y[7] -> c3; y[4] -> dotphi. 

  double y[10 + 36 * n];
  //id_b1_Mink_lqc(y, y0, &my_params);
  id_b1_Mink2_lqc(y, y0, &my_params);

  //We compute some additional observables: Hubble param, densitized constraint, Ricci scalar, shear, slow-roll PS and slow-roll spectral index ns
  
  double H, C_gr, C, R, Sig2, K, V, Rho, SLPS, SLns, Bet_t, Gamm_t;
  H = hubble_b1_lqc(y, &my_params);
  R = ricci_b1(y, &my_params);
  K = Kphi_b1(y, &my_params);
  V = Vphi2_b1(y, &my_params);
  Rho = rho_KV(K,V);
  C_gr = c_gr_b1_lqc(y, &my_params);
  C = rel_c(C_gr,Rho);
  Bet_t = bet_t(y, &my_params);
  Gamm_t = gamm_t(y, &my_params);
  Sig2 = sig2_lqc(y, &my_params);
  SLPS = slPS_b1_lqc(y, &my_params);
  SLns = slns_b1(y, &my_params);

  //printf ("# The value of shear is = %e\n",Sig2);
  
//   These are the files where we will store the data from the simulations.
  
  FILE *fp;//here we save the homogeneous trajectory
  fp = fopen("./B1hom-traj.txt", "w+");

  FILE *fp1;//here we store the 1st normalized solution (excited on scalar modes) and their time derivative
  fp1 = fopen("./B1inh-sol1.txt", "w+");
  FILE *fp2;//here we store the 2nd normalized solution (excited on tensor-plus modes) and their time derivative
  fp2 = fopen("./B1inh-sol2.txt", "w+");
  FILE *fp3;//here we store the 3rd normalized solution (excited on tensor-cross modes) and their time derivative
  fp3 = fopen("./B1inh-sol3.txt", "w+");
  FILE *fp4;//here we store the orthogonality relations between the previous solutions
  fp4 = fopen("./B1inh-ortho.txt", "w+");
  
  //Headers
  int l = 14; //Mode
  int j;
  double Norms[6];//Variable to store the norms (and additional orthogonal conditions)
  norms_b1(y, Norms, l);

  //Header homo
    
  fprintf (fp,"#\n");
  fprintf (fp,"#   t        eta         logp1        logp2        logp3        logv         phi         c1         c2         c3         dphi         alpha        beta       gamma         H          C             R           Sig2           SLPS          SLns \n");
  fprintf (fp,"#\n");
  fprintf (fp,"#\n");
  fprintf (fp,"%.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e\n", t, y[0], y[1], y[2], y[3], (y[1] + y[2] + y[3])/2.0, y[4], y[5], y[6], y[7], y[8], y[9], Bet_t, Gamm_t, H, C, R, Sig2, SLPS, SLns);


  //  Header Perts
  fprintf (fp1,"#\n");
  fprintf (fp1,"#    t        re u1         re u1'         im u1         im u1'         re mup1         re mup1'         im mup1         im mup1'         re muc1         re muc1'         im muc1         im muc1\n");
  fprintf (fp1,"#\n");
  fprintf (fp1,"#\n");
  fprintf (fp1,"%.5e ", t);
  for(j = 0; j < 12; j++){
    fprintf (fp1,"%.5e ", y[10+36*l+j]);
  }
  fprintf (fp1,"\n");

  fprintf (fp2,"#\n");
  fprintf (fp2,"#    t        re u2         re u2'         im u2         im u2'         re mup2         re mup2'         im mup2         im mup2'         re muc2         re muc2'         im muc2         im muc2\n");
  fprintf (fp2,"#\n");
  fprintf (fp2,"#\n");
  fprintf (fp2,"%.5e ", t);
  for(j = 0; j < 12; j++){
    fprintf (fp2,"%.5e ", y[10+12+36*l+j]);
  }
  fprintf (fp2,"\n");

  fprintf (fp3,"#\n");
  fprintf (fp3,"#    t        re u3         re u3'         im u3         im u3'         re mup3         re mup3'         im mup3         im mup3'         re muc3         re muc3'         im muc3         im muc3\n");
  fprintf (fp3,"#\n");
  fprintf (fp3,"#\n");
  fprintf (fp3,"%.5e ", t);
  for(j = 0; j < 12; j++){
    fprintf (fp3,"%.5e ", y[10+24+36*l+j]);
  }
  fprintf (fp3,"\n");
  
  
  fprintf (fp4,"#\n");
  fprintf (fp4,"#    t     norm_u      norm_mup      norm_muc\n");
  fprintf (fp4,"#\n");
  fprintf (fp4,"#\n");
  fprintf (fp4,"%.5e ", t);
  for(j = 0; j < 6; j++){
    fprintf (fp4,"%.5e ", Norms[j]);
  }
  fprintf (fp4,"\n");
  fflush(stdout);
  
  //The main program:

  double step, dt;// Step. For backward evolution it must be negative!!!
  dt = 1.0e0 /(1.0+pow(kmax,1.0));
  i = 0;
  double T = t0;
  for (t = t0; t <= 1.2e5; t=t)
    {
      //printf ("# t = %e\n",t);
      if(i == (int)(100 * (ti-t0)/(tf-t0)))
	{
	  printf("progress %d out of 100\n", i);
	  i++;
	}
      //We define a step that is finer around the bounce
      //dt = 1.0e1;
      step =  dt * (1.0 + (1.0e-3 / (1.0+pow(kmax,1.0)) / dt - 1.0) / cosh(0.001 * t));

      //We define a step that is finer around the bounce and when the modes exit the horizon. This choice is made for the sake of accuracy of the code. 
      //step = dt * (1.0 + (1.0e-3 / (1.0+pow(kmax,1.0)) / dt - 1.0) / cosh(0.001 * t)) * (1.0 + (1.0e0/(1.0+pow(kmax,1.0))/dt - 1.0) / cosh(t / 2.5e5 - 7.5 / 2.5));

      ti = t + step;
      int status = gsl_odeiv2_driver_apply (d, &t, ti, y);

      if (status != GSL_SUCCESS)
	{
	  printf("error, return value=%d\n", status);
	  break;
	}

      if (1 == (int)((t-T)/10.0))
        {
	  T = t;
	    if (t > ta)
	      if (t < tb)
		{
		  H = hubble_b1_lqc(y, &my_params);
		  R = ricci_b1(y, &my_params);
		  K = Kphi_b1(y, &my_params);
		  V = Vphi2_b1(y, &my_params);
		  Rho = rho_KV(K,V);
		  C_gr = c_gr_b1_lqc(y, &my_params);
		  C = rel_c(C_gr,Rho);
		  Sig2 = sig2_lqc(y, &my_params);
		  Bet_t = bet_t(y, &my_params);
		  Gamm_t = gamm_t(y, &my_params);
		  SLPS = slPS_b1_lqc(y, &my_params);
		  SLns = slns_b1(y, &my_params);
		  norms_b1(y, Norms, l);
	    
	    
		  fprintf (fp,"%.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e\n", t, y[0], y[1], y[2], y[3], (y[1] + y[2] + y[3])/2.0, y[4], y[5], y[6], y[7], y[8], y[9], Bet_t, Gamm_t, H, C, R, Sig2, SLPS, SLns);
	    
		  fprintf (fp1,"%.5e ", t);
		  fprintf (fp2,"%.5e ", t);
		  fprintf (fp3,"%.5e ", t);
		  fprintf (fp4,"%.5e ", t);
		  for(j = 0; j < 12; j++){
		    fprintf (fp1,"%.5e ", y[10+36*l+j]);
		    fprintf (fp2,"%.5e ", y[10+12+36*l+j]);
		    fprintf (fp3,"%.5e ", y[10+24+36*l+j]);
		  }
		  fprintf (fp1,"\n");
		  fprintf (fp2,"\n");
		  fprintf (fp3,"\n");
      
		  for(j = 0; j < 6; j++){
		    fprintf (fp4,"%.5e ", Norms[j]);
		  }
		  fprintf (fp4,"\n");
		}
	}
	fflush(stdout);

	//printf("\033[2J");        /*  clear the screen  */
	//printf("\033[H");         /*  position cursor at top-left corner */

      }

  //Params for FRW
    struct param_const my_params_frw = {1.27795e-6, gamma, 1.0, lambda, 1.0, kmin, kmax, n};
    //Here we initialize the FRW odes
  gsl_odeiv2_system sys3 = {func_perts, jac_perts, 5 + 8*n, &my_params_frw};
  gsl_odeiv2_driver * d3 = 
    gsl_odeiv2_driver_alloc_y_new (&sys3, gsl_odeiv2_step_msadams,
				   hinit, abs_e, rel_e);

  double y0frw[5];//We borrow b1 background for frw
  b1_to_frw(y, y0frw, &my_params_frw);

  double yfrw[5 + 8 * n];//We initiate the frw solution
  id_Mink(yfrw, y0frw, &my_params_frw);

  double yb1[10 + 36 * n];  //the b1-like solution of our frw evolution.
  //alpha and beta matrices of the bogoliubov tranformation for each mode. 
  double alpha[n][2][3][3], alpha_aux[2][3][3]; 
  double beta[n][2][3][3], beta_aux[2][3][3];
  for (j = 0; j < n; j++){
    frw_to_b1(yfrw, yb1, j, &my_params_frw);
      alpha_b1 (y, yb1, alpha_aux, j);
      beta_b1 (y, yb1, beta_aux, j);
      for (int i1 = 0; i1 < 3; i1++){
	for (int i2 = 0; i2 < 3; i2++){
	  alpha[j][0][i1][i2] = alpha_aux[0][i1][i2];
	  alpha[j][1][i1][i2] = alpha_aux[1][i1][i2];
	  beta[j][0][i1][i2] = beta_aux[0][i1][i2];
	  beta[j][1][i1][i2] = beta_aux[1][i1][i2];
	}
      }
  }
  
  dt = 1.0e0 /pow(kmax,1.0);
  //t0 = t; 
  T = t;
  for (t = T; t < tf; t=t)
    {
      if(i == (int)(100 * (ti-t0)/(tf-t0)))
	{
	  printf("progress %d out of 100\n", i);
	  i++;
	}
      //We define a step that is finer around the bounce
      //dt = 1.0e1;
      //step =  dt * (1.0 + (1.0e-3 / pow(kmax,1.0) / dt - 1.0) / cosh(0.001 * t));

      //We define a step that is finer around the bounce and when the modes exit the horizon. This choice is made for the sake of accuracy of the code. 
      step = dt * (1.0 + (1.0e-3 / (1.0+pow(kmax,1.0)) / dt - 1.0) / cosh(0.001 * t)) * (1.0 + (1.0e0/(1.0+pow(kmax,1.0))/dt - 1.0) / cosh(t / 2.5e5 - 7.5 / 2.5));

      ti = t + step;
      int status = gsl_odeiv2_driver_apply (d3, &t, ti, yfrw);
      for (int i1 = 0; i1 < n; i1++){
	frw_to_b1(yfrw, yb1, i1, &my_params);
	frw_to_b1(yfrw, y, i1, &my_params);
      }
      basis_change(y, yb1, alpha, beta, n);
      
      if (status != GSL_SUCCESS)
	{
	  printf("error, return value=%d\n", status);
	  break;
	}

      
      
      if (1 == (int)((t-T)/10.0))
        {
	  T = t;
	    if (t > ta)
	      if (t < tb)
		{
		  H = hubble_b1_lqc(y, &my_params);
		  R = ricci_b1(y, &my_params);
		  K = Kphi_b1(y, &my_params);
		  V = Vphi2_b1(y, &my_params);
		  Rho = rho_KV(K,V);
		  C_gr = c_gr_b1_lqc(y, &my_params);
		  C = rel_c(C_gr,Rho);
		  Sig2 = sig2_lqc(y, &my_params);
		  Bet_t = bet_t(y, &my_params);
		  Gamm_t = gamm_t(y, &my_params);
		  SLPS = slPS_b1_lqc(y, &my_params);
		  SLns = slns_b1(y, &my_params);
		  norms_b1(y, Norms, l);
	    
	    
		  fprintf (fp,"%.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e\n", t, y[0], y[1], y[2], y[3], (y[1] + y[2] + y[3])/2.0, y[4], y[5], y[6], y[7], y[8], y[9], Bet_t, Gamm_t, H, C, R, Sig2, SLPS, SLns);
	    
		  fprintf (fp1,"%.5e ", t);
		  fprintf (fp2,"%.5e ", t);
		  fprintf (fp3,"%.5e ", t);
		  fprintf (fp4,"%.5e ", t);
		  for(j = 0; j < 12; j++){
		    fprintf (fp1,"%.5e ", y[10+36*l+j]);
		    fprintf (fp2,"%.5e ", y[10+12+36*l+j]);
		    fprintf (fp3,"%.5e ", y[10+24+36*l+j]);
		  }
		  fprintf (fp1,"\n");
		  fprintf (fp2,"\n");
		  fprintf (fp3,"\n");
      
		  for(j = 0; j < 6; j++){
		    fprintf (fp4,"%.5e ", Norms[j]);
		  }
		  fprintf (fp4,"\n");
		}
	}
	fflush(stdout);

	//printf("\033[2J");        /*  clear the screen  */
	//printf("\033[H");         /*  position cursor at top-left corner */

      }

  
  gsl_odeiv2_driver_free (d3);
  
  fclose(fp);
  fclose(fp1);
  fclose(fp2);
  fclose(fp3);
  fclose(fp4);
  gsl_odeiv2_driver_free (d);
  
  return 0;
}