var searchData=
[
  ['rel_5fc',['rel_c',['../functions_8c.html#aeffbcf9e831e98e47996532353befe56',1,'rel_c(double C_gr, double Rho):&#160;functions.c'],['../functions_8h.html#aeffbcf9e831e98e47996532353befe56',1,'rel_c(double C_gr, double Rho):&#160;functions.c']]],
  ['rho',['rho',['../functions_8c.html#a3d94145e0f153738bc26248468085ba7',1,'rho(const double y[], void *params):&#160;functions.c'],['../functions_8h.html#a3d94145e0f153738bc26248468085ba7',1,'rho(const double y[], void *params):&#160;functions.c']]],
  ['rho_5fb1',['rho_b1',['../functions_8c.html#a35041b013f8558df344758851aea4b46',1,'rho_b1(const double y[], void *params):&#160;functions.c'],['../functions_8h.html#a35041b013f8558df344758851aea4b46',1,'rho_b1(const double y[], void *params):&#160;functions.c']]],
  ['rho_5fkv',['rho_KV',['../functions_8c.html#a982611a422c31b5860ac48f2af0b887b',1,'rho_KV(double K, double V):&#160;functions.c'],['../functions_8h.html#a982611a422c31b5860ac48f2af0b887b',1,'rho_KV(double K, double V):&#160;functions.c']]],
  ['rhoq_5fh',['rhoQ_H',['../functions_8c.html#abce908ef58adabeb40f73ed02e568dce',1,'rhoQ_H(double H, double Rho, void *params):&#160;functions.c'],['../functions_8h.html#abce908ef58adabeb40f73ed02e568dce',1,'rhoQ_H(double H, double Rho, void *params):&#160;functions.c']]],
  ['ricci',['ricci',['../functions_8c.html#a4b29dfce7008747d17dc096aade411c4',1,'ricci(const double y[], void *params):&#160;functions.c'],['../functions_8h.html#a4b29dfce7008747d17dc096aade411c4',1,'ricci(const double y[], void *params):&#160;functions.c']]],
  ['ricci_5fb1',['ricci_b1',['../functions_8c.html#abb075e988730836286bac21d229793a1',1,'ricci_b1(const double y[], void *params):&#160;functions.c'],['../functions_8h.html#abb075e988730836286bac21d229793a1',1,'ricci_b1(const double y[], void *params):&#160;functions.c']]],
  ['ricci_5fh',['ricci_H',['../functions_8c.html#a87c129f7c9d01a36ee95dfb045d35261',1,'ricci_H(double H, double dH):&#160;functions.c'],['../functions_8h.html#a87c129f7c9d01a36ee95dfb045d35261',1,'ricci_H(double H, double dH):&#160;functions.c']]],
  ['ricci_5frlqg',['ricci_rlqg',['../functions_8c.html#a74b3f82fd1ac2717b88c09876f61c690',1,'ricci_rlqg(const double y[], void *params):&#160;functions.c'],['../functions_8h.html#a74b3f82fd1ac2717b88c09876f61c690',1,'ricci_rlqg(const double y[], void *params):&#160;functions.c']]]
];
