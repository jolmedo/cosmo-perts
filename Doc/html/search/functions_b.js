var searchData=
[
  ['p_5fkv',['P_KV',['../functions_8c.html#aecafb85ddb4da7c8160938c6f9bb8f5f',1,'P_KV(double K, double V):&#160;functions.c'],['../functions_8h.html#aecafb85ddb4da7c8160938c6f9bb8f5f',1,'P_KV(double K, double V):&#160;functions.c']]],
  ['pq_5fh',['PQ_H',['../functions_8c.html#a1618eb6c0d189577115601c079163e9a',1,'PQ_H(double H, double dH, double Rho, double P, void *params):&#160;functions.c'],['../functions_8h.html#a1618eb6c0d189577115601c079163e9a',1,'PQ_H(double H, double dH, double Rho, double P, void *params):&#160;functions.c']]],
  ['ps_5fb1',['PS_b1',['../vacua_8c.html#a4471e95e69ddf356662074a167b5337d',1,'PS_b1(const double y[], double ps[], int n, void *params):&#160;vacua.c'],['../vacua_8h.html#a4471e95e69ddf356662074a167b5337d',1,'PS_b1(const double y[], double ps[], int n, void *params):&#160;vacua.c']]]
];
